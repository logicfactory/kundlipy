from enum import IntEnum
from Planet import PlanetName
from Position import Position


class Constellation(IntEnum):
    ASWINI = 1
    BHARANI = 2
    KRITTIKA = 3
    ROHINI = 4
    MRIGASIRA = 5
    ARDRA = 6
    PUNARVASU = 7
    PUSHYAMI = 8
    ASLESHA = 9
    MAKHA = 10
    PURVA_PHALGUNI = 11
    UTTARA_PHALGUNI = 12
    HASTA = 13
    CHITRA = 14
    SWATI = 15
    VISAKHA = 16
    ANURADHA = 17
    JYESHTA = 18
    MULA = 19
    PURVASHADHA = 20
    UTTARASHADHA = 21
    SRAVANA = 22
    DHANISHTA = 23
    SATABHISHAK = 24
    PURVABHADRA = 25
    UTTARABHADRA = 26
    REVATI = 27

    def __init__(self, value) -> None:
        super().__init__()
        self.constellation_num = value

        self._lords = (
            PlanetName.KETU
            , PlanetName.VENUS
            , PlanetName.SUN
            , PlanetName.MOON
            , PlanetName.MARS
            , PlanetName.RAHU
            , PlanetName.JUPITER
            , PlanetName.SATURN
            , PlanetName.MERCURY
            , PlanetName.KETU
            , PlanetName.VENUS
            , PlanetName.SUN
            , PlanetName.MOON
            , PlanetName.MARS
            , PlanetName.RAHU
            , PlanetName.JUPITER
            , PlanetName.SATURN
            , PlanetName.MERCURY
            , PlanetName.KETU
            , PlanetName.VENUS
            , PlanetName.SUN
            , PlanetName.MOON
            , PlanetName.MARS
            , PlanetName.RAHU
            , PlanetName.JUPITER
            , PlanetName.SATURN
            , PlanetName.MERCURY
        )

    @property
    def lord(self) -> PlanetName:
        return self._lords[self.value-1]

    @property
    def start_position(self) -> Position:
        return Position().from_rad_coordinates(ra=(self.value-1)*360/27)

    @property
    def end_position(self) -> Position:
        return Position().from_rad_coordinates(ra=self.value * 360 / 27)
