from DateTime import DateTime
import math
from Place import Place
from Planet import Planet, PlanetName
from Sign import Sign
import unittest
from Position import Position
from ChartFactory import ChartFactory
from spiceypy import spiceypy
from datetime import datetime, timezone


class Person:

    def __init__(self, name: str, datetime_of_birth: DateTime, place_of_birth: Place)-> None:
        print('******************initializing Person************************')

        self._name = name
        self._datetime_of_birth = datetime_of_birth
        print('datetime set to: ', spiceypy.et2utc(self._datetime_of_birth.et,"C",2))
        print(self._name, self._datetime_of_birth)
        self._place_of_birth = place_of_birth
        self._planets = dict()
        self._planets = self._datetime_of_birth.get_planets_positions('ECLIPJ2000', 'EARTH', fixed_zodiac=True)
        self._sunrise, self._midday, self._sunset = self.get_sunrise_sunset()

        print('planet positions for ', self._name, ' are ', self._planets)

        self._ascendant = self.get_ascendant()
        print('Lagna/ascendant: ', self._ascendant)

        #self._zenith = self.get_zenith()
        #print('madhya lagna/zenith: ', self._zenith)

        self._charts = ChartFactory(ascendant=self._ascendant, zenith=None, planets= self._planets,
                                    datetime_of_birth=self.datetime_of_birth).charts

        self._total_nature = {}
        self._total_strength = {}
        self._net_score = {}

        for planet in PlanetName:
            self._total_nature[planet] = 0
            self._total_strength[planet] = 0
            self._net_score[planet] = 0
            for chart in self._charts.values():
                if chart.name != 'Transit':
                    self._total_nature[planet] += chart.get_planet_nature(planet)
                    self._total_strength[planet] += chart.get_planet_state(planet).value
                    self._net_score[planet] += (chart.get_planet_nature(planet) * chart.get_planet_state(planet).value)


        print('self._total_nature: ', self._total_nature)

    def __repr__(self) -> str:
        return self._name

    def __str__(self) -> str:
        return self._name

    @property
    def name(self) -> str:
        return self._name

    @property
    def place_of_birth(self) -> Place:
        return self._place_of_birth

    @property
    def charts(self):
        return self._charts

    @property
    def datetime_of_birth(self) -> DateTime:
        return self._datetime_of_birth

    @property
    def planets(self):
        return self._planets

    def get_planet_position(self, planet) -> Position:
        if isinstance(planet, Planet):
            return self.planets.get(planet.name).position
        elif isinstance(planet, PlanetName):
            return self.planets.get(planet).position
        elif isinstance(planet, str):
            return self.planets.get(PlanetName[planet]).position

        raise Exception("Invalid type Parameter provided")

    def get_sunrise_sunset(self):
        print('****************************getting sunrise and sunset for: ', self._place_of_birth.latitude,
              self._place_of_birth.longitude, self._datetime_of_birth, '********************************')

        day = self._datetime_of_birth.day
        month = self._datetime_of_birth.month
        year = self._datetime_of_birth.year

        TO_RAD = spiceypy.rpd()
        zenith = 90 + (50/60)

        # 1. first calculate the day of the year
        N1 = math.floor(275 * month / 9)
        N2 = math.floor((month + 9) / 12)
        N3 = (1 + math.floor((year - 4 * math.floor(year / 4) + 2) / 3))
        N = N1 - (N2 * N3) + day - 30

        print('N: ', N)

        # 2. convert the longitude to hour value and calculate an approximate time
        lngHour = self._place_of_birth.longitude / 15

        sunrise_t = N + ((6 - lngHour) / 24)
        sunset_t = N + ((18 - lngHour) / 24)

        # 3. calculate the Sun's mean anomaly
        sunrise_M = (0.9856 * sunrise_t) - 3.289
        sunset_M = (0.9856 * sunset_t) - 3.289


        # 4. calculate the Sun's true longitude
        sunrise_L = (sunrise_M + (1.916 * math.sin(TO_RAD * sunrise_M)) + (0.020 * math.sin(TO_RAD * 2 * sunrise_M))
                     + 282.634) % 360
        sunset_L = (sunset_M + (1.916 * math.sin(TO_RAD * sunset_M)) + (0.020 * math.sin(TO_RAD * 2 * sunset_M))
                    + 282.634) % 360


        # 5a. calculate the Sun's right ascension

        sunrise_RA = ((1 / TO_RAD) * math.atan(0.91764 * math.tan(TO_RAD * sunrise_L))) % 360
        sunset_RA = ((1 / TO_RAD) * math.atan(0.91764 * math.tan(TO_RAD * sunset_L))) % 360


        # 5b. right ascension value needs to be in the same quadrant as L
        sunrise_Lquadrant = (math.floor(sunrise_L / 90)) * 90
        sunset_Lquadrant = (math.floor(sunset_L / 90)) * 90

        sunrise_RAquadrant = (math.floor(sunrise_RA / 90)) * 90
        sunset_RAquadrant = (math.floor(sunset_RA / 90)) * 90

        sunrise_RA = sunrise_RA + (sunrise_Lquadrant - sunrise_RAquadrant)
        sunset_RA = sunset_RA + (sunset_Lquadrant - sunset_RAquadrant)


        # 5c. right ascension value needs to be converted into hours
        sunrise_RA = sunrise_RA / 15
        sunset_RA = sunset_RA / 15


        # 6. calculate the Sun's declination
        sunrise_sinDec = 0.39782 * math.sin(TO_RAD * sunrise_L)
        sunset_sinDec = 0.39782 * math.sin(TO_RAD * sunset_L)

        sunrise_cosDec = math.cos(math.asin(sunrise_sinDec))
        sunset_cosDec = math.cos(math.asin(sunset_sinDec))


        # 7a. calculate the Sun's local hour angle
        sunrise_cosH = (math.cos(TO_RAD * zenith) - (sunrise_sinDec * math.sin(TO_RAD * self._place_of_birth.latitude)))\
                       / (sunrise_cosDec * math.cos(TO_RAD * self._place_of_birth.latitude))
        sunset_cosH = (math.cos(TO_RAD * zenith) - (sunset_sinDec * math.sin(TO_RAD * self._place_of_birth.latitude))) /\
                      (sunset_cosDec * math.cos(TO_RAD * self._place_of_birth.latitude))

        if sunrise_cosH > 1:
            return {'status': False, 'msg': 'the sun never rises on this location (on the specified date)'}

        if sunrise_cosH < -1:
            return {'status': False, 'msg': 'the sun never sets on this location (on the specified date)'}

        # 7b. finish calculating H and convert into hours

        sunrise_H = 360 - (1 / TO_RAD) * math.acos(sunrise_cosH)
        print('sunrise_H: ', sunrise_H)
        # offset atmospheric refration and .33 to consider the center of the sun's disk
        sunrise_H = sunrise_H + .5 + .34

        sunset_H = (1 / TO_RAD) * math.acos(sunset_cosH)
        print('sunset_H: ', sunset_H)
        #offset atmospheric refration
        sunset_H = sunset_H - .5 - .34

        print('sunrise_H: ', sunrise_H)
        sunrise_H = sunrise_H / 15
        print('sunrise_H: ', sunrise_H)

        print('sunset_H: ', sunset_H)
        sunset_H = sunset_H / 15
        print('sunset_H: ', sunset_H)

        # 8. calculate local mean time of rising/setting
        sunrise_T = sunrise_H + sunrise_RA - (0.06571 * sunrise_t) - 6.622
        print('sunrise_T: ', sunrise_T)

        sunset_T = sunset_H + sunset_RA - (0.06571 * sunset_t) - 6.622
        print('sunset_T: ', sunset_T)

        # 9. adjust back to UTC
        sunrise_UT = (sunrise_T - lngHour) % 24
        print('sunrise_UT: ', sunrise_UT)

        sunset_UT = (sunset_T - lngHour) % 24
        print('sunset_UT: ', sunset_UT)

        time_offset = (self.datetime_of_birth.date_time.utcoffset().total_seconds() / 3600)
        print(time_offset)

        sunrise_LT = (sunrise_UT + time_offset) % 24
        sunrise_LT_hr = math.floor(sunrise_LT)
        sunrise_LT_min = math.floor((sunrise_LT - sunrise_LT_hr) * 60)
        sunrise_LT_sec = math.floor((((sunrise_LT - sunrise_LT_hr) * 60) - sunrise_LT_min) * 60)
        print(sunrise_LT_hr, sunrise_LT_min, sunrise_LT_sec)

        sunset_LT = (sunset_UT + time_offset) % 24
        sunset_LT_hr = math.floor(sunset_LT)
        sunset_LT_min = math.floor((sunset_LT - sunset_LT_hr) * 60)
        sunset_LT_sec = math.floor((((sunset_LT - sunset_LT_hr) * 60) - sunset_LT_min) * 60)
        print(sunset_LT_hr, sunset_LT_min, sunset_LT_sec)

        midday_LT = (sunrise_LT + sunset_LT) /2
        midday_LT_hr = math.floor(midday_LT)
        midday_LT_min = math.floor((midday_LT - midday_LT_hr) * 60)
        midday_LT_sec = math.floor((((midday_LT - midday_LT_hr) * 60) - midday_LT_min) * 60)
        print(midday_LT_hr, midday_LT_min, midday_LT_sec)

        sunrise = DateTime(day=self.datetime_of_birth.day,month=self.datetime_of_birth.month,
                           year=self.datetime_of_birth.year, hour=sunrise_LT_hr, min=sunrise_LT_min, sec=sunrise_LT_sec,
                           microsecond=0, tzinfo=self.datetime_of_birth.tzinfo)
        sunset = DateTime(day=self.datetime_of_birth.day, month=self.datetime_of_birth.month,
                          year=self.datetime_of_birth.year, hour=sunset_LT_hr, min=sunset_LT_min, sec=sunset_LT_sec,
                          microsecond=0, tzinfo=self.datetime_of_birth.tzinfo)
        midday = DateTime(day=self.datetime_of_birth.day, month=self.datetime_of_birth.month,
                          year=self.datetime_of_birth.year, hour=midday_LT_hr, min=midday_LT_min, sec=midday_LT_sec,
                          microsecond=0, tzinfo=self.datetime_of_birth.tzinfo)

        print(sunrise, midday, sunset)
        return sunrise, midday, sunset

    def get_ascendant(self) -> Position:
        print('*******************inside getting ascendant************************')
        rasimanas = self._place_of_birth.get_rasimanas
        sunrise = self._sunrise
        print('DOB: ', self.datetime_of_birth, 'sunrise: ', sunrise)
        print('rasimanas: ', rasimanas)

        #time since sunrise
        difference = (self.datetime_of_birth - sunrise)
        #time since sunrise in seconds
        difference_in_seconds = difference.total_seconds()
        print('difference_in_seconds: ', difference_in_seconds)

        #sun's position at time of birth
        sun_position = sunrise.get_planet_position(PlanetName.SUN, 'ECLIPJ2000', 'EARTH', fixed_zodiac=False)
        ascendant_position = sun_position.copy()
        start_sign = ascendant_position.sign

        print('1. ascendant: ', ascendant_position, ' difference_in_seconds: ', difference_in_seconds)

        total_time_of_current_sign = rasimanas.get(ascendant_position.sign)
        print('total_time_of_current_sign: ', ascendant_position.sign, total_time_of_current_sign)
        time_left_in_current_sign = (total_time_of_current_sign/30)*(30-ascendant_position.degrees_in_sign)
        print('time_left_in_current_sign: ', time_left_in_current_sign)

        if time_left_in_current_sign <= abs(difference_in_seconds):
            difference_in_seconds = difference_in_seconds - math.copysign(time_left_in_current_sign,
                                                                          difference_in_seconds)
            degrees_left = 30 - ascendant_position.degrees_in_sign
            ascendant_position.add_ra(math.copysign(degrees_left, difference_in_seconds))
            print('2. ascendant: ', ascendant_position, ' difference_in_seconds: ', difference_in_seconds)

            start_sign = start_sign + math.copysign(1, difference_in_seconds)

        # loop through rasimanas starting from the next sign
        for i in range(0, 12):
            sign = Sign(((start_sign + math.copysign(i, difference_in_seconds)-1)%12)+1)
            time_of_sign = rasimanas.get(sign)
            print('in loop for i: ', i, ' sign: ', sign, ' with rashi time: ', time_of_sign)

            if time_of_sign <= abs(difference_in_seconds):
                difference_in_seconds = difference_in_seconds - math.copysign(time_of_sign, difference_in_seconds)
                ascendant_position.add_ra(math.copysign(30, difference_in_seconds))

            elif abs(difference_in_seconds) > 0:
                degrees = 30*difference_in_seconds/time_of_sign
                print('final degrees in the final sign: ', degrees)
                ascendant_position.add_ra(math.copysign(degrees, difference_in_seconds))
                difference_in_seconds = difference_in_seconds - math.copysign(difference_in_seconds, difference_in_seconds)
                #break

            print('3. ascendant: ', ascendant_position, ' difference_in_seconds: ', difference_in_seconds)

        ascendant_position.add_ra(-self.datetime_of_birth.precession)

        #sun_position_at_sunrise = sunrise.get_planet_position(PlanetName.SUN, 'ECLIPJ2000', 'EARTH', False)
        #degrees_sun_traversed = sun_position.ra - sun_position_at_sunrise.ra
        #print('degrees_sun_traversed: ', degrees_sun_traversed)
        #ascendant_position.add_ra(-degrees_sun_traversed)

        return ascendant_position

    def get_zenith(self) -> Position:
        print('********************inside getting zenith*****************')
        rasimanas = self._place_of_birth.get_rasimanas
        noon = self._midday

        print('DOB: ', self.datetime_of_birth, 'rasimanas: ', rasimanas)

        #time since sunrise
        difference = (self.datetime_of_birth - noon)
        #time since sunrise in seconds
        difference_in_seconds = difference.total_seconds()
        print('time difference: ', difference_in_seconds)

        #sun's position at time of birth
        sun_position = noon.get_planet_position(PlanetName.SUN, fixed_zodiac=True)
        zenith_position = sun_position.copy()

        total_time_of_current_sign = rasimanas.get(zenith_position.sign)
        time_left_in_current_sign = (total_time_of_current_sign/30)*(30-zenith_position.degrees_in_sign)
        print('initial zenith_position: ', zenith_position, 'time difference: ', time_left_in_current_sign)

        if time_left_in_current_sign <= abs(difference_in_seconds):
            difference_in_seconds = difference_in_seconds - math.copysign(time_left_in_current_sign,
                                                                          difference_in_seconds)
            degrees_left = 30-zenith_position.degrees_in_sign
            zenith_position.add_ra(math.copysign(degrees_left, difference_in_seconds))
            print('2. zenith_position: ', zenith_position, 'time difference: ', difference_in_seconds)

        # loop through rasimanas starting from the next sign
        for i in range(1, 13):
            sign = Sign(((zenith_position.sign + math.copysign(i, difference_in_seconds)-1)%12)+1)
            time_of_sign = rasimanas.get(sign)
            print('in loop for ', sign, ' with rashi time: ', time_of_sign)

            if time_of_sign <= abs(difference_in_seconds):
                difference_in_seconds = difference_in_seconds - math.copysign(time_of_sign, difference_in_seconds)
                zenith_position.add_ra(math.copysign(30, difference_in_seconds))
                print('3. zenith: ', zenith_position, 'time difference: ', difference_in_seconds)

            elif abs(difference_in_seconds) > 0:
                degrees = 30*difference_in_seconds/rasimanas.get(sign)
                zenith_position.add_ra(math.copysign(degrees, difference_in_seconds))
                print('4. zenith: ', zenith_position, 'time difference: ', difference_in_seconds)

                break

        return zenith_position

class TestPerson(unittest.TestCase):

    def setUp(self) -> None:
        super().setUp()

    def tearDown(self) -> None:
        super().tearDown()

