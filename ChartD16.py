from Chart import Chart
from Position import Position
from typing import List, Dict
from Planet import Planet
from Sign import Sign, MoveType


class ChartD16(Chart):

    def __init__(self, name: str, ascendant: Position = Position(), zenith: Position = Position(),
                 planets: Dict = None) -> None:
        super().__init__(name=name, ascendant=ascendant, zenith=zenith, planets=planets)
        print('****************initializing ', self.__class__, '******************')

    @staticmethod
    def translate_position(position: Position):
        print('inside loop for position: ', position)
        no_of_parts = 16

        quotient, degrees_in_sign = divmod(position.ra * no_of_parts, 30)
        print('quotient and degrees_in_sign: ', quotient, degrees_in_sign)

        part_number = (quotient % no_of_parts) + 1
        print('part_number: ', part_number)

        if position.sign.move_type == MoveType.MOVEABLE:
            position.ra = ((part_number - 1) * 30) + degrees_in_sign
        elif position.sign.move_type == MoveType.FIXED:
            position.ra = ((4 + part_number - 1) * 30) + degrees_in_sign
        elif position.sign.move_type == MoveType.DUAL:
            position.ra = ((8 + part_number - 1) * 30) + degrees_in_sign
