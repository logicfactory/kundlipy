from spiceypy import spiceypy
from datetime import datetime

print(spiceypy.tkvrsn("TOOLKIT"))
spiceypy.furnsh("KernelMeta.txt")
et = spiceypy.utc2et('1981-09-25T06:25:00.00')
for i in range(1,108000):
    distance1, sun_ra_itrf, sun_dec_itrf = spiceypy.recrad(spiceypy.spkpos('SUN', et+i, 'ITRF93', 'LT+S', 'EARTH')[0])
    distance2, sun_ra_j2000, sun_dec_j2000 = spiceypy.recrad(spiceypy.spkpos('SUN', et+i, 'J2000', 'LT+S', 'EARTH')[0])
    print('time: ', spiceypy.et2utc(et+i, 'C', 2), ' ra_itrf: ', sun_ra_itrf * spiceypy.dpr(),
        ' ra_j2000: ', sun_ra_j2000 * spiceypy.dpr(),
        'difference: ', (sun_ra_j2000 - sun_ra_itrf) * spiceypy.dpr())

spiceypy.kclear()

import ephem
atlanta = ephem.Observer()

atlanta.lat, atlanta.lon = '26.7829', '79.0277'
atlanta.date = '1985/10/12 17:00'

print(atlanta.previous_rising(ephem.Sun()))
print('---------------------')

atlanta.horizon = '0'
print(atlanta.previous_rising(ephem.Sun()))

atlanta.horizon = '+0:5'
print(atlanta.previous_rising(ephem.Sun()))
print('-------------pressure=0------------')

atlanta.pressure = 0

atlanta.horizon = '-0:34'
print(atlanta.previous_rising(ephem.Sun()))

atlanta.horizon = '0'
print(atlanta.previous_rising(ephem.Sun()))

atlanta.horizon = '+0:5'
print(atlanta.previous_rising(ephem.Sun()))
