from spiceypy import spiceypy
from datetime import datetime
print(spiceypy.tkvrsn("TOOLKIT"))
spiceypy.furnsh("KernelMeta.txt")
TDBFMT = 'YYYY MON DD HR:MN:SC.### (UTC) ::UTC'
MAXIVL = 1000
MAXWIN = 2 * MAXIVL

# Assign the inputs for our search.
#
# Since we're interested in the apparent location of the
# target, we use light time and stellar aberration
# corrections. We use the "converged Newtonian" form
# of the light time correction because this choice may
# increase the accuracy of the occultation times we'll
# compute using gfoclt.
#
srfpt = 'EARTH'
obsfrm = 'J2000'
target = 'SUN'
abcorr = 'LT+S'
crdsys = 'RA/DEC'
coord = 'DECLINATION'
relate = '='
revlim = 0

#
# The adjustment value only applies to absolute extrema
# searches; simply give it an initial value of zero
# for this inequality search.
#
adjust = 0.0

#
# stepsz is the step size, measured in seconds, used to search
# for times bracketing a state transition. Since we don't expect
# any events of interest to be shorter than five minutes, and
# since the separation between events is well over 5 minutes,
# we'll use this value as our step size. Units are seconds.
#
stepsz = 300

#
# Display a banner for the output report:
#
print('\n{:s}\n'.format('Inputs for target visibility search:'))
print('   Target                       = {:s}'.format(target))
print('   Observation surface location = {:s}'.format(srfpt))
print('   Observer\'s reference frame   = {:s}'.format(obsfrm))
print('   Aberration correction        = {:s}'.format(abcorr))
print('   Step size (seconds)          = {:f}'.format(stepsz))

#
# Convert the start and stop times to ET.
#
for year in range(0, 20):
    et = spiceypy.utc2et(str(1999+year) + '-01-01T00:00:00.0')

    etbeg = et
    etend = et + 7296000

    #print('searching between: ', spiceypy.et2utc(etbeg, "C", 2), ' and ', spiceypy.et2utc(etend, "C", 2))

    #
    # Initialize the "confinement" window with the interval
    # over which we'll conduct the search.
    #
    cnfine = spiceypy.stypes.SPICEDOUBLE_CELL(2)
    spiceypy.wninsd(etbeg, etend, cnfine)

    #
    # In the call below, the maximum number of window
    # intervals gfposc can store internally is set to MAXIVL.
    # We set the cell size to MAXWIN to achieve this.
    #
    riswin = spiceypy.stypes.SPICEDOUBLE_CELL(MAXWIN)

    #
    # Now search for the time period, within our confinement
    # window, during which the apparent target has elevation
    # at least equal to the elevation limit.
    #
    spiceypy.gfposc(target, obsfrm, abcorr, srfpt,
                crdsys, coord, relate, revlim,
                adjust, stepsz, MAXIVL, cnfine, riswin)
    #
    # The function wncard returns the number of intervals
    # in a SPICE window.
    #
    winsiz = spiceypy.wncard(riswin)
    time1 = time2 = et

    if winsiz == 0:

        print('No events were found.')

    else:

        #
        # Display the visibility time periods.
        #
        #print('Visibility times of {0:s} as seen from {1:s}:\n'.format(target, srfpt))

        #print('winsiz: ', winsiz)
        #print('riswin: ', riswin)
        time2 = etend

    for i in range(winsiz):

        #
        # Fetch the start and stop times of
        # the ith interval from the search result
        # window riswin.
        #
        ith_time = spiceypy.wnfetd(riswin, i)[0]
        #print('--------------in the loop------------', spiceypy.et2utc(ith_time, "C", 2))

        distance, ra1, dec1 = spiceypy.recrad(spiceypy.spkpos('SUN', ith_time, 'J2000', 'LT+S', 'EARTH')[0])
        #print('J2000 time: ', spiceypy.et2utc(ith_time+i, 'C', 2), ' ra: ', ra * spiceypy.dpr(), 'dec: ', dec * spiceypy.dpr())

        distance, ra2, dec2 = spiceypy.recrad(spiceypy.spkpos('SUN', ith_time, 'ECLIPJ2000', 'LT+S', 'EARTH')[0])
        print('ECLIPJ2000 time: ', spiceypy.et2utc(ith_time, 'C', 2), ' J2000ra: ', ra1 * spiceypy.dpr(),
              ' ECLIPJ2000ra: ',ra2 * spiceypy.dpr(), ' j2000dec: ', dec1*spiceypy.dpr(), ' eclipdec: ', dec2*spiceypy.dpr())

spiceypy.kclear()