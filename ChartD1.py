from Chart import Chart
from Position import Position
from typing import List, Dict
from Planet import PlanetName


class ChartD1(Chart):

    def __init__(self, name: str, ascendant: Position = Position(), zenith: Position = Position(),
                 planets: Dict = None) -> None:
        print('ascendant: ', ascendant)
        print('zenith: ', zenith)
        print('planets: ', planets)
        super().__init__(name=name, ascendant=ascendant, zenith=zenith, planets=planets)
        print('****************initializing ', self.__class__, '******************')

    @staticmethod
    def translate_position(position: Position):
        print('inside loop for position: ', position)
        print('no need to do anything')
