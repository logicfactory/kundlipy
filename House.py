from Sign import Sign
from Planet import Planet, PlanetName
from Position import Position


class House:

    def __init__(self, number: int, start_position: Position = Position(), cusp_position: Position = Position(),
                 end_position: Position = Position(), sign: Sign = None, resident_planets: list = None) -> None:

        self._number = number

        if isinstance(resident_planets, list):
            self._resident_planets = resident_planets
        else:
            self._resident_planets = list()

        self._sign = sign
        self._start_position = start_position
        self._cusp_position = cusp_position
        self._end_position = end_position

        self._aspecting_planets = list()

        self._areas_of_life = {
            1: ['Physical body', 'complexion', 'appearance', 'head', 'intelligence', 'strength', 'energy', 'fame',
                    'success', 'nature of birth', 'caste'],
            2: ['Wealth', 'assets', 'family', 'speech', 'eyes', 'mouth', 'face', 'voice', 'food'],
            3: ['Younger coborns', 'confidants', 'courage', 'mental strength', 'communication skills', 'creativity',
                'throat', 'ears', 'arms', 'fathers death', 'expenditure on vehicles and house', 'travels'],
            4: ['Mother', 'vehicles', 'house', 'lands', 'immovable property', 'motherland', 'childhood',
                'wealth from real estate', 'education', 'relatives', 'happiness', 'comforts', 'pleasures', 'peace',
                'state of mind', 'heart'],
            5: ['Children', 'Poorvapunya', 'intelligence', 'knowledge & scholarship', 'devotion', 'Mantras(prayers)',
                'stomach', 'digestive system', 'authority / power', 'fame', 'love', 'affection', 'emotions', 'judgment',
                'speculation'],
            6: ['Enemies', 'service', 'servants', 'relatives', 'mental tension', 'injuries', 'health', 'diseases',
                'agriculture', 'accidents', 'mental affliction', 'mothers younger brother', 'hips'],
            7: ['Marriage', 'marital life', 'life partner', 'sex', 'passion(and related happiness)', 'long journeys',
                'partners', 'business', 'death', 'portion of body below navel'],
            8: ['Longevity', 'debts', 'disease', 'ill - fame', 'inheritance', 'loss of friends', 'occult studies',
                'evils', 'gifts', 'unearned wealth', 'windfall', 'disgrace', 'secrets', 'genitals'],
            9: ['Father', 'teacher', 'boss', 'fortune', 'religiousness', 'spirituality', 'God',
                'higher studies & high knowledge', 'fortune in a foreign land', 'foreign trips',
                'Diksha(joining a religious order)', 'past life and cause of birth', 'grandchildren', 'principles',
                'Dharma', 'intuition', 'compassion', 'sympathy', 'leadership', 'charity', 'thighs'],
            10: ['Growth', 'profession', 'career', 'Karma(action)', 'conduct in society', 'fame', 'honors', 'awards',
                 'selfrespect', 'dignity', 'knees'],
            11: ['Elder coborns', 'income', 'gains', 'realization of hopes', 'friends', 'ankles'],
            12: ['Losses', 'expenditure', 'punishment', 'imprisonment', 'hospitalization', 'pleasures in bed',
                 'misfortune', 'bad habits', 'sleep', 'meditation', 'donation', 'secret enemies', 'heaven', 'left eye',
                 'feet', 'foreign residence', 'Moksha']
        }

        self._lagna_results = {
            1: 'Persons born in Aries will have a certain amount of independent thinking and reasoning faculty. '
               'They will be capable. They will not be slaves to laws of convention or those arbitrarily established. '
               'They are lovers of scienti-fic thought and philosophy ; have their own ideas of right and wrong and are'
               ' strongly bent upon educational pursuits. As the Rain is ruling them, they are rather stubborn but '
               'often frank, impul-sive and courageous. They are more gossipers than practical men. They sometimes '
               'require a certain amount of cajolery and sycophancy to raise them to action. They will become pioneers.'
               ' As Mars is the lord of Aries they will be martial in spirit. Their constitution will be hot, and they '
               'are occasionally subject to hot complaints, piles and the like and must avoid enterprises obviously '
               'involving  any  serious risks.  They love beauty, art and elegance. The diseases they suffer from will '
               'be mostly those of the head and unpleasant sight-seeing may often lead to mental affliction and '
               'derangement of brain. Their build will be slender and & generally possess fairly perfect contours.',
            2: 'The stature of the persons born in this sign will be short and often corpulent, lips thick, complexion'
               '  swarthy. They generally resemble the WI in their behaviour toivards new people if they are not '
               'listened to properly. They will  have  their own  principles  and  ways, and  a  piercing  intellect. '
               'They shine well as authors and journalists. They are not bound by sentimentality but appreciate truth. '
               'They are remarkable for their ability to commit to memory. Their physical and mental endurance are '
               'indeed noteworthy. They haVe much business knack and surpassing intuition. They often think they are '
               'born to exercise authority over others and in a sense they are right. They are sensitive to physical '
               'influences. They are often liable  to extremes, zealous, and easily accessible to adul-tation. They '
               'generally suffer from nervous com-plaints after their fiftieth year but their memory and  powers of '
               'imagination will never deceive them. They are slow to anger, but when pro-voked furious like the bull. '
               'They are passionate and  many become preys to sexual diseases in their old age.',
            3: 'Person born when Gemini is rising will have a wavering mind, often tall and straight in nature and '
               'active in_ motion, forehead broad, eyes clear and nose a bit snubbed. They are very active and become '
               'experts in mechanical sciences provided Saturn has some strong influence over them. They will be a jack'
               ' of all trades but master of none. They are vivacious, but liable to be inconstant:  They will have '
               'sudden nervous bieakdowns and must exercise a certain amount of caution in moving with the opposite sex'
               ' ; a habit of self-control must be cultivated. Their mind will be often conscious of their own '
               'depra-vity. They are liable to fraud and deception. If evil planets are found in Gemini, trickery and '
               'deceit will  characterise their nature.',
            4: 'Persons born under  Cancer  will have a middle-sized body, face full, nose snub-bed to some extent and '
               'complexion white.   They are very intelligent, bright and extremely frugal and  equally industrious. '
               'Their frugality often takes  the form_ of miserliness  sympathy,  and cowardice. They will be much '
               'attached to their children and family.  Their extreme sensitiveness renders them  nervous and queer. '
               'Their minds will be bent upon  schemes of trade and manu-facture. They often meet with disappointments '
               'in  love affairs. They are very talkative, self-reliant, honest and unbending. They will have '
               'reputation as lovers of justice and fairplay.',
            5: 'Persons born under Leo will be majes-tic in appearance, broad shoulders, bilious consti-tution and '
               'bold and respectful in temperament. They possess the knack to adapt themselves to any condition of '
               'life. They are rather ambitious and sometimes avaricious too. They are inde-pendent thinkers. They '
               'stick up to orthodoxical principles in religion  but are perfectly tolerant towards others precepts and'
               '  practices. They are lovers of fine arts and literature and possess a certain amount of philosophical '
               'knowledge. They are voracious readers. In life they do not succeed in the end as they expect, and often'
               ' throughout their life they struggle very much for success. Their ambitions remain unfulfilled to a '
               'great extent.   They get into many difficulties because they lack a natural policy.',
            6: 'People born when Kanya or Virgo is rising will exhibit their intelligence and memory when quite young. '
               'They will be middle-sized persons and exhibit taste in art and literature. Their chest will be '
               'prominent and when afflicted very  weak also. They are discriminating and emotional and are carried '
               'away by impulses. As authors they make real progress especially in Bhouthika Sastras and Rasayana '
               'Sastras (Physi-cal and Chemical Sciences) and can judge things at a glance. They love music and fine '
               'arts and acquire much power and influence over other people. They are liable to suffer from nervous '
               'breakdowns and paralysis when the sign is afflicted.',
            7: 'The complexion of persons born in this sign will be fair, their stature middle-sized, face broad, eyes '
               'fine, chest broad and light, appearance handsome, constitution rather phleg-matic, sensual disposition '
               'and keen observation. They have a keen foresight and reason out things from the standpoint of their own'
               ' views. Firm in conviction and unmoved by mean motives they are somewhat susceptible to the feelings of'
               ' others minds. They are more idealists than realists or practical men and often contemplate upon '
               'schemes like building castles in the air. They are not sensitive to what others say of them. But as '
               'political leaders and religious reformers they exert tremendous influences over the masses and '
               'sometimes their zeal and enthusiasm go to such a high pitch that they force their views upon others of'
               ' opposite thought not realising the bane-ful after-effects of such a procedure.  They love excitement '
               'and will have the power of intuition which  they  more rely upon for their  own guidance. They are not '
               'easily amenable to reason and they are great lovers  of music.   They have a special liking for truth '
               'and honesty and do not hesitate to sacrifice even their lives at the altars of freedom and fairplay.',
            8: 'Those  born  under this sign will have youthful appearance, a generous disposition and  fierce eyes. '
               'They are  exceedingly  fickle-minded and love much excitement. They are inclined to sensual things in '
               'reality while they wont hesitate to philosophise upon the merits of controlling sensual pleasures. Even'
               ' females born in this sign will have more of masculine tenden-cies. They are good  correspondents and '
               'invite friendship from  among people throughout the world.   They will  become expert musicians if they'
               ' care to practise that art. They are profi-cient in fine arts, dancing and the like and no doubt they'
               ' have a philosophic disposition. They set at naught conventional habits and customs. They  vehemently '
               'uphold  their own  views but nevertheless will  not clash with  those holding opposite ones. Their '
               'constitution  will be  hot and they are liable to suffer from piles and vene-real complaints after '
               'their. 30th year. They are silent  and dignified and never speak before weighing each and every word. '
               'They are good conversationalists as well as writers and often rely too much on their own intelligence.',
            9: 'Jupiter  rules  this  sign  and persons born under this sign will generally be corpulent. They possess '
               'almond eyes and their hair is brown. They are of a phlegmatic tempera-ment. They are too conventional '
               'and sometimes businesslike also.   They are prompt and uphold orthodox views. They will be attracted '
               'towards the study of occult philosophy and sciences. In these departments of knowledge they will really'
               ' acquire a great mastery in a short time. They are too callous and enthusiastic. They hate all external'
               ' show. They are God-fearing, honest, humble and free from hypocrisy. They never think of schemes which '
               'are calculated to disturb the pro-gress of others. They will exercise strict control over their food '
               'and drinks and in regard to their relationship with the opposite  sex. They are brilliant, their '
               'manners  affable,  winning and hearts pure. They are prone to be misunderstood unintentionally by '
               'others on account of their hastiness in conversation.  In their later years they must be careful about '
               'their lungs as they are liable to suffer from rheumatic pains and the like.',
            10: 'Persons born in this sign will be tall, reddish-brown in colour with prominent stiff hair on the '
                'eye-brows and the chest.   They have large teeth sometimes protruding outside the lips and presenting '
                'an uncouth appearance.   They have a knack to adapt themselves to circumstances and environments. They'
                ' have great aspirations in life and cannot economise funds even if they were to be under the influence'
                ' of adversity. They are modest, liberal, gentlemanly in business tran-sactions. They are noted for '
                'their perseverance and strong-mindedness. In fact they are stoical to the miseries of life. They are '
                'possessed of sympathy, generosity and philanthropy and take a great interest in literature, science '
                'and education. Sometimes they are vindictive, when Saturn is badly posited. They are possessed of an '
                'invincible bigotry before which sober counsels will be of no avail.',
            11: 'This "Water Bearer"  sign in which the birth  of an individual is condemned, as the birth lord becomes'
                ' the lord of the house of loss, has to its credit - some of the greatest philosophers and seers. Those'
                ' born under this sign will be:tall, lean, countenance, handsome, manners winning, appearance '
                'attractive,  dispo-sition  elegant. Their lips are flushy, cheeks broad with prominent temples and '
                'buttocks. They are highly intelligent and make friends of others very soon. They are peevish and when '
                'provoked rise like a bulldog but their anger is very soon subsided. They are pure in heart and always '
                'inclined to help others. They shine very well as writers and are good spokesmen. They are timid,  '
                'funky  and  at  times cowardly also. They feel shy to exhibit their talents  before new audience  but '
                'their conversation will  be  most interesting and  highly instructive. They  will specialise in '
                'subjects like astrology. Their literary greatness will come before the world when they are quite young'
                ' and they themselves will not be able to estimate their capacities well, while others find in them '
                'something remarkable and extra-ordinary.   They are intuitive and good judges of character.  They have'
                ' no organising capacity and are capable of acquiring very fine education. They will be much devoted to'
                ' their husbands or wives and never betray the interests of even their ene-mies, when trust is placed '
                'in them.  They are liable to suffer from colic troubles and must take special   precautions   to   '
                'safeguard  themselves against diseases incidental to exposure to cold weather.   On the whole, '
                'Aquarius people have something subtle  in  them  which endears them to all they come in contact with.',
            12: 'Persons born in this sign will be fair, stout, of middle-sized height. They are very reserved in their'
                ' manners and are liable to draw premature conclusions on any matter. They are God-fearing. They are '
                'extremely superstitious and religious, very  rigid  in  the observance of orthodoxical principles and '
                'can forego anything but their orthodoxy. They are stubborn, rather timid, and ambitious to exercise '
                'authority over others.   They are true friends and are proud of their educational and other '
                'attainments. If the lord of the 7th is badly afflicted, they will have double marriage. They are '
                'restless and fond of history, antiquarian talks and mythological masterpieces. They are frugal in '
                'spending money and though generally dependent  upon  others throughout their life still bear a mark of'
                ' inde-pendence. They are just in their dealings and fear to transgress the laws of truth. With all '
                'this, they lack self-confidence.'
        }

        self._house_results = {
            1: {
                PlanetName.SUN: "Righteous-minded, healthy, bilious, eye-disease, intelligence, good  morals, political "
                                "success,  stately appearance, humanitarian instincts, lazy in work, fond of daring deeds, "
                                "hot constitution, careless of reputation,  strong will, caprice, generosity, neglect of "
                                "personal credit or respect, good work, not combative or impetuous and not pioneering.",
                PlanetName.MOON: "Fanciful and romantic, moderate eater, an attractive appearance, corpulent, windy "
                                 "temperament, great travel-ler and explorer, disease in private organs and ears, "
                                 "capricious, licentious, sociable, easy going, educated, warring, loved by the fair "
                                 "sex, shy, modest, stubborn, proud, fickle-minded, eccentric and hysterical.",
                PlanetName.MARS: "Heated consti-tution, scars in the body, pilfering habits, corpulence, big navel, "
                                 "early danger to father, reddish composition,  respected,  active,  powerful  and "
                                 "low-minded.",
                PlanetName.MERCURY: "Cheerful, humorous, well read, clever, many enemies, learned, fond of occult "
                                    "studies and astronomy,  witty, influential, intellectual, respected, long-lived, "
                                    "love of literature and poetry.",
                PlanetName.JUPITER: "Magnetic per-sonality, good grammarian, majestic appearance, highly educated, "
                                    "many children, learned, dexte-rous, long lived, respected by rulers, philologist,"
                                    " political success,  sagacious, stout  body,  able, influential, leader. ",
                PlanetName.VENUS: "Expert mathe-matician, very fortunate, ambitious, bold, long life, pioneering, fond "
                                  " of wife, strenuous, skilled in sexual science,  successful,  practical, scents, "
                                  "flowers, women skilled in  fine  arts, pleasing, vivacious,  astrologer,  much  "
                                  "magnetic  power, leader of people.",
                PlanetName.SATURN: "Foreign customs and habits, perverted mind, bad thoughts, evil-natured, tyrannical, "
                                   "unscrupulous, insigni-ficant life, well-built thighs, strong-minded, cun-ning, thrifty,"
                                   " dirty, passionate, aspiring, curious, deformed, sickly, exploring, flatulence, "
                                   "licentious, adultery among low women.",
                PlanetName.RAHU: "Obliging, sympa-thetic, abortion, courageous.",
                PlanetName.KETU: "Emaciated figure, weak   constitution, • great  perspiration, weak-hearted, slender, "
                                 "piles, sexual indulgence, diplo-matic."
                       },
            2: {
                PlanetName.SUN: "Diseased  face,  ugly,  losses from  prosecutions,  good  earnings, inclined  to waste, bright speech, enquiring, well educated, scientific, stubborn and peevish temper, danger in the 25th year.",
                PlanetName.MOON: "Wealthy, handsome, attrac-tive, generous, highly intelligent, breaks in edu-cation, charming, poetical, great respect, sweet speech persuasive, squint eyes and much admired.",
                PlanetName.MARS: "Quarrelsome,  extravagant, harsh speech. adulterous, short-temper, immoral. wasteful, sharp-tongued, broken education, sati-rical, large patrimony, bad-tempered, aggressive, unpopular and awkward.",
                PlanetName.MERCURY: "Learned  in religious and philosophical lore, sweet speech, good  conversa-tionalist, humorous, clever, many children, deter-mined,  fine  manners,  captivating look,  self-acquisition, wealthy, careful, thrifty, clever inearning money.",
                PlanetName.JUPITER: "Wealthy,  intelligent,  digni-fi ed,  attractive,   happy,  fluent speaker,  aristo-cratic, tasteful,  winning manners, accumulated fortune, witty, good wife and family, eloquent, humorous, and dexterous.",
                PlanetName.VENUS: "Large family, happy, deli-cious drinks, luxurious meals, handsome, large fair eyes, charming wife, witty, brilliant, polite, educated, hating women, obliging, rapid in-mind, clever in speech, agreeable, narrow-minded, origi-nal, creative author, orthodox, composer, eco-nomical, wealthy, logical, able.",
                PlanetName.SATURN: "Two wives, diseased face, un-popular, broken education, weak sight, unsocial, harsh speech, stammering, addicted to wine.",
                PlanetName.RAHU: "Poor, more than one wife if afflicted, dark complexion, diseased face, peevish, luxurious dinners.",
                PlanetName.KETU: "Bad speaker, quiet, quick in perception, peevish, cruel, thrifty and economical."
            },
            3: {
                PlanetName.SUN: "Courageous, adventurous, fa-mous, intelligent, wealthy, successful and restless.",
                PlanetName.MOON: "Sickly, dyspeptic and later on piles,  mild,   lean,   disappointments,  impious, many  brothers, cruel, educated, consumption, famous, sisters, intelligent, unscrupulous, purpose-less , fond of travelling and active-minded.",
                PlanetName.MARS: "Pioneering, few brothers, fond of prostitutes, courageous, intelligent, reckless, adulterous,  adventurous,   short-tempered,   un-principled, immoral, unpopular.",
                PlanetName.MERCURY: "Daughter,   happy   mother, clever, cruel, loved by fair sex, tactful, diplomatic, discretion, bold, sensible.",
                PlanetName.JUPITER: "Famous,   many   brothers, ancestors, devoted to the family, miserly, oblig-ing,  polite,  unscrupulous,  good  agriculturist, thrifty,  good success, energetic, bold, taste for fine arts and literature, loved by relatives.",
                PlanetName.VENUS: "Lover of fine arts, prosperity to mother, wealthy, miserly, obliging, well placed, travelling, original.",
                PlanetName.SATURN: "Intelligent, wealthy, wicked, loss of brothers, polite, adventurous, bold, eccen-tric, cruel, courageous, obliging, agriculturist.",
                PlanetName.RAHU: "Few children; very wealthy, bold, adventurous, courageous, good gymnastic, many relations.",
                PlanetName.KETU: "Adventurous, strong, artistic, wealthy, popular."
            },
            4: {
                PlanetName.SUN: "Mental  worry,  meditative, defective organs, success in foreign countries, hatred  of   relations, keen-minded,  sensitive, good   reputation,   success   after   middle-age, quarrels  without   causes,   weak   constitution, introspective, unhappy and philosophical.",
                PlanetName.MOON: "Comfortable, fine taste, good perfumes and dress, polite and affable manners, high education, happy, licentious, helped by all, wealthy and successful, good mother, popular and death to mother, early if in conjunction with malefics.",
                PlanetName.MARS: "Sickly mother, quarrels, un-happy home life, danger  to  father,  domestic quarrels and conveyances, uncomfortable, coarse, brutal, tyrannical, vulgar.",
                PlanetName.MERCURY: "Learned, agriculturist, good mother,  mother unhappy, skilled in conjuring tricks, obliging,  cultured, affectionate, popular, bale, inclined to pursue literary activities.",
                PlanetName.JUPITER: "Good conveyances, educated, happy, intelligent, wealthy, founder of charitable institutions, comfortable, good inheritance, good mother, well read, contented life.",
                PlanetName.VENUS: "Intelligent, happy, affection-ate,  learned, affectionate mother, agriculturist, educated, scientific methods, peaceful life, pro-tector of cattle, endeared by relations, fond of milk, famous, literary, successful, popular.",
                PlanetName.SATURN: "Danger to mother if with the Moon, unhappy, sudden losses, colic pains, nar-row-minded, miserable, crafty, estates encumber-ed,  good thinker, good patrimony, success in foreign countries, political disfavour, licentious, interrupted education.",
                PlanetName.RAHU: "Intercourse with low women, subordinate, proficient in European languages.",
                PlanetName.KETU: "Quarrelsome,	licentious, weak, fear of poisons."
            },
            5: {
                PlanetName.SUN: "Intelligent, poor, few children, paternal  danger,  corpulent,  danger to father early,  unhappy, disturbed in  mind,  lover  of fi ne arts, and tactful in decision.",
                PlanetName.MOON: "Subtle, handsome wife, shrewd, showy, many daughters, intelligent, gains through quadrupeds., interrupted education.",
                PlanetName.MARS: "Unpopular, no issues, rogue, ambitious, intelligent, persevering, unhappy, bold, unprincipled, decisive.",
                PlanetName.MERCURY: "Showy,  learned, quarrelsome, danger to maternal  uncles,  parents sickly, good administrative capacity, fond of good furniture and dress, respect from moneyed men, ministerial office,  executive  ability,  speculative,   scholar, vain, danger to father, combative.",
                PlanetName.JUPITER: "Broad eyes, handsome, states-manly ability, good insight, minister, intelligent, skilful in trade, few children, good position, dire misfortune, leader.",
                PlanetName.VENUS: "Clever, intelligent, statesmanly ability, good counsel, danger to mother, com-mander,  educated,  able,  social,  kind-hearted, affable, good-natured, many daughters and few sons, affable manners.",
                PlanetName.SATURN: "Narrow-minded, insignificant, no children,  perverted  views, tale-teller, royal displeasure, troubled life, clear-minded.",
                PlanetName.RAHU: "Childless, flatulent, tyrannical, polite, narrow-minded and cruel-hearted.",
                PlanetName.KETU: "Liberal, loss of children, sinful, immoral."
            },
            6: {
                PlanetName.SUN: "Defier of customs and castes, good administrative ability, few cousins and few enemies, bold and successful, war-like, licentious, wealthy, gains from enemies, clever in planning, terror to enemies, executive ability.",
                PlanetName.MOON: "Submissive to  the  fair  sex. indolent. imperious, short-tempered, intelligent. lazy  slender  body,  weak  sexual  connection. widow hunter, poor, drunkard, refined, tender, pilfering habits,  stomach  troubles, many foes, worried by cousins.",
                PlanetName.MARS: "successful, good lands, rich, success over enemies, intelligent, political success, powerful, worry from near relations.",
                PlanetName.MERCURY: "Respected,  interrupted  edu-cation,  subordinate  officer,  executive capacity, quarrelsome,  showy,  dissimulation,  losses  in money, peevish,  bigoted,  troubles in  the  feet and toes.",
                PlanetName.JUPITER: "Obscure,  unlucky, troubled, many cousins and grandsons,' dyspeptic, much jocularity, witty, unsuccessful, intelligent, foeless.",
                PlanetName.VENUS: "Very licentious, fdeless, loose habits,  anger, low-minded, well informed, des-truction to enemies, fond of other women.",
                PlanetName.SATURN: "Obstinate, sickly, deaf,  few children, quarrelsome, venereal diseases, debau-chery, clever, active, indebted.",
                PlanetName.RAHU: "Enjoyment,   venereal   com-plaints, no enemies, many cousins.",
                PlanetName.KETU: "Fond of adultery, good con-versationalist,  licentious, venereal  complaints, learned."
            },
            7: {
                PlanetName.SUN: "Late marriage  and  rather troubled, loose morals and irreligious, hated by the fair sex, fond of travelling, submissive to wife, wealth through female agency, fond  of foreign  things,  discontented,  wife's  character questionable, subservient to women and risk of dishonour and disgrace through them.",
                PlanetName.MOON: "Passionate, fond of women, handsome wife, blood-thirsty, mother short-lived. narrow-minded, good family, pains in the loins. social, successful, jealous and energetic in several matters.",
                PlanetName.MARS: "Two wives, dropsy, trouble with wife, rash, speculations, unsuccessful, intelli-gent, tactless, stubborn, idiosyncratic.",
                PlanetName.MERCURY: "Diplomatic, interesting lite-rary ability early in life and success through it, early marriage, wife handsome, dutiful and short-tempered ; gains through wife,  breaks in edu-cation,  learned  in astrology,  astronomy  and mathematics, good success in trade, successful, dashing, gay, skilful, religious, happy  mother and early death to her, cunning,  adulterous, charitable, strong body.",
                PlanetName.JUPITER: "Educated, proud, good wife and gains through her, diplomatic ability, specu-lative mind, very sensitive, success in agriculture, virtuous wife, pilgrimage to distant places.",
                PlanetName.VENUS: "Passionate, bad habits, mas-turbation and  sodomy, fond of others' wives, unhappy  marriage, adulterous, always enjoying sexual pleasure.",
                PlanetName.SATURN: "More than one wife, enter-prising, sickly, colic pains, deafness, diplomatic, unhappy marriage, ambitious, political success, travelling,	dependent,   dissimulator,	foreign honours, deputation.",
                PlanetName.RAHU: "Wife suffering from mens-trual  disorders,   widow   connection, diabetes, luxurious food, unhappy.",
                PlanetName.KETU: "Obscure debauchery,  pas-sionate, sinful, connections with widows, danger to wife."
            },
            8: {
                PlanetName.SUN: "Long life, uncharitable, sickly constitution, sudden gains, complaints in eyes, sores  in  the head, poor and uneventful life, narrow and obscure.",
                PlanetName.MOON: "Unhealthy, legacy, capricious, mother short-lived, few children, bilious, slender. bad sight, kidney disease,  unsteady, easy acqui-sitions.",
                PlanetName.MARS: "Short life, few children, dan-ger to maternal uncles,  widower later, concu-bines, hater of relations,  bad  sight,  fond  of prostitutes.",
                PlanetName.MERCURY: "Long life, landed estate, easy access to anything desired, grief through dome-stics,  obliging,  few issues, many lands, famous, respected, ill-health.",
                PlanetName.JUPITER: "Educated, proud, good wife and gains through her, diplomatic ability, specu-lative mind, very sensitive, success in agriculture, virtuous wife, pilgrimage to distant places.",
                PlanetName.VENUS: "Danger to  mother, happy, given to bad habits, short-lived, famous, cele-brated, unexpected legacies, trouble in mind, dis-appointment in love affairs, pious later in life.",
                PlanetName.SATURN: "Struggling, big belly, poor, obscure,  debauchery,  few  issues,  corpulent, drunkard, fond of servile women and work, colic pains, defect in sight, seductive, clever, well-informed, impious, danger by poisons, asthma, consumption, etc., if with malefics, dishonest, ungrateful .children, cruel, long life.",
                PlanetName.RAHU: "Vicious, degraded, quarrel-some, narrow-minded, immoral, adulterous.",
                PlanetName.KETU: "Senseless, obscure, dull, san-guine complexion."
            },
            9: {
                PlanetName.SUN: "Well read in solar sciences, attracted  by  sublime  phenomena,  charitable, godly, lucky and successful, devoted, ordinary: health, little patrimony, dutiful sons, a man of action and thought, self-acquired property, many lands, philosophical, glandular disease, lover of poetry and music, successful agriculturist, learned in esoteric and  occult subjects,  ambitious and enterprising.",
                PlanetName.MOON: "Popular, educated, intelligent. well read, lover of fiction, builder of charitable institutions, wealthy, active, inclined to travel, godly good children, many lands, religious. mys-tical, righteous, agricultural success, devotional. successful and good reputation.",
                PlanetName.MARS: "Cruel, worldly, successful tra-der, loss from. agriculture, sickly father, naval merchant,  dependent  life,  self-seeking, acute, stubborn, impetuous, logical.",
                PlanetName.MERCURY: "Highly  educated,  musician, many children, obliging, licentious, philosophical, lover  of literature, creative  mind,  inquisitive, scientific-minded, popular, well known.",
                PlanetName.JUPITER: "Charitable,  many   children, devoted, religious,  merciful,  pure; ceremonial-minded,   humanitarian   principles,  principled, conservative, generous, long-lived father, bene-volent,  God-fearing,  highly  cultured, famous, high position in the State.",
                PlanetName.VENUS: "Selfish, religious, respect for preceptors, able, successful, commander, lover of fine arts, generous.",
                PlanetName.SATURN: "Legal  success,  founder   of charitable  institutions, very miserly, thrifty in domestic life, scientific, irreligious, logical, cere-monial-minded, unfilial.",
                PlanetName.RAHU: "A puppet in the hands of the wife,  unpolite, uncharitable,  emaciated  waist, loose morals.",
                PlanetName.KETU: "Short-sighted, sinful, untruth-ful, thrifty, many children, good wife."
            },
            10: {
                PlanetName.SUN: "Bold, courageous, well known, famous,  clever  in  acquiring  wealth, superior knack, healthy, learned, adventurous, educated, quick decision, reckless of cost, fond of music, founder of institutions, high  position,  dutiful sons,  much  personal influence, successful mili-tary or political career.",
                PlanetName.MOON: "Persuasive, passionate. hunter after  widows,  charitable,  shrewd,  adulterous, bold, tactful, ambitious, great position, active. rusty of religious institutions, obliging to good people, many friends, easy success, popular and able, wealthy, comfortable and long life.",
                PlanetName.MARS: "Founder of institutions and towns, energetic, adventurous, wealthy, active, healthy, famous, self-made man, good agricul-turist, good profits, clever, successful, loved by relations, decisive.",
                PlanetName.MERCURY: "Determined, fortunate, enjoy-ments in life, intelligent, bad sight, active, cheer-ful, charitable, able, philanthropic.",
                PlanetName.JUPITER: "Virtuous, learned,  clever in acquisition  of  wealth,  conveyances,  children, determined,  accumulated  wealth,  founder   of towns, good agriculturist, non-violent, ambitious, scrupulous.",
                PlanetName.VENUS: "Respect for divine people and parents,  carriage,  broken education, successful as a lawyer, popular, social, moderate eater.",
                PlanetName.SATURN: "Visits to  sacred rivers  and shrines,   great   worker,   bilious,  good farmer, sudden elevations and depressions, residence in foreign countries, uncertain, later on in life an ascetic.",
                PlanetName.RAHU: "Adultery with widow, taste in poetry and   literature,   good artist, traveller, learned.",
                PlanetName.KETU: "Fertile brain, happy, religious, pilgrimages to sacred rivers and places, fond of scriptures."
            },
            11: {
                PlanetName.SUN: "Learned,  wealthy,  stately and persevering, success without effort, famous. many enemies, wealth through fair means, good reputation,  profound insight,  capacity  to be-friend, many political enemies, man of principles. great sagacity, great success and position.",
                PlanetName.MOON: "Many children, powerful, philanthropic, polite, literary and artistic taste, helpful,  influential, cultured, charitable,  many friends, great position, reputation, good lands, easy success, liked and helped by the fair sex, giver of donations, man of principles.",
                PlanetName.MARS: "Learned, educated, wealthy, , influential, property, crafty, happy, commanding.",
                PlanetName.MERCURY: "Wealthy,  happy,  mathe-matical faculty, good astrologer, many friends among famous men, many lands, logical and scientific, success in trade.",
                PlanetName.JUPITER: "Lover-of music, very weal-thy, statesmanly ability, good deeds, accumulated, unds, God-fearing, charitable, dependent, ,influ-ential, many friends, philanthropic.",
                PlanetName.VENUS: "Very influential,  learned, wealthy, good  conveyances,  successful, many friends, much popularity.  ",
                PlanetName.SATURN: "Learned, feared  and res-pected,  very  wealthy,  much  landed  property, broken education, conveyances, political success, influential, political respect.",
                PlanetName.RAHU: "Wealthy, influential among lower castes, many children, good agriculturist.",
                PlanetName.KETU: "Humorous, witty, licentious, intelligent, wealthy."
            },
            12: {
                PlanetName.SUN: "Sinful, poor, fallen,  thief, unsuccessful, adulterous among widows, neglect-ed, long limbs,  ceremonial-minded and lover of esoteric and occult knowledge.",
                PlanetName.MOON: "Obstructed, deformed, nar-row-minded, cruel, unhappy, obscure, powerless, deceived, solitary.",
                PlanetName.MARS: "Unsuccessful, poor, rotten body, unpopular,  incendiary,  habits,  diseases, suffering, stumbling, active, fruitless, liable to fraud and deception, dishonest, unseen impedi-ments.",
                PlanetName.MERCURY: "Philosophical,  intelligent, worried, adulterous, obliging, capricious, way-ward, narrow-minded, gifted, despondent, pas-sionate, few children, lacking in opportunities, danger to mother.",
                PlanetName.JUPITER: "Cruel, depraved, fallen, sin-ful, poor, few children, attracted by prostitutes, unlucky,  struggling,  life  lascivious,  later  life inclined  to  asceticism,  artistic taste, pious  in after-life.",
                PlanetName.VENUS: "Low-minded,  fond  of low women,  wealthy, miserly,  obscure,  licentious, unprincipled, weak eyes, fond of sexual pleasures, clever, liar, _pretentious, unhappy love affairs.",
                PlanetName.SATURN: "Deformed, 	squint   eyes, losses in trade, learned in  occult science, poor, spendthrift, many enemies, dexterous,' unpopular, attracted towards yoga in later life.",
                PlanetName.RAHU: "Deformed,  few  children, defective sight, very many losses, saintly.",
                PlanetName.KETU: "Capricious, unsettled mind, foreign residence, attracted to servile  classes, traveller, licentious."
            }
        }

    def __repr__(self) -> str:
        return str(self.number) + ": " + str(self.start_position) + '<->' + str(self.cusp_position) + '<->' + str(self.end_position)

    @property
    def number(self):
        return self._number

    @number.setter
    def number(self, value):
        self._number = value

    @property
    def sign(self) -> Sign:
        return self._sign

    @sign.setter
    def sign(self, value: int):
        self._sign = Sign(value)

    @property
    def areas_of_life(self):
        return self._areas_of_life.get(self.number)

    @property
    def resident_planets(self) -> list:
        return self._resident_planets

    @resident_planets.setter
    def resident_planets(self, planets: list):
        self._resident_planets = planets

    @property
    def num_of_residents(self) -> int:
        return len(self.resident_planets)

    @property
    def lord(self) -> Planet:
        return self._sign.lord

    def add_resident_planets(self, planets: list):
        self._resident_planets.extend(planets)

    def add_resident_planet(self, planet: Planet, strength: int):
        self._resident_planets.append((planet, strength))

    def has_resident_planet(self, planet) -> bool:
        if isinstance(planet, str):
            planet = PlanetName[planet]
        elif isinstance(planet, Planet):
            planet = planet.name
        elif isinstance(planet, PlanetName):
            planet = planet

        for record in self._resident_planets:
            if planet == record[0].name:
                return True

        return False

    @property
    def aspecting_planets(self):
        return self._aspecting_planets

    @aspecting_planets.setter
    def aspecting_planets(self, planets: list):
        self._aspecting_planets = planets

    def add_aspecting_planets(self, planets: list):
        self._aspecting_planets.extend(planets)

    def add_aspecting_planet(self, planet: Planet, strength: int):
        self._aspecting_planets.append((planet, strength))

    @property
    def start_position(self) -> Position:
        return self._start_position

    @start_position.setter
    def start_position(self, start_position: int):
        self._start_position = start_position

    @property
    def end_position(self) -> Position:
        return self._end_position

    @end_position.setter
    def end_position(self, end_position: int):
        self._end_position = end_position

    @property
    def cusp_position(self) -> Position:
        return self._cusp_position

    @cusp_position.setter
    def cusp_position(self, cusp_position: int):
        self._cusp_position = cusp_position

    @property
    def results(self):
        result_list = []
        if self.number == 1:
            result_list.append(self._lagna_results.get(self.sign))

        for planet, percent in self.resident_planets:
            result_list.append(self._house_results[self.number].get(planet.name))

        return result_list

