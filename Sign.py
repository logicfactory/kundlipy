from enum import IntEnum, Enum
from Planet import Planet, PlanetName


class Sign(IntEnum):
    #MESHA
    ARIES = 1

    #VRISHABHA
    TAURUS = 2

    #MITHUNA
    GEMINI = 3

    #KARK
    CANCER = 4

    #SIMHA
    LEO = 5

    #KANYA
    VIRGO = 6

    #TULA
    LIBRA = 7

    #VRISCHIKA
    SCORPIO = 8

    #DHANU
    SAGITTARIUS = 9

    #MAKAR
    CAPRICORN = 10

    #KUMBHA
    AQUARIUS = 11

    #MEEN
    PISCES = 12

    def __init__(self, value) -> None:
        super().__init__()
        self.sign_no = value
        print('Initializing sign number ', value)

        self._lords = {
            1: [PlanetName.MARS],
            2: [PlanetName.VENUS],
            3: [PlanetName.MERCURY],
            4: [PlanetName.MOON],
            5: [PlanetName.SUN],
            6: [PlanetName.MERCURY],
            7: [PlanetName.VENUS],
            8: [PlanetName.MARS, PlanetName.KETU],
            9: [PlanetName.JUPITER],
            10: [PlanetName.SATURN],
            11: [PlanetName.SATURN, PlanetName.RAHU],
            12: [PlanetName.JUPITER]
        }

    @property
    def start_position(self):
        return (self.value - 1) * 30

    @property
    def mid_position(self):
        return (self.value - 1) * 30

    @property
    def end_position(self):
        return (self.value - 1) * 30

    @property
    def lord(self):
        return self._lords[self.value]

    @property
    def move_type(self):
        if self.value % 3 == 1:
            return MoveType.MOVEABLE
        elif self.value % 3 == 2:
            return MoveType.FIXED
        elif self.value % 3 == 0:
            return MoveType.DUAL


class MoveType(IntEnum):
    MOVEABLE = 1
    FIXED = 2
    DUAL = 3





