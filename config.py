import math
import spiceypy as spice
from enum import Enum, IntEnum


places = {}
r2d = spice.dpr()


class PlanetNature(IntEnum):
    YOGKARAKA = 7
    BENEFIC = 1
    MALEFIC = -1
    NEUTRAL = 0


class PlanetState(IntEnum):
    EXALTED = 8
    MOOLTRIKON = 7
    IN_OWN_SIGN = 6
    IN_GREAT_FRIEND_SIGN = 5
    IN_FRIENDLY_SIGN = 4
    IN_NEUTRAL_SIGN = 3
    IN_ENEMY_SIGN = 2
    IN_GREAT_ENEMY_SIGN = 1
    DEBILITATED = 0

class PlanetRelation(IntEnum):
    GREAT_FRIEND = 2
    FRIEND = 1
    NEUTRAL = 0
    ENEMY = -1
    GREAT_ENEMY = -2


def degrees_to_decimal(degrees, minutes, seconds):
    return degrees + minutes/60 + seconds/60


def decimal_to_degrees(decimal):
    degree = math.floor(decimal)
    minutes_value = (decimal - degree) * 60
    minutes = math.floor(minutes_value)
    seconds = (minutes_value - minutes) * 60

    return degree, minutes, seconds


def add_longitudes(longitude1, longitude2):
    print('adding ', longitude1, longitude2)
    sum = longitude1 + longitude2
    if sum < 0:
        return sum + 360
    elif sum > 360:
        return sum - 360
    else:
        return sum


def degrees_between(from_degree, to_degree):
    if from_degree > to_degree:
        return 360 + to_degree - from_degree
    return to_degree - from_degree


def relationship_houses():
    a_array = [
        [2, "  Badey Mama/Maasi"],
        [6, "  Badey Mama/Maasi -> children"],
        [12, "  Badey Mama/Maasi -> elder sibling"],
        [10, "  Badey Mama/Maasi -> father"],
        [8, "  Badey Mama/Maasi -> spouse"],
        [5, "  children"],
        [11, "  children -> spouse"],
        [9, "  children -> spouse -> elder sibling"],
        [7, "  children -> spouse -> father"],
        [2, "  children -> spouse -> mother"],
        [1, "  children -> spouse -> younger sibling"],
        [6, "  chotey maama/maasis"],
        [10, "  chotey maama/maasis -> children"],
        [12, "  chotey maama/maasis -> spouse"],
        [5, "  Daada"],
        [7, "  Daada -> younger sibling"],
        [12, "  Daadi"],
        [11, "  elder sibling"],
        [3, "  elder sibling -> children"],
        [9, "  elder sibling -> children -> spouse"],
        [7, "  elder sibling -> grandchildren"],
        [5, "  elder sibling -> spouse"],
        [1, "  elder sibling -> spouse -> father"],
        [8, "  elder sibling -> spouse -> mother"],
        [7, "  elder sibling -> spouse -> younger sibling"],
        [9, "  father"],
        [2, "  father -> chotey maama/maasis"],
        [7, "  father -> elder sibling"],
        [11, "  father -> elder sibling -> children"],
        [1, "  father -> elder sibling -> spouse"],
        [11, "  father -> younger sibling"],
        [3, "  father -> younger sibling -> children"],
        [5, "  father -> younger sibling -> spouse"],
        [9, "  grandchildren"],
        [1, "  grandchildren -> children"],
        [3, "  grandchildren -> spouse"],
        [4, "  mother"],
        [5, "  mother -> Badey Mama/Maasi"],
        [12, "  Naana"],
        [10, "  Naana -> elder sibling"],
        [8, "  Naana -> father"],
        [3, "  Naana -> mother"],
        [6, "  Naana -> spouse"],
        [2, "  Naana -> younger sibling"],
        [7, "  Naani"],
        [10, "  Naani -> mother"],
        [9, "  Naani -> younger sibling"],
        [7, "  spouse"],
        [8, "  spouse -> Badey Mama/Maasi"],
        [12, "  spouse -> chotey maama/maasis"],
        [11, "  spouse -> Daada"],
        [6, "  spouse -> Daadi"],
        [5, "  spouse -> elder sibling"],
        [9, "  spouse -> elder sibling -> children"],
        [11, "  spouse -> elder sibling -> spouse"],
        [3, "  spouse -> father"],
        [1, "  spouse -> father -> elder sibling"],
        [5, "  spouse -> father -> younger sibling"],
        [10, "  spouse -> mother"],
        [6, "  spouse -> Naana"],
        [1, "  spouse -> Naani"],
        [9, "  spouse -> younger sibling"],
        [1, "  spouse -> younger sibling -> children"],
        [3, "  spouse -> younger sibling -> spouse"],
        [7, "  younger sibling -> children"],
        [1, "  younger sibling -> children -> spouse"],
        [11, "  younger sibling -> grandchildren"],
        [9, "  younger sibling -> spouse"],
        [7, "  younger sibling -> spouse -> elder sibling"],
        [5, "  younger sibling -> spouse -> father"],
        [12, "  younger sibling -> spouse -> mother"],
        [11, "  younger sibling -> spouse -> younger sibling"],
        [3, "  younger siblings"]

    ]


charkhandas = [[0, 0, 0, 0],
               [1, 12.6, 10.08, 4.38],
               [2, 25.2, 20.16, 8.4],
               [3, 37.8, 30.24, 12.6],
               [4, 50.4, 40.32, 16.8],
               [5, 63, 50.4, 21],
               [6, 75.6, 60.48, 25.2],
               [7, 88.2, 70.56, 29.4],
               [8, 101.4, 81.12, 33.78],
               [9, 114, 91.2, 37.98],
               [10, 127.2, 101.76, 42.36],
               [11, 139.8, 111.84, 46.56],
               [12, 153, 122.4, 51],
               [13, 162, 130.2, 52.8],
               [14, 179.4, 143.52, 59.76],
               [15, 192.6, 154.08, 64.2],
               [16, 206.4, 165.12, 68.76],
               [17, 219.6, 175.68, 73.2],
               [18, 234, 187.2, 78],
               [19, 247.8, 198.24, 82.56],
               [20, 262.2, 209.76, 87.36],
               [21, 276, 220.8, 91.98],
               [22, 291, 232.8, 96.96],
               [23, 305.4, 244.32, 101.76],
               [24, 320.4, 256.32, 106.8],
               [25, 335.4, 268.32, 111.78],
               [26, 351, 280.8, 117],
               [27, 366.6, 293.28, 122.16],
               [28, 382.8, 306.24, 127.56],
               [29, 399, 319.2, 132.96],
               [27, 366.6, 293.28, 122.16],
               [28, 382.8, 306.24, 127.56],
               [29, 399, 319.2, 132.96],
               [30, 415.8, 332.64, 138.6],
               [31, 432.6, 346.08, 145.98],
               [32, 450, 360, 150],
               [33, 467.4, 373.92, 155.76],
               [34, 485.4, 388.32, 161.76],
               [35, 504, 403.2, 168],
               [36, 522.6, 418.08, 174.18],
               [37, 542.4, 433.92, 180.78],
               [38, 562.2, 449.76, 187.38],
               [39, 583.2, 466.56, 192.24],
               [40, 603.6, 482.88, 201.18],
               [41, 625.8, 500.64, 208.38],
               [42, 648, 518.4, 216],
               [43, 671.4, 537.12, 223.8],
               [44, 694.8, 555.84, 231.6],
               [45, 720, 576, 240],
               [46, 745.2, 596.16, 248.4],
               [47, 772.2, 617.76, 257.4],
               [48, 799.8, 639.84, 266.58],
               [49, 828, 662.4, 276],
               [50, 858, 686.4, 285.96],
               [51, 889.2, 711.36, 296.4],
               [52, 921, 736.98, 307.02],
               [53, 955.2, 764.16, 318.36],
               [54, 991.2, 792.96, 330.36],
               [55, 1027.8, 822.24, 342.6],
               [56, 1067.4, 853.92, 355.8],
               [57, 1107.6, 887.04, 369.6],
               [58, 1152, 921.6, 384],
               [59, 1198.2, 958.56, 399.36],
               [60, 1246.8, 997.44, 415.56]]
