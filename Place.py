import math
from config import charkhandas
from Sign import Sign
from spiceypy import spiceypy


class Place:

    def __init__(self, name: str, latitude: float, longitude: float) -> None:
        self._name = name
        self._latitude = latitude
        self._longitude = longitude

    def __str__(self) -> str:
        return self._name

    def __repr__(self) -> str:
        return self._name

    @property
    def name(self) -> str:
        return self._name

    @property
    def latitude(self):
        return self._latitude

    @property
    def longitude(self):
        return self._longitude

    @property
    def get_rasimanas(self):

        latitude_floor = math.floor(self.latitude)
        latitude_ceil = math.ceil(self.latitude)
        print('checking charkhandhas for: ', latitude_floor, latitude_ceil)
        value1 = (charkhandas[latitude_floor][1] + charkhandas[latitude_ceil][1]) / 2
        value2 = (charkhandas[latitude_floor][2] + charkhandas[latitude_ceil][2]) / 2
        value3 = (charkhandas[latitude_floor][3] + charkhandas[latitude_ceil][3]) / 2

        print('value1: ', value1, 'value2: ', value2, 'value3: ', value3)

        if self.latitude > 0:
            direction_factor = 1
        else:
            direction_factor = -1

        value1 = 47.8782926548 * math.tan(self.latitude * spiceypy.rpd()) * 15
        value2 = 38.3874951743 * math.tan(self.latitude * spiceypy.rpd()) * 15
        value3 = 15.9918274024 * math.tan(self.latitude * spiceypy.rpd()) * 15

        print('value1: ', value1, 'value2: ', value2, 'value3: ', value3)
        print('direction_factor: ', direction_factor)

        return {Sign.ARIES: (1674 - value1 * direction_factor)*4,
                Sign.TAURUS: (1795 - value2 * direction_factor) * 4,
                Sign.GEMINI: (1931 - value3 * direction_factor) * 4,
                Sign.CANCER: (1931 + value3 * direction_factor) * 4,
                Sign.LEO: (1795 + value2 * direction_factor) * 4,
                Sign.VIRGO: (1674 + value1 * direction_factor)*4,
                Sign.LIBRA: (1674 + value1 * direction_factor)*4,
                Sign.SCORPIO: (1795 + value2 * direction_factor)*4,
                Sign.SAGITTARIUS: (1931 + value3 * direction_factor) * 4,
                Sign.CAPRICORN: (1931 - value3 * direction_factor)*4,
                Sign.AQUARIUS: (1795 - value2 * direction_factor) * 4,
                Sign.PISCES: (1674 - value1 * direction_factor) * 4
                }
        """
        return {Sign.ARIES: 7200,
                Sign.TAURUS: 7200,
                Sign.GEMINI: 7200,
                Sign.CANCER: 7200,
                Sign.LEO: 7200,
                Sign.VIRGO: 7200,
                Sign.LIBRA: 7200,
                Sign.SCORPIO: 7200,
                Sign.SAGITTARIUS: 7200,
                Sign.CAPRICORN: 7200,
                Sign.AQUARIUS: 7200,
                Sign.PISCES: 7200
                }
        """
