import math
import unittest
import spiceypy
from config import decimal_to_degrees, degrees_to_decimal


class Position:

    degrees_per_radian = spiceypy.dpr()

    def __init__(self, frame: str = 'J2000', x: float = 0, y: float = 0, z: float = 0):
        self._frame = frame
        self._x = x
        self._y = y
        self._z = z

    def __eq__(self, other):
        return self.ra == self.ra

    def __lt__(self, other):
        return self.ra < other.ra

    def __le__(self, other):
        return self.ra <= other.ra

    def __gt__(self, other):
        return self.ra > other.ra

    def __ge__(self, other):
        return self.ra >= other.ra

    def __add__(self, other):
        return Position(frame=self.frame, x=self._x+other._x, y=self._y+self._y, z=self._z+self._z)

    def __sub__(self, other):
        return Position(frame=self.frame, x=self._x-other._x, y=self._y-self._y, z=self._z-self._z)

    def __repr__(self) -> str:
        deg, min, sec = decimal_to_degrees(self.degrees_in_sign)
        return str(deg) + "' " + self.sign.name[:3] + ' ' + str(min) + "''"

    def __str__(self) -> str:
        deg, min, sec = decimal_to_degrees(self.degrees_in_sign)
        return str(deg) + "' " + self.sign.name[:3] + ' ' + str(min) + "''"

    def __copy__(self):
        print('copying Position')
        return Position(frame=self.frame, x=self._x, y=self._y, z=self._z)

    @property
    def frame(self):
        return self._frame

    @property
    def radius(self):
        return self.latitudinal_coordinates[0]

    @property
    def rectangular_coordinates(self):
        return self._x, self._y, self._z

    def from_latitudinal(self, frame: str, radius: float = 1, longitude: float = 0, latitude: float = 0):
        print('creating position from latitudinal parameters: ', frame, radius, longitude, latitude)
        quotient, remainder = divmod(longitude, 180)
        if quotient % 2 == 0:
            longitude = -remainder
        else:
            longitude = remainder

        quotient, remainder = divmod(latitude, 90)
        if quotient % 2 == 0:
            latitude = -remainder
        else:
            latitude = remainder

        self._frame = frame
        [self._x, self._y, self._z] = spiceypy.latrec(radius=radius, longitude=longitude/self.degrees_per_radian,
                                                      latitude=latitude/self.degrees_per_radian)
        return self

    @property
    def latitudinal_coordinates(self):
        distance, longitude, latitude = spiceypy.reclat([self._x, self._y, self._z])
        return distance, longitude*self.degrees_per_radian, latitude*self.degrees_per_radian

    @property
    def longitude(self):
        return self.latitudinal_coordinates[1]

    @longitude.setter
    def longitude(self, value):
        self.from_latitudinal(frame=self.frame, radius=self.radius, longitude=value, latitude=self.latitude)

    def add_longitude(self, degrees):
        print((self.longitude + degrees))
        quotient, remainder = divmod(self.longitude + degrees, 180)
        if quotient%2 == 0:
            return -remainder
        else:
            return remainder

    @property
    def latitude(self):
        return self.latitudinal_coordinates[2]

    @property
    def rad_coordinates(self):
        distance, ra, declination = spiceypy.recrad([self._x, self._y, self._z])
        return distance, ra*self.degrees_per_radian, declination*self.degrees_per_radian

    def from_rad_coordinates(self, frame: str = 'J2000', distance: int =10000, ra: float =0, declination: float=0):
        print('creating position from rad: ', frame, distance, ra, declination)
        if ra >=360:
            ra = ra % 360

        if declination >= 180:
            declination = declination % 180

        self._frame = frame

        [self._x, self._y, self._z] = spiceypy.radrec(distance, ra/self.degrees_per_radian, declination/self.degrees_per_radian)

        return self

    @property
    def distance(self):
        return self.rad_coordinates[0]

    @property
    def ra(self):
        return self.rad_coordinates[1]

    @ra.setter
    def ra(self, value):
        self.from_rad_coordinates(frame=self.frame, distance=self.distance, ra=value, declination=self.declination)

    def add_ra(self, degrees):
        self.ra = (self.ra + degrees) % 360
        return self

    @property
    def declination(self):
        return self.rad_coordinates[2]

    @property
    def sign(self):
        sign_num, remainder = divmod(self.ra, 30)
        from Sign import Sign

        return Sign(sign_num+1)

    @property
    def degrees_in_sign(self):
        return self.ra % 30

    @property
    def astrological_coordinates(self):
        return self.sign, self.degrees_in_sign

    def from_astrological_coordinates(self, sign, degrees_in_sign: float):
        return self.from_rad_coordinates(ra=((sign-1)*30)+degrees_in_sign)

    def copy(self):
        print('copying Position: ', self)
        return Position(frame=self.frame, x=self._x, y=self._y, z=self._z)

    @property
    def constellation(self):
        print('getting constellation for: ', self)
        quotient, remainder = divmod(self.ra * 27, 360)
        print('constellation num: ', quotient+1)
        from Constellation import Constellation
        for c in Constellation:
            print(c)
        return Constellation(quotient+1)


class TestPosition(unittest.TestCase):

    def setUp(self) -> None:
        super().setUp()

    def tearDown(self) -> None:
        super().tearDown()
