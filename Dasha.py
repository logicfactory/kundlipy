from datetime import datetime, date, time, timedelta
from DashaRow import DashaRow, DashaDepth
from Planet import PlanetName
from DateTime import DateTime
from Position import Position


class Dasha():

    def __init__(self) -> None:

        self._dashas = dict()

        self._dasha_order = {
            'vimshottri' : [
                PlanetName.SUN,
                PlanetName.MOON,
                PlanetName.MARS,
                PlanetName.RAHU,
                PlanetName.JUPITER,
                PlanetName.SATURN,
                PlanetName.MERCURY,
                PlanetName.KETU,
                PlanetName.VENUS
            ]
        }

        self._dasha_length = {
            'vimshottri': {
                PlanetName.SUN: 6,
                PlanetName.MOON: 10,
                PlanetName.MARS: 7,
                PlanetName.RAHU: 18,
                PlanetName.JUPITER: 16,
                PlanetName.SATURN: 19,
                PlanetName.MERCURY: 17,
                PlanetName.KETU: 7,
                PlanetName.VENUS: 20
            }
        }

    def get_dashas(self, dasha_type, start_position: Position(), date_of_birth: DateTime) -> list:
        self._dashas[dasha_type] = []
        self.generate_dashas(dasha_type=dasha_type, dasha_order=self._dasha_order.get(dasha_type),
                             start_position=start_position, date_of_birth=date_of_birth)
        return self.dashas.get(dasha_type)

    def generate_dashas(self, dasha_type:str, dasha_order: list, start_position: Position, date_of_birth: DateTime):

        constellation = start_position.constellation
        progress_in_constellation = start_position.ra % (360 / 27)
        ratio = progress_in_constellation * 27 / 360

        gone_dasha_years = ratio * self._dasha_length.get(dasha_type).get(constellation.lord)

        start_of_dasha = date_of_birth.date_time - timedelta(days=gone_dasha_years * 365.2425)
        print('start_of_dasha: ', start_of_dasha)

        dasha_total = 0

        for length in self._dasha_length.get(dasha_type).values():
            dasha_total += length

        self.add_dasha(dasha_type=dasha_type, dasha_order=dasha_order, dasha_depth=DashaDepth.MAHADASHA, planet=constellation.lord,
                       start_of_dasha=start_of_dasha, total_duration=timedelta(days=365.2425 * dasha_total))

    @property
    def dashas(self):
        return self._dashas

    def add_dasha(self, dasha_type, dasha_order, dasha_depth: DashaDepth, planet: PlanetName, start_of_dasha: DateTime,
                  total_duration: timedelta, parent: DashaRow=None):
        start_index = dasha_order.index(planet)
        print('start_index: ', start_index)

        dasha_total = 0

        for length in self._dasha_length.get(dasha_type).values():
            dasha_total += length

        for i in range(0, 9):
            next_index = ((start_index + i) % 9)
            next_planet = dasha_order[next_index]
            print(next_index, next_planet)

            duration = ((self._dasha_length.get(dasha_type).get(next_planet) * total_duration)/dasha_total)
            end_of_dasha = start_of_dasha + duration

            dasha = DashaRow(dasha_depth=dasha_depth, lord=next_planet, start=start_of_dasha, end=end_of_dasha,
                             parent=parent)

            self._dashas[dasha_type].append(dasha)

            if dasha_depth < 3:
                self.add_dasha(dasha_type=dasha_type, dasha_order=dasha_order, dasha_depth=DashaDepth(dasha_depth + 1), planet=next_planet,
                               start_of_dasha=start_of_dasha, total_duration=duration, parent=dasha)

            start_of_dasha = end_of_dasha
