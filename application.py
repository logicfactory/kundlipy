from datetime import datetime, timedelta
from flask import request, redirect, url_for, render_template, flash, g, Flask, session
import spiceypy as spice
import os
from Forms import PersonForm
from os import urandom
from Person import Person
from Place import Place
from Planet import PlanetName
from Constellation import Constellation
import csv
from DateTime import DateTime

print(spice.tkvrsn("TOOLKIT"))
spice.furnsh("KernelMeta.txt")

application = Flask(__name__)
application.secret_key = b'\x0c\x13\xf8\xc0\xbe\x18}\xab\x05\x1fv^\xbd\xb7\x9f\xc1\x12\x80S8b\xd0F\xc4'

cache = dict()


@application.route('/', methods=["GET", "POST"])
def home():
    form = PersonForm()
    print("form: ", form)

    if form.validate_on_submit():
        print("--------------form submitted---------------")

    if 'submit' in request.form:
        print("submit", form.name.data, form.DOB.data, form.TOB.data, form.latitude_of_birth.data,
              form.longitude_of_birth.data)
        person = Person(form.name, DateTime(date_time=str(form.DOB.data) + ' ' + str(form.TOB.data) + ' ' + form.timezone.data),
                        Place(form.place_of_birth, form.latitude_of_birth.data, form.longitude_of_birth.data))

        save_session(person)

        print('dob: ', person.datetime_of_birth, person.datetime_of_birth.utc)
        print(person.planets)

        name = form.name.data
        DOB = form.DOB.data
        TOB = form.TOB.data
        timezone = form.timezone.data
        place_of_birth = form.place_of_birth.data
        latitude = form.latitude_of_birth.data
        longitude = form.longitude_of_birth.data

        if form.save.data:
            save_person(name, DOB, TOB, timezone, place_of_birth, latitude, longitude)

        result_url = url_for('divisional_chart', number=1, name=name, DOB=DOB, TOB=TOB, timezone=timezone,
                               place_of_birth=place_of_birth, latitude=latitude, longitude=longitude)
        print('url: ', result_url)
        return redirect(result_url)

    return render_template("home.html", form=form)


def save_person(name, DOB, TOB, timezone, place_of_birth, latitude_of_birth, longitude_of_birth):
    print('saved', name, DOB, TOB, timezone, place_of_birth, latitude_of_birth, longitude_of_birth)
    with open('person_details','at') as fout:
        cout = csv.DictWriter(fout,['name','DOB','TOB','timezone','city','latitude','longitude'])
        cout.writerow({'name': name,'DOB': DOB, 'TOB': TOB, 'city':place_of_birth, 'latitude': latitude_of_birth,
                       'timezone': timezone, 'longitude': longitude_of_birth})


@application.route('/D<number>', methods=['GET', 'POST'])
def divisional_chart(number):
    print('request: ', request.args)
    print(number)

    name = request.args.get('name')
    DOB = request.args.get('DOB')
    TOB = request.args.get('TOB')
    timezone = request.args.get('timezone')
    place_of_birth = request.args.get('place')
    latitude = float(request.args.get('latitude'))
    longitude = float(request.args.get('longitude'))

    person = Person(name, DateTime(date_time=DOB + ' ' + TOB + ' ' + timezone), Place(place_of_birth, latitude, longitude))
    chart = person.charts.get('D'+number)
    return render_template("chart.html", person=person, chart=chart, planet_names=PlanetName)

@application.route('/summary', methods=['GET', 'POST'])
def summary():
    print('request: ', request.args)

    name = request.args.get('name')
    DOB = request.args.get('DOB')
    TOB = request.args.get('TOB')
    timezone = request.args.get('timezone')
    place_of_birth = request.args.get('place')
    latitude = float(request.args.get('latitude'))
    longitude = float(request.args.get('longitude'))

    person = Person(name, DateTime(date_time=DOB + ' ' + TOB + ' ' + timezone), Place(place_of_birth, latitude, longitude))
    return render_template("summary.html", person=person, planet_names=PlanetName)

@application.route('/adjust/<seconds>', methods=['GET', 'POST'])
def adjust(seconds):
    print(seconds)
    person = cache.get('person')
    new_date_time = DateTime()
    new_date_time.date_time = person.datetime_of_birth.date_time + timedelta(seconds=int(seconds))
    person = Person(person.name, new_date_time, person.place_of_birth)
    cache['person'] = person

    return render_template("result.html", person=person, planet_names=PlanetName)


def save_session(person):
    session['name'] = person.name
    session['DateTime'] = person.datetime_of_birth.date_time.strftime('%Y-%m-%d %H:%M %z')
    session['place'] = person.place_of_birth.name
    session['latitude'] = person.place_of_birth.latitude
    session['longitude'] = person.place_of_birth.longitude

    print('session saved: ', session)

    return True

def get_session():
    return Person(session.get('name'), DateTime(date_time=session.get('DateTime')), Place(session.get('place'),
                                                                                          session.get('latitude'),
                                                                                          session.get('longitude')
                                                                                          )
                  )


if __name__ == '__main__':
    # app.run()

    print(os.getcwd())


    # print(help(spice.spkpos))
    application.run()

    # Clean up the kernels
    spice.kclear()
