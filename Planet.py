import spiceypy as spice
from enum import Enum
from config import degrees_to_decimal, PlanetNature, PlanetState, PlanetRelation
from Position import Position


class PlanetName(Enum):
    SUN = 'SUN'
    MOON = 'MOON'
    MERCURY = 'MERCURY'
    VENUS = 'VENUS'
    MARS = 'MARS'
    JUPITER = 'JUPITER'
    SATURN = 'SATURN'
    RAHU = 'RAHU'
    KETU = 'KETU'

    def __init__(self, value) -> None:
        super().__init__()

        self._natural_benefic = ['JUPITER', 'VENUS']

        self._natural_malefic = ['MARS', 'SATURN', 'SUN', 'RAHU', 'KETU']

        self._male_planets = ['JUPITER', 'SUN', 'MARS']

        self._female_planets = ['VENUS', 'RAHU', 'MOON']

        self._eunuch_planets = ['SATURN', 'MERCURY', 'KETU']

        self._permanent_friends = {
            'SUN': ['MOON', 'MARS', 'JUPITER'],
            'MOON': ['SUN', 'MERCURY'],
            'MARS': ['SUN', 'MOON', 'JUPITER'],
            'MERCURY': ['SUN', 'VENUS'],
            'JUPITER': ['SUN', 'MOON', 'MARS'],
            'VENUS': ['MERCURY', 'SATURN'],
            'SATURN': ['MERCURY', 'VENUS']
        }

        self._permanent_neutrals = {
            'SUN': ['MERCURY'],
            'MOON': ['MARS', 'JUPITER', 'VENUS', 'SATURN'],
            'MARS': ['VENUS', 'SATURN'],
            'MERCURY': ['MARS', 'JUPITER', 'SATURN'],
            'JUPITER': ['SATURN'],
            'VENUS': ['MARS', 'JUPITER'],
            'SATURN': ['JUPITER']
        }

        self._permanent_enemies = {
            'SUN': ['SATURN', 'VENUS'],
            'MOON': [],
            'MARS': ['MERCURY'],
            'MERCURY': ['MOON'],
            'JUPITER': ['MERCURY', 'VENUS'],
            'VENUS': ['SUN', 'MOON'],
            'SATURN': ['SUN', 'MOON', 'MARS']
        }

    @property
    def nature(self):
        if self.name in self._natural_benefic:
            return PlanetNature.BENEFIC
        elif self.name in self._natural_malefic:
            return PlanetNature.MALEFIC

    @property
    def permanent_friends_with(self):
        return self._permanent_friends.get(self.name) or []

    @property
    def permanent_neutral_with(self):
        return self._permanent_neutrals.get(self.name) or []

    @property
    def permanent_enemies_with(self):
        return self._permanent_enemies.get(self.name) or []

    def get_permanent_relation_with(self, planet) -> PlanetRelation:
        print('checking friendship with: ', planet)

        if planet in self.permanent_friends_with:
            return PlanetRelation.FRIEND
        elif planet in self.permanent_enemies_with:
            return PlanetRelation.ENEMY
        elif planet in self.permanent_neutral_with:
            return PlanetRelation.NEUTRAL

        return PlanetRelation.NEUTRAL

    @property
    def exaltation_positions(self):
        print('getting exaltation position for: ', self.name)

        from Sign import Sign

        if self.name == 'SUN':
            return [
                Position().from_astrological_coordinates(sign=Sign.ARIES, degrees_in_sign=degrees_to_decimal(0, 0, 0)),
                Position().from_astrological_coordinates(sign=Sign.ARIES, degrees_in_sign=degrees_to_decimal(30, 0, 0))]
        elif self.name == 'MOON':
            return [
                Position().from_astrological_coordinates(sign=Sign.TAURUS, degrees_in_sign=degrees_to_decimal(0, 0, 0)),
                Position().from_astrological_coordinates(sign=Sign.TAURUS, degrees_in_sign=degrees_to_decimal(3, 0, 0))]
        elif self.name == 'MERCURY':
            return [
                Position().from_astrological_coordinates(sign=Sign.VIRGO, degrees_in_sign=degrees_to_decimal(0, 0, 0)),
                Position().from_astrological_coordinates(sign=Sign.VIRGO, degrees_in_sign=degrees_to_decimal(15, 0, 0))]
        elif self.name == 'VENUS':
            return [
                Position().from_astrological_coordinates(sign=Sign.PISCES, degrees_in_sign=degrees_to_decimal(0, 0, 0)),
                Position().from_astrological_coordinates(sign=Sign.PISCES,
                                                         degrees_in_sign=degrees_to_decimal(30, 0, 0))]
        elif self.name == 'MARS':
            return [Position().from_astrological_coordinates(sign=Sign.CAPRICORN,
                                                             degrees_in_sign=degrees_to_decimal(0, 0, 0)),
                    Position().from_astrological_coordinates(sign=Sign.CAPRICORN,
                                                             degrees_in_sign=degrees_to_decimal(30, 0, 0))]
        elif self.name == 'JUPITER':
            return [
                Position().from_astrological_coordinates(sign=Sign.CANCER, degrees_in_sign=degrees_to_decimal(0, 0, 0)),
                Position().from_astrological_coordinates(sign=Sign.CANCER,
                                                         degrees_in_sign=degrees_to_decimal(30, 0, 0))]
        elif self.name == 'SATURN':
            return [
                Position().from_astrological_coordinates(sign=Sign.LIBRA, degrees_in_sign=degrees_to_decimal(0, 0, 0)),
                Position().from_astrological_coordinates(sign=Sign.LIBRA, degrees_in_sign=degrees_to_decimal(30, 0, 0))]
        elif self.name == 'RAHU':
            return [
                Position().from_astrological_coordinates(sign=Sign.TAURUS, degrees_in_sign=degrees_to_decimal(0, 0, 0)),
                Position().from_astrological_coordinates(sign=Sign.TAURUS,
                                                         degrees_in_sign=degrees_to_decimal(30, 0, 0))]
        elif self.name == 'KETU':
            return [Position().from_astrological_coordinates(sign=Sign.SCORPIO,
                                                             degrees_in_sign=degrees_to_decimal(0, 0, 0)),
                    Position().from_astrological_coordinates(sign=Sign.SCORPIO,
                                                             degrees_in_sign=degrees_to_decimal(30, 0, 0))]

    @property
    def mooltrikon_positions(self):
        print('getting mooltrikon position for: ', self)

        from Sign import Sign

        if self.name == 'SUN':
            return [
                Position().from_astrological_coordinates(sign=Sign.LEO, degrees_in_sign=degrees_to_decimal(0, 0, 0)),
                Position().from_astrological_coordinates(sign=Sign.LEO, degrees_in_sign=degrees_to_decimal(20, 0, 0))]
        elif self.name == 'MOON':
            return [
                Position().from_astrological_coordinates(sign=Sign.TAURUS, degrees_in_sign=degrees_to_decimal(3, 0, 0)),
                Position().from_astrological_coordinates(sign=Sign.TAURUS,
                                                         degrees_in_sign=degrees_to_decimal(30, 0, 0))]
        elif self.name == 'MERCURY':
            return [
                Position().from_astrological_coordinates(sign=Sign.VIRGO, degrees_in_sign=degrees_to_decimal(15, 0, 0)),
                Position().from_astrological_coordinates(sign=Sign.VIRGO, degrees_in_sign=degrees_to_decimal(20, 0, 0))]
        elif self.name == 'VENUS':
            return [
                Position().from_astrological_coordinates(sign=Sign.LIBRA, degrees_in_sign=degrees_to_decimal(0, 0, 0)),
                Position().from_astrological_coordinates(sign=Sign.LIBRA, degrees_in_sign=degrees_to_decimal(15, 0, 0))]
        elif self.name == 'MARS':
            return [
                Position().from_astrological_coordinates(sign=Sign.ARIES, degrees_in_sign=degrees_to_decimal(0, 0, 0)),
                Position().from_astrological_coordinates(sign=Sign.ARIES, degrees_in_sign=degrees_to_decimal(12, 0, 0))]
        elif self.name == 'JUPITER':
            return [Position().from_astrological_coordinates(sign=Sign.SAGITTARIUS,
                                                             degrees_in_sign=degrees_to_decimal(0, 0, 0)),
                    Position().from_astrological_coordinates(sign=Sign.SAGITTARIUS,
                                                             degrees_in_sign=degrees_to_decimal(10, 0, 0))]
        elif self.name == 'SATURN':
            return [Position().from_astrological_coordinates(sign=Sign.AQUARIUS,
                                                             degrees_in_sign=degrees_to_decimal(0, 0, 0)),
                    Position().from_astrological_coordinates(sign=Sign.AQUARIUS,
                                                             degrees_in_sign=degrees_to_decimal(20, 0, 0))]
        elif self.name == 'RAHU':
            return [
                Position().from_astrological_coordinates(sign=Sign.LEO, degrees_in_sign=degrees_to_decimal(0, 0, 0)),
                Position().from_astrological_coordinates(sign=Sign.LEO, degrees_in_sign=degrees_to_decimal(30, 0, 0))]
        elif self.name == 'KETU':
            return [
                Position().from_astrological_coordinates(sign=Sign.AQUARIUS, degrees_in_sign=degrees_to_decimal(0, 0, 0)),
                Position().from_astrological_coordinates(sign=Sign.AQUARIUS,
                                                         degrees_in_sign=degrees_to_decimal(30, 0, 0))]

    @property
    def debilitation_positions(self):
        print('getting debilitation position for: ', self)

        from Sign import Sign

        if self.name == 'SUN':
            return [
                Position().from_astrological_coordinates(sign=Sign.LIBRA, degrees_in_sign=degrees_to_decimal(0, 0, 0)),
                Position().from_astrological_coordinates(sign=Sign.LIBRA, degrees_in_sign=degrees_to_decimal(30, 0, 0))]
        elif self.name == 'MOON':
            return [Position().from_astrological_coordinates(sign=Sign.SCORPIO,
                                                             degrees_in_sign=degrees_to_decimal(0, 0, 0)),
                    Position().from_astrological_coordinates(sign=Sign.SCORPIO,
                                                             degrees_in_sign=degrees_to_decimal(30, 0, 0))]
        elif self.name == 'MERCURY':
            return [
                Position().from_astrological_coordinates(sign=Sign.PISCES, degrees_in_sign=degrees_to_decimal(0, 0, 0)),
                Position().from_astrological_coordinates(sign=Sign.PISCES,
                                                         degrees_in_sign=degrees_to_decimal(30, 0, 0))]
        elif self.name == 'VENUS':
            return [
                Position().from_astrological_coordinates(sign=Sign.VIRGO, degrees_in_sign=degrees_to_decimal(0, 0, 0)),
                Position().from_astrological_coordinates(sign=Sign.VIRGO, degrees_in_sign=degrees_to_decimal(30, 0, 0))]
        elif self.name == 'MARS':
            return [
                Position().from_astrological_coordinates(sign=Sign.CANCER, degrees_in_sign=degrees_to_decimal(0, 0, 0)),
                Position().from_astrological_coordinates(sign=Sign.CANCER,
                                                         degrees_in_sign=degrees_to_decimal(30, 0, 0))]
        elif self.name == 'JUPITER':
            return [Position().from_astrological_coordinates(sign=Sign.CAPRICORN,
                                                             degrees_in_sign=degrees_to_decimal(0, 0, 0)),
                    Position().from_astrological_coordinates(sign=Sign.CAPRICORN,
                                                             degrees_in_sign=degrees_to_decimal(30, 0, 0))]
        elif self.name == 'SATURN':
            return [
                Position().from_astrological_coordinates(sign=Sign.ARIES, degrees_in_sign=degrees_to_decimal(0, 0, 0)),
                Position().from_astrological_coordinates(sign=Sign.ARIES, degrees_in_sign=degrees_to_decimal(30, 0, 0))]
        elif self.name == 'RAHU':
            return [Position().from_astrological_coordinates(sign=Sign.SCORPIO,
                                                             degrees_in_sign=degrees_to_decimal(0, 0, 0)),
                    Position().from_astrological_coordinates(sign=Sign.SCORPIO,
                                                             degrees_in_sign=degrees_to_decimal(30, 0, 0))]
        elif self.name == 'KETU':
            return [
                Position().from_astrological_coordinates(sign=Sign.TAURUS, degrees_in_sign=degrees_to_decimal(0, 0, 0)),
                Position().from_astrological_coordinates(sign=Sign.TAURUS,
                                                         degrees_in_sign=degrees_to_decimal(30, 0, 0))]


class PlanetAge(Enum):
    CHILD = 'CHILD'
    ADOLESCENT = 'ADOLESCENT'
    YOUTH = 'YOUTH'
    OLD = 'OLD'
    DEAD = 'DEAD'


class Planet:

    def __init__(self, name: PlanetName, position: Position = Position()):
        super().__init__()
        if isinstance(name, PlanetName):
            self._name = name
        elif isinstance(name, str):
            self._name = PlanetName[name]
        else:
            raise Exception('invalid type for name')

        self._position = position

        self._planet_sign_results = {
            PlanetName.SUN: {
                1: "Active, intelligent famous, traveller, wealthy, warrior, variable fortune, ambitious, phlegmatic, "
                   "powerful, marked  per-sonality, impulsive, irritable, pioneering, initiative.",
                2: "Clever, reflective, attracted by per-fumes and dealer in them, hated by women, slow to action, "
                   "musician, self-confident, delicious drinks, happy meal's, tactful, original, sociable, intelligent,"
                   " prominent nose.",
                3: "Learned, astronomer, scholarly, grammarian, polite, wealthy, critical, assimilative, good "
                   "conversationalist, shy, reserved, lacking in originality.",
                4: "Poor, cruel, indolent, sorrowful, un-happy,  constipation, sickly, sodomy, travelling, servile "
                   "mentality.",
                5: "Stubborn, strong, poor, cruel, inde-pendent, organising capacity and talents for propaganda, "
                   "humanitarian, frequenting solitary places, generous.",
                6: "Linguist poet, mathematician, taste for literature, well read, scholarly, artistic, good memory, "
                   "reasoning  faculty,  effeminate body, frank, lucid comprehension, learned in religious lore, "
                   "reserved, wanting adulation.",
                7: "Manufacturer of liquors,  popular, tactless, base, drunkard, loose morals, arrogant, wicked, frank,"
                   " submissive, pompous.",
                8: "Adventurous, bold, fearing thieves and robbers, reckless, cruel, stubborn, unprin-cipled. "
                   "impulsive, idiotic, indolent, surgical skill, dexterous, military ability.",
                9: "short-tempered, spoils, reliable, rich, obstinate, respected by all, happy, popular, religious, "
                   "wealthy, musician.",
                10: "Mean-minded, stubborn,  igno-rant,  miserly, pushful, unhappy , boring, active, meddlesome, "
                    "obliging, humorous, witty, affable, prudent, firm.",
                11: "Poor, unhappy,  stubborn,  un-lucky, unsuccessful, medium height, rare facul-ties, self-esteem.",
                12: "Pearl merchant, peaceful, wealthy, uneventful, religious, prodigal, loved by women."
            },
            PlanetName.MOON: {
                1: "Round eyes, impulsive, fond of travel, irritable, fond of women, vegetable diet, quick to decide "
                   "and act, haughty, inflexible, sores in the head, dexterous, fickle-minded, war-like, enterprising, "
                   "subordinate in  position, self-respect, valiant, ambitious, liable to hydrophobia if the Moon is "
                   "afflicted, large thighs, popular, restless, idiosyncratic, a versatile.",
                2: "Liberal, powerful, happy, ability to command, intelligent, handsome, influential, fond of the fair "
                   "sex, happy in middle life and old age, great strides in life, beautiful gait, large thighs, and "
                   "hips, phlegmatic afflictions, rich patience, respected, love intrigues,  inconsistent, wavering "
                   "mind,  sound  judgment,  voracious  eater  and reader,  lucky, popular,  influenced  by  women, "
                   "passionate, indolent.",
                3: "Well read, creative, fond of women, learned in scriptures, able, persuasive, curly hair,. powerful "
                   "speaker, clever, witty, dexterous, fond of music, elevated nose, thought reader, subtle, long life.",
                4: "Wise, powerful, charming,  influ-enced by women, wealthy, kind, good, a bit stout, sensitive, "
                   "impetuous, unprofitable voyages,. meditative, much immovable property, scientist, middle stature, "
                   "prudent, frugal, piercing, con-ventional.",
                5: "Bold, irritable, large cheeks, blonde, broad face, brown eyes, repugnant to women, meat eater,  "
                   "frequenting forests and hills, colic troubles, inclined to be unhappy, haughty, mental anxiety, "
                   "liberal, generous, deformed body, steady, aristocratic, settled views, proud, ambitious.",
                6: "Lovely complexion,  almond  eyes, gait through modesty,  sunken  shoulders  and arms,  charming,  "
                   "attractive, principled, affluent, comfortable, soft body, sweet speech,  honest, truthful, modest, "
                   "virtuous, intelligent, phlegmatic, fond of women, acute insight, conceited in self-estimation, "
                   "pensive, conversationalist, many daughters, loquacious, astrologer and claiivoyant or attracted "
                   "towards them, skilled in arts like music and dancing, few sons.",
                7: "Reverence and respect for learned and holy people, saints and gods, tall, raised nose, thin, "
                   "deformed limbs, sickly constitution, rejected  by  kinsmen,   intelligent,  principled, wealthy, "
                   "businesslike, obliging,   love  for arts, far-seeing, idealistic, clever, mutable, amicable, losses"
                   " through women, loves women, just, not ambitious, aspiring.",
                8: "Broad  eyes,  wide  chest, round shanks and thighs, isolation from parents  or preceptors, brown "
                   "complexion, straight-forward, frank, open-minded, cruel, simulator, malicious, abortion, sterlity, "
                   "agitated, unhappy,  wealthy, impetuous, obstinate, immoral.",
                9: "Face broad, teeth large, skilled in fine arts, indistinct shoulders, disfigured nails and arms, "
                   "deep and inventive intellect, yielding to praise, good speech,  upright, help from wife and women, "
                   "happy marriage, many  children, good inheritance, benefactor, patron of arts and literature, "
                   "ceremonial-minded, showy, unexpected gifts,  author,  reflective mentality, inflexible to threats.",
                10: "Ever attached to wife and child-ren, virtuous, good eyes, slender waist, quick in perception, "
                    "clever, active, crafty, _selfish, saga-cious, strategic, liberal, merciless, unscrupulous, "
                    "inconsistent, wandering, sexual contact with low women, niggard, shameless, base and mean..",
                11: "Camers  neck,  muscular built, rough skin with hair, tall, large teeth, big buttocks and thighs, "
                    "belly- low, deaf, adulterous,- sudden elevations  and depressions, unhappy, beautiful minded, "
                    "artistic, intentional, inclined to hypo-crisy,  inoffensive, strange brain, artistic taste, "
                    "energetic, emotion, esoteric, liable to fraud and deception, mystical, grateful, debaucherous.",
                12: "Fixed, dealer in pearls and fond of wife and children, perfect build, long nose, bright body, "
                    "annihilating enemies, subservient to fair sex,  handsome, learned, steady,  simple, good "
                    "reputation, loose  morals,  adventurous,  many children, spiritually inclined later in life."
            },
            PlanetName.MARS: {
                1: "Organising capacity, com-manding, rich, social, scars in the body, too sensual, dark, "
                   "mathematician, active, powerful, inspiring, pioneering, able, statesmanly, frank, generous, "
                   "careful, not economical in domestic dealings, vague imaginations, combative tenden-cies, cruel.",
                2: "Influenced by women, timid, rough body,  stubborn, adulterous, able in magic and sports, loose, "
                   "unprincipled, selfish,  tyrannical, hard-hearted, fond of others' wives, rash, emo-tional, animal "
                   "instinct, sensitive.",
                3: "Loving family and children, taste in refinement, scientific, middle stature, well built, learned, "
                   "ambitious, quick, rash, ingenious, skilled in music,  fearless,  tactless,  peevish,  unhappy, "
                   "subservient,  diplomatist,  humiliating,  detective.",
                4: "Intelligent,  wealthy,  rich,  travels and voyages, wicked, perverted, love of agricul-ture, "
                   "stupid,  medical  and surgical proficiency, fickle-minded,  defective  sight,  bold,  dashing, "
                   "headlong, speculative, cruel, egoistic.",
                5: "Tendency  to   occultism,   astrology, astronomy and  mathematics, love for parents, regard and "
                   "respect for elders and  preceptors: independent thinking, peevish, extravagant, vic-torious, "
                   "stomach troubles, worried  by  mental complaints,  generous,  noble,  miserly,   author early in "
                   "life, carried by others, few children, suc-cessful, combative, restless.",
                6: "Imitable, explosive, trouble in diges-tive organs, no marital harmony, general love for the fair "
                   "sex, revengeful, self-confident, conceited, affable, boastful, materialistic, ceremonial-minded, "
                   "positive, indiscriminative, pretentious, deceptive, scientific enterprise.",
                7: "Tall, body symmetrically built, com-plexion fair and  swarthy,  ambitious, self-confi-dent, "
                   "perceptive faculties,  materialistic, love for family, self-earned wealth, affable, warlike, "
                   "fore-sighi, businesslike, deceived by women, sanguine temperament, kind, gentle, fond of adulation,"
                   " easily  ruffled,  boastful.",
                8: "Middle stature, clever, diplomatic, positive tendency, indulgent, tenacious memory, malicious,  "
                   "aggressive,  proud,   haughty,   great strides in life.",
                9: "Gentlemanly, many  foes,  fa-mous, minister, statesman, open, frank, pleasure loving, few children,"
                   " liable to extremes, conser-vative,  indifferent, exacting, impatient,  severe, quarrelsome, "
                   "litigation troubles, good  citizen.",
                10: "Rich,  high  political   position, many  sons, brave, generous, love for children, middle stature,"
                    " industrious,  indefatigable, suc-cessful, penetrating, bold, tactful, respected, gene-rous, "
                    "gallant, influential.",
                11: "Unhappy, miserable, poor, liar, independent, wicked, wandering, impulsive, con-troversial, "
                    "combative, well-versed in dialects, free, quick in forgiving and forgetting, conventional, danger "
                    "on water, morose, meditative.",
                12: "Fair complexion,  troubles in love affairs, few children, passionate, sterile, restless, "
                    "antagonistic,  exacting,  uncertainty  of feeling, faithful, unclean, colic, indolent, wilful."
            },
            PlanetName.MERCURY: {
                1: "Poor, ignorant, middle stature, obstinate, clever, social, great endurance, materialistic "
                   "tendencies,   unscrupulous,	liar, idiotic, swindler, antagonistic, fond of gambling, almost an "
                   "atheist,  impulsive, greedy, dangerous connections,  deceitful,  swerving from rectitude.",
                2: "Minister,   corpulent,   well   built, clever, logical, mental harmony,  many children, liberal, "
                   "persevering,	obstinate,	opinionative, wealthy, practicable, friends among women of eminence, "
                   "inclination to sensual pleasures, well read, showy, many wives.",
                3: "nclination   to   physical  labour, boastful, sweet speech, tall, active, cultured, tact-ful, "
                   "dexterous, two mothers, indolent, inventive, taste in literature, arts and sciences,  winning "
                   "manners, liable to throat and bronchial troubles, musician, good grammarian, comfortable, "
                   "mirth-ful, studious.",
                4: "Witty,  trade in pearls, disliked by relations,  low stature, speculative,  diplomatic, discreet, "
                   "flexible, restless, religious, liable to con-sumption, strong parental love, dislike for chastity.",
                5: "Poor, few children, wanderer, idiotic, confessed, proud, indolent, fond of women, tall, boastful, "
                   "orator,  good  memory, two mothers, early marriage, independent in thinking,  sub-servient to  "
                   "women, strongly impulsive, positive will, remunerative profession.",
                6: "Learned, virtuous, liberal, fearless, ingenious, handsome, irritable, refined, subtle, intuitive, "
                   "sociable, no self-control, morbid imagi-nations, dyspeptic difficulties, eloquent, author, priest, "
                   "astronomer.",
                7: "Fair complexion, sanguine disposi-tion, inclination to excesses, perceptive faculties, material "
                   "tendencies, frugal, agreeable, courteous, philosophical, faithful, ceremonial-minded, sociable, "
                   "discreet.",
                8: "Short,  curly  hair,  incentive  to indulgence, liable to disease of the generative organ, general "
                   "debility, crafty, malicious, selfish, subtle, indiscreet, bold, reckless.",
                9: "Taste in sciences, respected by polished society, tall, well built, learned, rash, superstitious, "
                   "vigorous,  executive,  diplomatic, cunning, just, capable.",
                10: "Selfless, business tendencies, eco-nomical,  debtor,   inconsistent,   extreme,   low stature, "
                    "cunning, cruel, inventive, active, restless, suspicious, drudging.",
                11: "Middle stature, licentious, proud, frank,  sociable, rapid strides in  life,  famous, scholar.",
                12: "Mean, ignorant, servile, dexterous, despised, peevish, indolent, filthy, flow-minded."
            },
            PlanetName.JUPITER: {
                1: "Love of grandeur, power-ful, wealthy, prudent, many children, courteous, generous, firm, "
                   "sympathetic,  happy  marriage, patient nature, harmonious, refined.",
                2: "Stately,  elegant,   self-importance, extravagant,  free from diseases, liberal, dutiful sons, "
                   "just, sympathetic, well read, creative ability, despotic,  healthy, happy marriage, liked by all, "
                   "inclination to self-gratification.",
                3: "Oratorical ability, tall, well-built, benevolent,  comfortable,  scholarly,  sagacious, diplomatic,"
                   " linguist, elegant, incentive.",
                4: "Well read, dignified, wealthy, com-fortable, intelligent, swarthy complexion, inclined to social "
                   "gossip, mathematician, faithful.",
                5: "Commanding appearance, tall, great, easily offended, ambitious, active, happy, intelli-gent,  wise,  prudent,   magnanimous, generous, broad-minded, literary, harmonious surroundings, musician.",
                6: "Middle  stature,  bombastic,  ambi-tious, selfish,  stoical  resignation,  affectionate, fortunate, stingy, lovable, a beautiful wife, great endurance, learned.",
                7: "Handsome, free  open-minded,  at-tractive, just, courteous, strong, able, exhaustion from   over-activity,   competent,   unassuming, pleasing.",
                8: "Tall, hunchback, majestic appear-ance, elegant manners, serious, exacting, well built,  superior  airs,  selfish,  help from wife, imprudent, subservient to women, passionate, conventional,  proud,  exalted  ideals, zealous, ceremonious.   ",
                9: "Pretty inheritance,  rich, influ-ential,   handsome,   noble,   noteworthy,  trust-worthy, great friends, a big zamindar, extreme executive ability, weak constitution, artistic quali-ties, poetic, open-minded, good conversationalist.",
                10: "Tactless,   good intention,  dis-. graceful,   demoralisation,  initiative,  unhappy, irritable, inconsistent, avaricious.",
                11: "Learned,  controversial specula-tive, philosophical, popular, compassionate, sym-pathetic, amiable, prudent, humanitarian, melan-cholic,  meditative,  intentional, dreamy, active, much respect.",
                12: "Good inheritance, stout,  medium height,  two marriages if with malefics,  enter-prising, political diplomacy."
            },
            PlanetName.VENUS: {
                1: "Extravagant, loose morals, disgraceful character,  active, mutable, artistic, dreamy, idealist, proficient in music, art, poetry, licentious,   sorrowful,   fickle-minded,  prudent, unhappy too indulgent, irreligious, easy going, selfish.",
                2: "Well   built,  handsome,  pleasing countenance,  independent, sensational, love of nature, fond of pleasure, elegant, taste in dancing and music, voluptuous.",
                3: "Rich, gentle, kind, generous, elo-quent, proud,  respected,  gullible, love of fine arts, learned, intelligent, good logician, just. dual marriage, tendencies towards materialism.",
                4: "Melancholy. 	emotional, 	timid, haughty, sorrowful, light character, inconsistent, unhappy, many children, sensitive.",
                5: "Money   through   women,   excellent wife, wayward, few sons, conceited, passionate, fair complexion, emotional, zealous, licentious, subservient to women, attracted by the fair sex, premature in conclusions, haughty, superior airs.",
                6: "Petty-minded, licentious, unscrupu-lous, sensuous, unhappy, illicit love, agile, mean. unsuccessful, loquacious.",
                7: "Statesman, poet, intelligent,  gene-rous, philosophical, handsome, matrimonial feli-city, successful marriage, passionate, proud, res-pected, intuitive, sensual.",
                8: "Broad  features,   short  statured, ignoble,   independent,   artistic,  unjust,  proud, disappointed in love, haughty.",
                9: "Med ium	height,	powerful, wealthy, respected, impertinent, generous, chival-rous, frank.",
                10: "Fond of mean  women, impru-dent,  powerful,  ambitious,  unprincipled, licen-tious, adulterous, boastful, subtle, learned.",
                11: "Liked  by  all,   middle  stature, amusing, handsome,  affable,  persuasive,  witty, effeminate, timid, chaste, calm.",
                12: "Witty, tactful, learned, popular, just, ingenious, caricaturist,•modest, refined, powerful, exalted, much respected, dyspeptic difficulties."
            },
            PlanetName.SATURN: {
                1: "Idiotic, unfortunate, sor-rowful,   wanderer,   peevish,   resentful,  cruel, fraudulent, immoral, ignorant,  waster, boastful, quarrelsome, dissembler, gloomy,  mischievous, perverse.",
                2: "Dark complexion,  deceitful,  suc-cessful, powerful, irreligious, obscure debauchery, clever,   solitary,   voracious  eater,   persuasive, incestuous intercourse, contagious diseases, many wives.",
                3: "Shamelss, miserable, liar, lunatic, original, thin, subtle, ingenious, strategic, few children,  taste  for  chemical  and  mechanical sciences,   narrow-minded,  speculative, logical, desperado.",
                4: "Poor,   few   sons, 	unsuccessful, cheeks full,   slow, dull, cunning, selfish, deceit-ful, malicious, beggar, stubborn.",
                5: "Middle stature, severe, obstinate, few sons,  stubborn,  menial  mentality, unfortunate, conflicting, hard worker.",
                6: "Dark complexion,  malicious, poor, shameless, erratic, narrow=minded, unscrupulous, conservative, taste for public life, invention.",
                7: "Famous, founder of institutions and the like, rich, tall, fair, self-conceited, handsome, tactful, powerful, respected,  sound  judgment, antagonistic,   independent,  proud,  prominent, subservient to females.",
                8: "Rash, indifferent, merciless, adven-turous,  mean, self-conceited, reserved, unscru-pulous, violent, unhappy.",
                9: "Pushful, artful, cunning, peace-ful, death, faithful, pretentious, apparently gene-rous, troubles with wife, courteous.",
                10: "Intelligent, harmony and felicity in domestic life, selfish, covetous, peevish, intel-lectual,  learned,  suspicious, reflective, revenge-ful, prudent, melancholy.",
                11: "Adultery with other's wife, practi-cal,  able, diplomatic, ingenious, a bit conceited. prudent,  happy, reflective,  intellectual, philoso-phical.",
                12: "Clever, pushful, gifted, happy, good wife,	untrustworthy,   dissembler,	scheming, wealthy, severe, malicious."
            }
        }

    def __repr__(self) -> str:
        return str(self._name) + " at " + str(self._position.astrological_coordinates)

    @property
    def name(self):
        return self._name

    @name.setter
    def name(self, name: PlanetName):
        self._name = name

    @property
    def position(self) -> Position:
        return self._position

    @position.setter
    def position(self, position: Position):
        self._position = position

    @property
    def state(self):
        # if exalted
        print('getting state for: ', self, self.position)
        if self.exaltation_positions[0] <= self.position <= self.exaltation_positions[1]:
            print('returnig EXALTED')
            return PlanetState.EXALTED

        elif self.mooltrikon_positions[0] <= self.position <= self.mooltrikon_positions[1]:
            print('returnig MOOLTRIKON')

            return PlanetState.MOOLTRIKON

        # if in own raashi
        elif self.position.sign.lord == self.name:
            print('returnig IN_OWN_SIGN')

            return PlanetState.IN_OWN_SIGN

        # if debilitated
        elif self.debilitation_positions[0] <= self.position <= self.debilitation_positions[1]:
            print('returnig DEBILITATED')

            return PlanetState.DEBILITATED

        else:
            print('returnig None')

            return None

    @property
    def age(self):
        if self.position.sign.value % 2 == 1:
            if divmod(self.position.degrees_in_sign, 6)[0] == 0:
                return PlanetAge.CHILD
            elif divmod(self.position.degrees_in_sign, 6)[0] == 1:
                return PlanetAge.ADOLESCENT
            elif divmod(self.position.degrees_in_sign, 6)[0] == 2:
                return PlanetAge.YOUTH
            elif divmod(self.position.degrees_in_sign, 6)[0] == 3:
                return PlanetAge.OLD
            elif divmod(self.position.degrees_in_sign, 6)[0] == 4:
                return PlanetAge.DEAD

        else:
            if divmod(self.position.degrees_in_sign, 6)[0] == 4:
                return PlanetAge.CHILD
            elif divmod(self.position.degrees_in_sign, 6)[0] == 3:
                return PlanetAge.ADOLESCENT
            elif divmod(self.position.degrees_in_sign, 6)[0] == 2:
                return PlanetAge.YOUTH
            elif divmod(self.position.degrees_in_sign, 6)[0] == 1:
                return PlanetAge.OLD
            elif divmod(self.position.degrees_in_sign, 6)[0] == 0:
                return PlanetAge.DEAD

    @property
    def nature(self):
        return self.name.nature

    @property
    def dasha_length(self):
        return self.name.dasha_length

    @property
    def permanent_friends_with(self):
        return self.name.permanent_friends_with

    @property
    def permanent_neutral_with(self):
        return self.name.permanent_neutral_with

    @property
    def permanent_enemies_with(self):
        return self.name.permanent_enemies_with

    def get_permanent_relation_with(self, planet) -> PlanetRelation:
        print('checking friendship with: ', planet)

        if isinstance(planet, Planet):
            return self.name.get_permanent_relation_with(planet.name.name)
        elif isinstance(planet, str):
            return self.name.get_permanent_relation_with(planet)
        elif isinstance(planet, PlanetName):
            return self.name.get_permanent_relation_with(planet.name)
        else:
            raise Exception("invalid type variable planet for checking permanent relation")

    @property
    def exaltation_positions(self):
        return self.name.exaltation_positions

    @property
    def mooltrikon_positions(self):
        return self.name.mooltrikon_positions

    @property
    def debilitation_positions(self):
        return self.name.debilitation_positions

    @property
    def result(self):
        return self._planet_sign_results.get(self.name).get(self.position.sign.value)
