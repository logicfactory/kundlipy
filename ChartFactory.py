from ChartD1 import ChartD1
from ChartD2 import ChartD2
from ChartD3 import ChartD3
from ChartD4 import ChartD4
from ChartD5 import ChartD5
from ChartD6 import ChartD6
from ChartD7 import ChartD7
from ChartD8 import ChartD8
from ChartD9 import ChartD9
from ChartD10 import ChartD10
from ChartD11 import ChartD11
from ChartD12 import ChartD12
from ChartD16 import ChartD16
from ChartD20 import ChartD20
from ChartD24 import ChartD24
from DateTime import DateTime
from datetime import datetime


class ChartFactory:

    def __init__(self, ascendant, zenith, planets, datetime_of_birth: DateTime) -> None:
        print('*******************************inside ChartFactory: ', ascendant, zenith, planets)

        self._charts = dict()
        
        self._charts['D1'] = (ChartD1('D1', ascendant=ascendant, zenith=zenith, planets= planets))
        self._charts['D2'] = (ChartD2('D2', ascendant=ascendant, zenith=zenith, planets=planets))
        self._charts['D3'] = (ChartD3('D3', ascendant=ascendant, zenith=zenith, planets= planets))
        self._charts['D4'] = (ChartD4('D4', ascendant=ascendant, zenith=zenith, planets= planets))
        self._charts['D5'] = (ChartD5('D5', ascendant=ascendant, zenith=zenith, planets= planets))
        self._charts['D6'] = (ChartD6('D6', ascendant=ascendant, zenith=zenith, planets= planets))
        self._charts['D7'] = (ChartD7('D7', ascendant=ascendant, zenith=zenith, planets= planets))
        self._charts['D8'] = (ChartD8('D8', ascendant=ascendant, zenith=zenith, planets= planets))
        self._charts['D9'] = (ChartD9('D9', ascendant=ascendant, zenith=zenith, planets= planets))
        self._charts['D10'] = (ChartD10('D10', ascendant=ascendant, zenith=zenith, planets= planets))
        self._charts['D11'] = (ChartD11('D11', ascendant=ascendant, zenith=zenith, planets= planets))
        self._charts['D12'] = (ChartD12('D12', ascendant=ascendant, zenith=zenith, planets= planets))
        self._charts['D16'] = (ChartD16('D16', ascendant=ascendant, zenith=zenith, planets= planets))
        self._charts['D20'] = (ChartD20('D20', ascendant=ascendant, zenith=zenith, planets= planets))
        self._charts['D24'] = (ChartD24('D24', ascendant=ascendant, zenith=zenith, planets= planets))

        today = DateTime(date_time=datetime.utcnow().strftime('%Y-%m-%d %H:%M'), fmt='%Y-%m-%d %H:%M')
        print('utc_now: ', today)

        transit_planets = today.get_planets_positions('J2000', 'EARTH', fixed_zodiac=True)

        print('today planet positions: ', transit_planets)

        self._charts['Transit'] = (ChartD1('Transit', ascendant=ascendant, zenith=zenith, planets=transit_planets))

        for chart in self._charts.values():
            chart.calculate_dashas(datetime_of_birth)

    @property
    def charts(self):
        return self._charts

