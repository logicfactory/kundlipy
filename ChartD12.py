from Chart import Chart
from Position import Position
from typing import List, Dict
from Planet import Planet
from Sign import Sign, MoveType


class ChartD12(Chart):

    def __init__(self, name: str, ascendant: Position = Position(), zenith: Position = Position(),
                 planets: Dict = None) -> None:
        super().__init__(name=name, ascendant=ascendant, zenith=zenith, planets=planets)
        print('****************initializing ', self.__class__, '******************')

    @staticmethod
    def translate_position(position: Position):
        print('inside loop for position: ', position)
        no_of_parts = 12

        quotient, degrees_in_sign = divmod(position.ra * no_of_parts, 30)
        print('quotient and degrees_in_sign: ', quotient, degrees_in_sign)

        part_number = (quotient % no_of_parts) + 1
        print('part_number: ', part_number)

        position.ra = ((position.sign.value - 1 + part_number - 1) * 30) + degrees_in_sign
