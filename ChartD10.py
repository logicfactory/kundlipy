from Chart import Chart
from Position import Position
from typing import List, Dict
from Planet import Planet
from Sign import Sign


class ChartD10(Chart):

    def __init__(self, name: str, ascendant: Position = Position(), zenith: Position = Position(),
                 planets: Dict = None) -> None:
        super().__init__(name=name, ascendant=ascendant, zenith=zenith, planets=planets)
        print('****************initializing ', self.__class__, '******************')

    @staticmethod
    def translate_position(position):

        print('inside translate_position: ', position)
        no_of_parts = 10

        quotient, degrees_in_sign = divmod(position.ra * no_of_parts, 30)
        print('quotient and degrees_in_sign: ', quotient, degrees_in_sign)

        part_number = (quotient % no_of_parts) + 1
        print('part_number: ', part_number)

        if position.sign.value % 2 == 1:
            position.ra = ((position.sign.value - 1 + part_number - 1) * 30) + degrees_in_sign
        elif position.sign.value % 2 == 0:
            #position.ra = ((position.sign.value + 8 + part_number - 1) * 30) + degrees_in_sign
            position.ra = ((position.sign.value - 8 - (part_number - 1)) * 30) - degrees_in_sign