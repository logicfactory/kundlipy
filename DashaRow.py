from enum import IntEnum
from Planet import PlanetName
from DateTime import DateTime


class DashaDepth(IntEnum):
    MAHADASHA = 1
    ANTARDASHA = 2
    PRATYANTARDASHA = 3
    SUKSHMADASHA = 4
    PRANADASHA = 5
    DEHANTARDASHA = 6


class DashaRow:
    def __init__(self, dasha_depth: DashaDepth, lord:PlanetName, start:DateTime, end:DateTime, parent=None, *children) -> None:
        self._dasha_depth = dasha_depth
        self._lord = lord
        self._start = start
        self._end = end
        self._parent = parent
        self._children = list()
        self._children.extend(children)

    def __repr__(self):
        output = self._lord.name
        if self._parent:
            output = str(self._parent) + '->' + output
        return output

    def __str__(self):
        output = self._lord.name
        if self._parent:
            output = str(self._parent) + '->' +  output
        return output

    @property
    def lord(self):
        return self._lord

    @lord.setter
    def lord(self, lord: PlanetName):
        self._lord = lord

    @property
    def dasha_depth(self) -> DashaDepth:
        return self._dasha_depth

    @dasha_depth.setter
    def dasha_depth(self, dasha_depth: DashaDepth):
        self._dasha_depth = dasha_depth

    @property
    def start(self) -> DateTime:
        return self._start

    @start.setter
    def start(self, start: DateTime):
        self._start = start

    @property
    def end(self) -> DateTime:
        return self._end

    @end.setter
    def end(self, end: DateTime):
        self._end = end

    @property
    def parent(self):
        return self._parent

    @parent.setter
    def parent(self, parent):
        self._parent = parent

    @property
    def children(self):
        return self._children

    @children.setter
    def children(self, *children):
        self._children = list()
        self._children.extend(children)

    def add_children(self, *children):
        self._children.extend(children)

    def add_child(self, dasha_type: DashaDepth, lord:PlanetName, start:DateTime, end:DateTime, parent, *children):
        self.add_children(DashaRow(dasha_type, lord, start, end, parent, *children))
