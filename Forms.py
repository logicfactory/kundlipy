from flask_wtf import FlaskForm, RecaptchaField
from wtforms import StringField, SubmitField, PasswordField, IntegerField, FloatField, DateField, DateTimeField, BooleanField
from wtforms.validators import InputRequired, Email, Regexp, EqualTo


class PersonForm(FlaskForm):
    name = StringField('Name',
                              validators=[InputRequired()])
    DOB = DateField('Date of Birth',
                                   validators=[InputRequired()])
    TOB = StringField('Time of Birth',
                      validators=[InputRequired()])
    timezone = StringField('Timezone')
    place_of_birth = FloatField('Place', validators=[InputRequired()])
    latitude_of_birth = FloatField('Latitude', validators=[InputRequired()])
    longitude_of_birth = FloatField('Longitude', validators=[InputRequired()])
    save = BooleanField(label='save?')
    submit = SubmitField('submit')

class OneRegisterForm(FlaskForm):

    username = StringField('*Your Name', validators=[InputRequired(), Regexp(r'(?=.*[\w\s])',
                                                                             message="Please enter a valid name.")])

    useremail = StringField('*Your EmailID (This will be your MailTalk ID as well)', validators=[InputRequired(),
                                                                                                 Email()])

    userpassword = PasswordField('*Your Password (8 to 20 characters long)',
                                 validators=[InputRequired(),
                                             EqualTo('userpasswordreenter', message='Passwords must match.')
                                             ]
                                 )

    userpasswordreenter = PasswordField('*Confirm Password')

    serveraddress = StringField('IMAP Address', validators=[InputRequired()])

    serverport = IntegerField('IMAP Port', validators=[InputRequired()])

    smtpaddress = StringField('smtp address', validators=[InputRequired()])

    smtpport = IntegerField('smtp port', validators=[InputRequired()])

    recaptcha = RecaptchaField()

    submit = SubmitField('Save')
