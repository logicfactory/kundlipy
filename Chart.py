from Planet import Planet, PlanetName
from Position import Position
from Sign import Sign
from config import PlanetState, PlanetNature, PlanetRelation
from typing import List
from House import House
from config import degrees_between
from jinja2 import Markup
from math import floor, ceil
from typing import Dict
from abc import abstractmethod
from Position import Position
from Dasha import Dasha


class Chart:

    def __init__(self, name: str, ascendant: Position = Position(), zenith: Position = Position(),
                 planets: Dict = None) -> None:

        print('ascendant: ', ascendant)
        print('zenith: ', zenith)
        print('planets: ', planets)

        self._areas_of_life = {
            'D1': 'Existence at physical level',
            'D2': 'Wealth and money',
            'D3': 'Everything related to brothers and sisters',
            'D4': 'Residence, houses owned, properties and fortune',
            'D5': 'Fame, authority and power',
            'D6': 'Health troubles',
            'D7': 'Everything related to children (and grand-children)',
            'D8': 'Sudden and unexpected troubles, litigation etc.',
            'D9': 'Marriage, spouse(s), Dharma (duty and righteousness), interaction with other people, basic skills, inner self',
            'D10': 'Career, activities and achievements in society',
            'D11': 'Death and destruction',
            'D12': 'Everything related to parents (also uncles, aunts and grand-parents, i.e. blood-relatives of parents)',
            'D16': 'Vehicles, pleasures, comforts and discomforts',
            'D20': 'Religious activities and spiritual matters',
            'D24': 'Learning, knowledge and education',
            'D27': 'Strengths and weaknesses, inherent nature',
            'D30': 'Strengths and weaknesses, inherent nature',
            'D40': 'Auspicious and inauspicious events',
            'D45': 'All matters',
            'D60': 'Karma of past life, all matters'
        }

        self._name = name
        self._planets = dict()
        self._houses = list()
        for i in range(1, 13):
            self._houses.append(House(i))
        self._ascendant = Position()
        self._zenith = Position()

        self._D1_sun_position = planets.get(PlanetName.SUN).position
        self._D1_moon_position = planets.get(PlanetName.MOON).position

        for planet in planets.values():
            print('inside loop: ', planet)
            self._planets[planet.name] = Planet(planet.name, planet.position.copy())

        print('planet_positions: ', self._planets)

        self._ascendant = ascendant.copy()
        #self._zenith = zenith.copy()

        self.translate_position(self._ascendant)

        for planet in self._planets.values():
            self.translate_position(planet.position)

        print('planet positions in ', self.__class__, ': ', self._planets)

        self.get_house(1).cusp_position = self._ascendant.copy()

        self.equal_houses()
        print('houses: ', self._houses)

        self.add_planets_to_houses()

        print('chart ', self._name, ' initialized ', self._planets, self._houses)

        self._dashas = dict()

        self._lord_results = dict()

        self._lord_results = {
            1: {
                1: "The subject lives by his own exertion, will have an independent spirit and will have two wives or "
                   "one married and another illegal. If the Lagnadhipati is well disposed in Lagna, the person becomes "
                   "famous in his own community and country.",
                2: "There will be more of gains, teased or worried by enemies, good character respectable and generous "
                   "hearted. Well disposed. He will gladly discharge his duties towards his kith and kin, and will be "
                   "ambitious. He will possess prominent eyes and be blessed with forethought.",
                3: "Makes one highly couragious, fortunate, reepectable, two wives, intelligent and happy. When Lagnadhipati is well disposed, the native's rise in life will be brought about by his brothers. He may become famous as a musician or a mathematician depending upon the nature of the sign and the planets involved.",
                4: "There will be happiness from parents, many brothers, materialistic, well built, fair looking and well-behaved. If the 4th lord is favourably disposed, the person will acquire considerable landed properties, specially through maternal sources. He will be rich, happy, famous and commands a number of conveyances.",
                5: "The first child does not survive, not much happiness from children, short tempered, subservient and serving others. If fortified : In the good graces of rulers or powerful political parties and likely to be absorbed into trade or diplomatic services. He will propitiate deities consistent with the indications of the fifth lord.",
                6: "In addition to the results produced by the lord of Lagna being in the third, the following may also be noted. There will be bebts but they will be liquidated when the Dasa of the Lagndhipati comes. When the lord is fortified, the native joins the army, becomes a Commander or even a Commander-in-chief provided the lord's Dasa operates at the opportune period. Or, he may become head of medical or health services or an expert physician or surgeon. Here the other influences should be suitably balanced.",
                7: "The wife does not live or there will be more than one marriage. Later in life becomes detached from worldly affairs and tries to lead an ascetic life. Depending upon other factors the subject will either be rich or poor.	There will be much travelling. If well disposed, he will spend most of his time in foriegn countries and lead a licentious life. Or he will be a puppet in the hands of his parents-in-law.",
                8: "Learned, gambling tendencies, interested in occultism and mean character. If the lord is strong the native takes pride in helping others, has a number of friends, religiously inclined and will have a peaceful and sudden end.",
                9: "Generally fortunate, protector of others, religious; if a Hindu—worshipper of Vishnu, good orator, happiness on account of wife and children; and rich. Provided the lord is well disposed, he will inherit good ancestral and paternal property. The father will be famous, philanthropic and god-fearing.",
                10: "In addition to the results of the 4th house, there will be professional success, honoured by eminent men, a research scholar or specialisation in that branch of knowledge or profession which is represented by lord of Lagna and the 10th.",
                11: "In addition to the results of the 2nd house, there will always be gains in business if a businessman. The subject will not experience financial straits. He owes his prosperity to his elder brother. He will earn enormous business profits, consistent with the indications of the other planets joining the combination.",
                12: "The same results as in the 8th are produced. In addition, there will be many losses, visiting holy places and no success in business enterprises. He will spend inherited riches on charities and other deserving causes. Emotionally balanced he will dedicate himself for public weal."
            },
            2: {
                1: "The native becomes no doubt wealthy but he hates his own family, lacks polite manners, becomes passionate, subservient and time-serving. When the lord of the 2nd is in Lagna, one will earn money by his own effort, intelligence and learning or he will get inherited wealth if the 9th lord or the Sun has anything to do with this combination.",
                2: "Becomes proud. The native may marry twice or thrice depending upon the strength of the seventh house. He may become childless. The position of the 2nd lord in a constellation which does not happen to be the 3rd, 5th and 7th from Janma Nakshatra is highly desirable, especially if he is otherwise fortified, i.e., joined with or aspected by benefics. The exaltation of the 2nd lord of his disposition in Lagna, the 4th, 5th, 7th, 9th or 10th, will fortify him to the extent of making him a yogakaraka. The native will be enabled to earn considerable fortune through business or other occupations consistent with the nature of the 2nd lord and 2nd house. Depending upon the nature of the aspects cast on the 2nd lord by malefic planets, the native will sustain losses. An affliction to the 2nd lord may also express itself in the shape of the subject not getting nutritious and delicious food, or his family and children suffering from constant diseases, or want of cordial relations between the native and his life-partner.",
                3: "Brave, intelligent, good-natured but depraved character. Atheistic tendencies will be rampant and he becomes addicted to luxuries. Later in life he turns out to be a miser. When the 2nd lord in the 3rd is well fortified, he will be benefited by his sisters. He will benefit by learning fine arts, viz., music and dancing. He will also indulge in propitiating Kshudra Devatas or evil spirits.",
                4: "The above results will apply in addition to the following. He will spend money for his own happiness. He will be highly frugal in dealing with money. When the 2nd lord in the 4th is well fortified, one will earn well as an automobile dealer or agent or an agriculturist or landlord or commission agent. He will also be benefited by his maternal relations. There will be losses on this account if the 4th lord is afflicted.",
                5: "Hating family, sensual, not spending money even on children, he lacks manners and etiquette. When the 2nd lord in the 5th is well fortified there will be unexpected acquisition of wealth through lotteries, crosswords, or the favour of rulers.",
                6: "Income and expenditure from enemies. He will suffer from defects or diseases in the anus and thighs. Amassing of wealth by black-marketing, deceit, dissimulation and by creating misunderstandings and troubles between friends and relatives and through questionable and suspicious dealings can be anticipated if the second lord joins the 6th and is well fortified. If afflicted, he will be involved in these troubles and sentenced for such crimes as breach of trust, forgeries and perjuries.",
                7: "Likely to become a healer. Laxity of morals will mark both husband and wife. He will waste much money on the gratification of the senses. When the second lord who joins the 7th with the 7th lord is strong there will be influx of wealth through foreign sources. The native will undertake journeys to foreign countries and do business. When the Rasi, Navamsa or the constellation held by the 2nd lord happens to be a feminine one, then he will benefit by contact with women.",
                8: "Will have very little or no happiness from wife or husband. Misunderstandings with elder brothers. Gets landed properties. When the 2nd lord is strong, there will be influx as well as loss of wealth. Actual observation reveals that under such a combination, there will hardly be any earnings, but inherited or accumulated wealth will disappear.",
                9: "Skilful, ill-health in young age but healthy afterwards, will possess lot of wealth and become happy. When the 2nd lord is well fortified and the 9th lord is in Lagna, the native will have good inheritance. There will also be benefits through different sources according to the nature of the sign and nakshatra held by the 2nd lord.",
                10: "Respect from elders and superiors, learned, wealthy and he earns by his own exertions. The native will take to a number of useful avocations. He will do business or take to agriculture and also engage himself in philosophical lectures and dissertations, and thereby benefit financially. Here again, the constellation and the sign held by the 2nd lord determine the exact nature and sources of earnings. Powerful afflictions will cause loss from the very same sources.",
                11: "Health will be bad during childhood, earns considerable wealth but becomes unscrupulous. When well fortified, one earns by lending money or as a banker or by running a boarding house.",
                12: "Health will be bad during childhood, earns considerable wealth but becomes unscrupulous. When well fortified, one earns by lending money or as a banker or by running a boarding house."
            },
            3: {
                1: "Earns livelihood by self-exertion, becomes vindictive, lean and tall body, brave and courageous, always sickly and serving others. When well fortified he will become an expert in dancing, music and acting, and the means of livelihood will be primarily fine arts. He will earn a good name as an actor.",
                2: "This is an unfavourable position as it makes the subject rather unscrupulous unless there are other favourable combinations. He will make advances on the women and wealth of others. Likes mean deeds and is generally devoid of happiness. He is likely to lose his younger brothers.",
                3: "Brave, surrounded by friends, relatives, blessed with good children, wealthy, happy and contented. The 3rd lord well disposed in the 3rd, 6th or 11th indicates a number of younger brothers. When the 3rd lord happens to be Mars and occupies the 3rd, then, generally, the native will lose all his younger brothers. Saturn will also give similar effects. The Sun in a similar position will kill elder brothers.",
                4: "If the lord of the 3rd is in the 4th, life will be happy on the whole. He becomes rich and learned. But the wife will be cruel-hearted and mean. When the lord of the 3rd is well fortified in the 4th and the lords of Lagna and 9th are rendered weak, his brothers will survive him. He will have step-brothers if the 9th lord is strong. When Mars is weak he will lose his lands and he will have to live in others' houses. Evil results will be minimised if the 3rd house is beneficially disposed.",
                5: "Much pleasure will not be derived from children. Financially well off in life. Friction will prevail in the domestic life. If the 3rd lord is in the 5th, well disposed, the native will be highly benefited by his brothers. He will carry on agricultural operations on a large scale, or he will be adopted by a rich family. He will also shine well in Government service.",
                6: "Hates brothers and relatives and difficulty through them. Becomes rich. Maternal relatives will suffer. Accepts illegal gratifications. When the 3rd lord is in the 6th, well disposed, younger brother joins the Army. One of the brothers will become a successful physician. If the 6th lord also joins the 3rd, the native becomes either a sportsman, physical culturist or an athlete. When the 6th and the 3rd are both afflicted, he will suffer from diseases and be tormented by enemies and he will himself be deceitful.",
                7: "May incur the displeasure of rulers  or authorities. Many vicissitudes in life. Much suffering in childhood. The union will be unfortunate. Danger while travelling. When the 3rd lord is in the 7th, well fortified, there will be cordial feelings between brothers. When the 7th  lord is in Lagna, one of the brothers will settle in a foreign country and he will help the native.",
                8: "Involvement in a criminal case or false accusations. Trouble on account of death or bequests, marriage unfortunate, career will not be smooth, victim of misfortune. When the 3rd lord is in the 8th, he will suffer from a serious and dangerous disease and lose his younger brother.",
                9: "Fortune will improve after marriage. Father untrustworthy. Long journeys. Sudden and unexpected changes in life. When the 3rd lord is in the 9th, favourably disposed, the native's brother will inherit ancestral property. The native himself will be benefited by his brother. When afflicted, the person will have misunderstandings with his father.",
                10: "A quarrelsome and faithless wife. The native will become rich. He will be happy and intelligent. Gain from journeys connected with profession. When the 3rd lord is in the 10th, all the brother, will shine well and they will be helpful to him in all ways",
                11: "Not a very good combination.. Earnings with effort. He becomes vindictive. The body will be unattractive and emaciated. Subservient to or dependent upon others, and liable to surfer from frequent attacks of illness.",
                12: "Sorrow through relatives, gets fortune from marriage, seclusion. Great ups and downs. Unscrupulous father. When the 3rd lord is in the 12th, the youngest brother will be a tyrant. The native becomes poor on account of him."
            },
            4: {
                1: "The person becomes highly learned, but will be afraid to speak in public assemblies. He is likely to lose inherited wealth. According as the fourth lord posited in Lagna is strong, middling or weak, the native will have been born in a rich, mediocre or poor family.",
                2: "He will be highly fortunate, courageous and happy. He will have a sarcastic nature. He will inherit property from maternal grandfather.",
                3: "The person will be sickly, generous, a man of character, and will acquire wealth by self-effort. He will suffer from the machinations of step-brothers and step- mother.",
                4: "Religiously inclined, will have respect for traditions. He will be rich, respected, happy and sensual.",
                5: "Loved and respected by others, devotee of Vishnu, becomes rich by self-effort. Mother comes from a respectable family. The native will acquire vehicles.",
                6: "Short-tempered and mean, he will have dissimulating habits, evil thoughts and intentions. He will always be roaming about.",
                7: "Generally happy ; will command houses and lands ; will eke out livelihood in distant places or near his birthplace according as the seventh happens to be a movable or fixed sign.",
                8: "The person becomes miserable. Father dies early. He will be either impotent or loose in sex-life. He is also likely to lose landed properties or face litigation.",
                9: "Generally a fortunate combination favouring happiness in regard to father and properties.",
                10: "Will have political success. He will be an expert chemist. He will vanquish his enemies and make his personality felt by the world. Loss of reputation is posible if the 4th lord is afflicted.",
                11: "Self-made, generous, sickly, mother fortunate, but may have a step-mother also. Favours success in selling and buying cattle and lands.",
                12: "Deprived of happiness and properties. Early death to mother, bad finances and generally a miserable existence."
            },
            5: {
                1: "If the 5th lord is favourably combined and associated, the person commands a number of servants, becomes a Judge, Magistrate or Minister empowered to punish the evil-minded. He will earn the grace of God ; few children ; will have foes and gives happiness to others. If the 5th lord is afflicted, there will be no issues; will invoke kshudra devatas (evil and destructive forces), will be evil-minded and leader of a gang of deceitful persons ; a tale-bearer with a sting. If the lord is moderately good, mixed results will follow.",
                2: "If the 5th lord is favourably disposed, the person will be blessed with a beautiful wife and well-behaving children. There will be gains from Government or King. He will become learned and a good astrologer. Lord weak and afflicted, Poor ; loss of money through Government displeasure ; will be unable to maintain his own family ; will have family troubles and misunderstandings ; becomes a priest in a Siva temple.",
                3: "Favourably disposed, many good children and brothers, If unfavourably disposed, loss of children, misunderstandings with brothers and continuous occupational troubles. He will become stingy and a tale bearer.",
                4: "If favourably disposed, the person will have a few sons, one of whom will live by agriculture. The mother will live long. May become an adviser to a ruler or his preceptor. The lord, afflicted, causes death of children. The lord, moderately strong, confers daughters and no sons.",
                5: "Lord favourably disposed indicates a number of sons ; becomes great in his own line of activity ; otherwise he becomes an expert in Mantrasastra and befriends persons in power. He may also become an expert in mathematics or head of a religious institution. Lord afflicted, contrary results should be anticipated. Children will die; he will not keep to his word; wavering mentality and cruel.",
                6: "If the fifth lord is favourably disposed, the maternal uncle will be a famous man. He will have enmity with his own son. If the lord is afflicted, issues will not be born and he may have to adopt one from maternal uncle's line.",
                7: "When the lord is favourably disposed, the native's son lives abroad and attains distinction, wealth and fame. Or he will have a number of issues. He will also become renowned, learned, prosperous, greatly devoted to his master and possesses a charming personality. When-the lord is afflicted, there will be loss of children, one of whom will die abroad after attaining name and fame.",
                8: "Paternal property will be lost due to debts. There will be extinction of the family. He will suffer from lung troubles. He will be peevish, unhappy but not poor.",
                9: "He will become a teacher or a preceptor. Renovates ancient temples, wells, choultries and gardens. One of the sons attains distinction as an orator or author. If the lord is afflicted, he will earn divine wrath and consequent destruction of fortune.",
                10: "If the lord is beneficially disposed, a Raja Yoga is formed. Acquires landed property ; earns the goodwill of the rulers, constructs temples and performs religious sacrifices ; one of the sons becomes a gem of the family. If aspected by the Sun the native may join the intelligence department. If the lord is afflicted, faces the wrath of the rulers and contrary results will happen.",
                11: "Benefits through sons and success in all undertakings; becomes rich and learned and helps others; will have a number of sons; becomes an author.",
                12: "Quest for knowing the Ultimate Reality will be pronounced. He will lead a life of non- attachment, becomes spiritual, moves from one place to another and ultimately attains Moksha."
            },
            6: {
                1: "Well aspected, the person  may Join the army as a soldier or Commander consistent with the  strength  or  otherwise  of  the  disposition.  Or  he may become a Minister of War or. an official or officer concerned with prisons.	He will live in the house of his maternal uncle. If the sixth lord is weak and otherwise afflicted, he will become a robber or a thief or leader of a criminal gang.",
                2: "Conjoined with or aspected by benefics the native will have untold suffering in family life and deep sorrows, loss of money through enemies, defective vision, uneven teeth and stammering. If the sixth lord is weak and otherwise ill-disposed in the 2nd, there will be loss of wife in the Dasa or Bhukti of the malefic lord. If Venus is weak, the native will be a celibate and poverty stricken, just able to get a morsel of food when hungry.",
                3: "The sixth lord fortified confers enmity with brothers. Or his maternal uncle befriending the native's brother works against the native's interests or the native's brother suffers from frequent ill-health. The 6th lord weak and afflicted, the native will have no younger brothers.",
                4: "Well fortified, lives in a dilapidated building. He will have breaks in education and will discard his mother. Maternal uncles will generally be land cultivators. Weak and afflicted: he will quarrel with his mother and ancestral property will be involved in debts. He will work as a menial and lead a miserable life. Troublesome home and domestic affairs and trouble through servants.",
                5: "Sickly children. The native wil' be adopted by his maternal uncle and become fortunate-",
                6: "Increase of cousins. Native's maternal uncle becomes famed. If in conjunction with weak Lagnadhipathi, he will suffer from an incurable disease and increase of enmity with kith and kin.",
                7: "Generally marries mother's, brother's or father's sister's daughter. The maternal uncle lives in a far-off place (or a foreign country). The wife's character will be doubtful. If the sixth lord is afflicted, he will either divorce his wife early in life or she will die. If the Rasi and Navamsa involved are hermaphrodite ones, he will have a sickly or barren wife. When Lagnadhipati joins the sixth lord in the 7th which happens to be a hermaphrodite sign, the native will be a eunuch and unable to perform the sexual act. There will also be troubles with disrespectable women.",
                8: "When fortified, he will have Madhyayu or middle life. When afflicted, he will have plenty of debts and will suffer from loathsome diseases. He will hunt after women other than his own wife and take pleasure in inflicting pain on others.",
                9: "Father becomes a judge, if the sixth lord is well fortified. The maternal uncle becomes highly fortunate. There will be misunderstandings between him and his father. There will be benefits from Gnatis or cousins. If afflicted : poverty, sinful acts, misfortunes through relatives, ungrateful towards preceptors and engaged in unrighteous deeds. If moderate : becomes a mason, timber merchant or stone cutter.",
                10: "If fortified : Sinful and destructive nature, poses as an orthodox and pious man but really unscrupulous in regard to religious matters. When  the lord is weak : dismissal formidable enemies, low life or begging.",
                11: "If benefic, eldest brother will be a judge. If ordinary : an elder brother becomes a judge for some time but loses his job. If malefic : poor and wretched life : suffering on account of convictions.",
                12: "Well disposed : difficulty and sorrow through destructive nature; causes harm to others. If afflicted : miserable, hard and wretched existence."
            },
            7: {
                1: "The native may marry someone he has known since childhood or one who has been brought up in the same house. The wife or husband of the native will be a stable and mature person. He will be intelligent and capable of weighing the pros and cons. Afflictions to the 7th lord may entail constant travelling. If the 7th lord and Venus are afflicted, the native may be sensual and seek clandestine relationship with the opposite sex.",
                2: "The native will get wealth from women or through marriage. If afflicted, one may earn money through despicable means as trading in flesh, women, not excluding his wife. He may eat food offered at death-ceremonies (shraddha) and wander about seeking such food. If the second house is a dual sign and afflicted, more than one marriage is likely. If a maraka Dasa is on, the native may die during the period of the seventh lord. The person will have a wavering mind and will always be inclined sensually.",
                3: "This disposition gives lucky brothers who may live abroad. If afflicted the native may indulge in adultery with a brother's or sister's married partner. Affliction also gives misfortunes to co-boms. Female issues servive.",
                4: "Gives a lucky and happy married partner with many children and comforts. The native may have the benefit of high academic qualification and own many vehicles. If afflicted, domestic harmony may be spoilt through an immature and mean partner. The native may run into endless problems on account of his conveyances. If severely afflicted by the nodes and other malefics, the native's wife's character becomes questionable.",
                5: "An early marriage, the partner may hail from an affluent and well-to-do family. The wife or husband will be mature and an advantage to the native. If the 7th lord is weak, there may be no children. If severely afflicted, one may get issues through the adulterous conduct of the wife. If there are both afflictions and benefic influences on the 7th lord, the native may get only female progeny. Trouble to one's office superiors through foreign sources is likely. The native will possess good character.",
                6: "The native may have two marriages with both partners living. One may marry a cousin such as an uncle's daughter. If badly afflicted and the karaka Venus is also ill-disposed, one may suffer from impotency and many other diseases. The native's wife may be sickly and jealous by nature denying the husband happiness from marriage. If Venus is well placed but the 7th lord is afflicted, the native may suffer from piles. If Venus is weak but not afflicted, one may desert or lose one's married partner through some indiscreet act.",
                7: "If well placed, the native will have a charming and magnetic personality. Women will flock to him and seek him out for alliance. The wife or husband will be a just and honourable person coming from a family of reputation and social standing. If weak and afflicted, it gives a lonely life devoid of marriage and friends, and loss through marriage negotiations.",
                8: "When well placed, marriage may take place with relatives or the partner may be a rich person. Affliction causes the early death of partner while the native may die in distant lands. It gives a sickly and ill-tempered wife or husband leading to estrangement and separation.",
                9: "If fortified, the father may live abroad while the native may make his fortune in foreign lands. He will get an accomplished wife who will enable him to lead a righteous life. If afflicted, the father may die early. Married partner may drag the native from the right course (Dharmic) of life and he may waste away his wealth and suffer penury.",
                10: "The native may flourish in a profession abroad or his career may involve constant travelling One will get a devoted and faithful wife or husband. The wife may also be employed and contribute to the native's income. Or, she may help in the advancement of the native's career. If afflicted, wife will be avaricious and over-ambitious but without sufficient capacity. Consequently native's career may suffer and deteriorate.",
                11: "There may be more than one marriage or the native may associate with many women. If beneficially disposed, wife may hail from a rich background or bring in much wealth. If afflicted, the native may marry more  than once, but one wife may outlive him.",
                12: "There maybe more than one marriage in the native's life. He may marry a second time clandestinely while the first wife is still alive. Or, if afflicted, he may marry a second time after losing the first wife by death or separation. But if the affliction is severe, the wife or husband may die or separate soon after marriage and there may be no second marriage. Death may occur while travelling or abroad. If both karaka and the 7th lord are weak, the native may only dream of women but never marry. The native's wife may hail from a servant's family. He will be close-fisted and generally Door."
            },
            8: {
                1: "Penury and heavy debts will befall the native who has the 8th lord placed in the Ascendant with the Ascendant lord. Misfortune will follow him at every step. If the 8th lord is weak or placed in the 6th, 8th or 12th from Navamsa Lagna, the intensity of misfortune is reduced. If the 8th lord is severely afflicted the native will suffer bodily complaints such as disease and disfiguration. His constitution will be weak and he will have no bodily comforts. He will be the target of the displeasure of his superiors and higher-ups. Trouble from Government will cause him worries.",
                2: "The lord of the 8th house in conjunction with the 2nd lord in the 2nd house brings in troubles and problems of all sorts. The native suffers from eye and tooth troubles. He will have to eat unhealthy and tasteless or putrid foods. His domestic life will be filled with discontent and quarrels. His wife will not understand him. This may lead to estrangement and even separation. If longevity is good, he may suffer some severe illness. If the 8th lord is in the 6th, 8th or 12th from Navamsa Lagna, the intensity of the results will be reduced in degree.",
                3: "If the lord of the 8th house combines with the lord of the 3rd in the 3rd, the third house significations suffer. The native's ears may cause problems or he may go deaf. Misunderstandings will crop up with brothers and sisters leading to quarrels. The native will be beset by all sorts of fears and mental anguish. He may imagine things and suffer from hallucinations. He may involve himself in debts and get into trouble thereby. If malefics afflict the 8th lord in the 3rd with the 3rd lord, the sufferings of the native will be unbearable. But if the 8th iord combines with the 6th or 12th lords, benefic results may come by. He may get a monetary windfall through writing or through the agency of a co born.",
                4: "If the iord of the 8th house joins the 4th lord in the 4th house, the native's mental peace will be shattered Domestic bickerings, financial and other problems will increase. Mother's health may suffer and cause great concern. The native may be beset with problems regarding his house, land and conveyance. If the affliction is heavy his land and immovable property may slip from his hands due to circumstances beyond his control. His conveyances may get lost or be destroyed. His pets may contract diseases and die. Malefics furthering the affliction may force him to seek his fortune abroad where he will meet with all sons of troubles and losses. Reverses in profession and the displeasure of superiors are also likely.",
                5: "If the lord of the 8th is in the 5th with the 5th lord, the children of the native may get into trouble. They may commit some crime and invite situations that could affect the native's reputation. Or the native and his father may develop misunderstandings. The native's child may fall sick and suffer thereby. If the affliction is heavy, a child may die as soon as it is born or cause much grief to the native due to some incurable physical affliction or mental retardation. The native may also suffer much bodily ill-health. If the 8th lord is in the 6th, 12th or 8th from Navamsa Lagna, the evil results are greatly mitigated. But if fortified in a kendra (quadrant) or trikona (trine), the evil results are intensified. Since the 5th house is the buddhisthana, the native may also suffer nervous debility or breakdown or mental aberration. When the 9th lord conjoins the 8th lord in the 5th house and the Lagna lord is debilitated, the native has neither knowledge nor wealth ; he is hostile, lustful and ill-temprered. He tends to  be crafty, reviles God and pious men and is himself treated with contempt by his wife and children.",
                6: "If the lord of the 8th house joins the 6th lord in the 6th house, a Rajayoga results. Material affluence, fame and acquisition of objects desired are the good results. But because the 6th house is the house of disease the native may suffer ill-health. If afflicted, the native suffers loss of money through theft and trouble through courts and the  police. The evil is intensified if the 8th lord is in a kendra or trikona. The native's maternal uncle may suffer much trouble. If the 6th lord is fortified, he is able to overcome all his troubles and emerge victor. No attempts made by his ill-wishers and enemies to harm him will succeed.",
                7: "The lord of the 8th house placed in the 7th house with the 7th lord curtails longevity. The native's wife may suffer ill-heaith. !f afflicted, the native will also suffer from disease. He may go abroad where he will meet with ill- health and problems. If the 7th and 8th lords are strong, the native will undertake foreign journeys on diplomatic missions and distinguish himself.",
                8: "If the lord of the 8th house occupies the 8th house in strength, the native lives long enjoying happiness. He will acquire lands, conveyance, power and position through the merit acquired in former lives. If the 8th lord is weak, he may have no serious troubles but may not also enjoy any luck or good fortune of significance. The father of the native may die or pass through some crisis. If the 8th lord is afflicted, the native will fail in his undertakings. He will be prompted to do the wrong things and thereby suffer loss.",
                9: "If the lord of the 8th house joins the 9th lord in the 9th house with malefics, the native may lose his father's property. Misunderstandings with father may arise. If the Sun, the natural significator of father is afflicted, father may die during the period of the 9th lord. If conjoined with benefics, the native acquires his father's property. Relations with father will be harmonious. If the 9th lord is weak, the native suffers ail kinds of hardships,  misery  and  unhappi-ness.	His friends and kinsmen may desert him while his superiors will find fault with him. If the 8th lord is in the 6th, 8th or 12th from Navamsa Lagna, the evil results will be greatly reduced.",
                10: "If the lord of the 8th house is in the 10th house with the 10th lord, the native has slow advancement in career. He faces obstacles and impediments in his activities. In the appropriate period he may be superseded by his subordinates and his merit may go unnoticed. He may resort to deceit and unrighteous means to gain his ends. His thinking will be clouded and his actions will invite the wrath of the government or the law. He may suffer poverty. If the 2nd lord is also afflicted and joins the 8th lord, his reputation may suffer due to involvement in huge debts and inability to repay them. If the 8th lord is placed in the 6th, 8th or 12th from Navamsa Lagna, the intensity of the evil is greatly reduced. The 8th lord in the 10th may also confer unexpected gains due to the death of the superiors or elders.",
                11: "If the lord of the 8th house combines with the 11th lord in the 11 th house, there may be trouble to close friends. Elder brother may pass through a difficult time. Relations with him will be strained and troubled. Or the elder brother may cause anguish to the native and his family by his unscrupulous behaviour and conduct. Business may suffer losses and run into debts. If benefic planets influence the combinations there will be troubles but the native gets help from friends and elder brother to overcome them. Afflictions will aggravate the malefic results.",
                12: "The position of the 8th lord in the 12th house with the 12th lord gives rise to a Rajayoga. If benefics join the 8th lord, unfavourable results may be expected. Treachery of friends will result in many problems and grief. Unexpected expenditure will arise and there may be pecuniary losses. If the 8th lord is in the 12th house and the 12th lord is favourably placed in a trine or quadrant, the native will gain in religious learning and piety. Some post or seat of authority may be thrust on him with all its attendant paraphernalia. If afflicted by malefics, the native may resort to vicious acts clandestinely. Such acts would include rape, adultery, conterfeiting of money (the 12th house is the house of secrecy end deceit. The 8th house signifies sudden gains of money) and smuggling activities."
            },
            9: {
                1: "When the ninth lord is placed in the first house, the native becomes a self-made man. He earns much money througn his own efforts. If the 9th lord combines with the Lagna lord in the first house and associates with or is aspected by a benefic planet, the native is fortunate with riches and happiness.",
                2: "When the ninth lord is placed in the 2nd house beneficially, the native's father is a rich and influential man. The native acquires wealth from the father. Malefics influencing the ninth lord in the 2nd house ruin or destroy paternal property.",
                3: "If the lord of the ninth is placed in the 3rd, the native makes his fortune through writing, speeches and oratorial abilities. The native's father will be a man of moderate means while the native advances his fortune through his co- borns. If malefics afflict the ninth lord in the 3rd house, the native may land in trouble through his writings which may be irrational and even obscene depending upon the nature of affliction. He may be forced to sell his paternal property because of troubles occurring through his writings.",
                4: "The ninth lord in the 4th house gives vast landed properties and beautiful bungalows. Or the native may earn through estate and land dealings. His mother will be a rich and fortunate woman. He will inherit his father's immovable properties. If the ninth lord is afflicted in the 4th house, the native may not have any domestic unhappiness. His early life will be crossed by miseries due to a hard-hearted father or disharmony between parents. If Rahu afflicts, mother may be a divorcee or living separately from his father. ",
                5: "The ninth lord in the fifth house gives a prosperous and famous father. The native's sons may also be very fortunate in life and enjoy success and distinction.",
                6: "The ninth lord in the sixth house gives a sickly father afflicted with chronic diseases. If benefics flank such a sixth house, the native may gain wealth through successful termination of father's legal problems, and by way of compensation, costs, etc. If malefics afflict the ninth lord in the sixth house, the native's attempts to make his fortune may be frustrated through litigation involving his father or debts contracted by him.",
                7: "The native may go abroad and prosper there. His father may also prosper in foreign lands. He will get a noble and lucky wife. If ascetic yogas are present in the chart, the native may seek spiritual guidance and fulfilment abroad. If asubhayogas spoil the ninth lord, the father may meet with his death abroad.",
                8: "The native may lose his father early in life. If malefics afflict the eighth house in such a case, he may suffer severe poverty and heavy responsibility due to father's death If benefics influence the ninth lord, the native may inherit substantial paternal property. Afflictions may cause the native to abandon traditions or damage religious institutions and trusts set up by the family.",
                9: "The ninth iord in the ninth house gives a long-lived and prosperous father. The native will be religiously inclined and be charitable. He will travel abroad and earn money and distinction thereby. If afflicted by malefics or if the 9th lord occupies the 6th, 8th or 12th from Navamsa Lagna, the native's father will die early.",
                10: "If the ninth lord is in the tenth house, the native will become very famous and powerful. He will be generous and occupy posts of authority. He will earn much wealth and acquire every kind of comfort and luxury. His means of livelihood will be righteous and he will be a law abiding citizen.",
                11: "The native will be exceedingly rich. He will have powerful and influential friends. His father will be a well-known and well-placed man- If afflicted, unfaithful friends will destroy the native's wealth through selfish scheming and fraud.",
                12: "The position of the lord of the ninth house in the twelfth house gives a poor background. The native will suffer much and will have to work very hard in life. Even then success may not come to him. He will be religious and noble but always in want. Father may die early leaving the native penniless."
            },
            10: {
                1: "When the lord of the tenth house occupies the Ascendant, the native rises in life by sheer dint of perseverance. He will be self-employed or pursue a profession of independence. When the Lagna and 10th lord combine in the first house the native becomes very famous and a pioneer in his field of work. He founds a public institution and engages himself in social projects.",
                2: "The 10th lord in the second house makes the native fortunate. He rises well in life and makes a lot of money. He may engage himself in the family trade and develop it. If malefics afflict the 10th house he will suffer losses and be responsible for winding up the family business. He will prosper in catering and restaurant businesses.",
                3: "The native may have to travel constantly on short-journeys. He will be a speaker or writer of celebrity if the 10th lord is well-placed. His brothers may be instrumental to some extent in advancing his career. If 10th lord is in the 6th, 8th or 12th from Navamsa Lagna or in an unfriendly constellation in the 3rd house, the native's rise in life is slow and beset with obstacles. If the 3rd lord is also afflicted, rivalry between brothers may Ie3d to reversals, obstacles, etc, in the native's career.",
                4: "The native will be a lucky man and highly learned in various subjects. He will be famous both for his learning and generosity. If the 10th lord is strong, the native is respected wherever he goes and he receives royal avour. He may engage in agricultural pursuits or in dealings with immovable properties- If the 4th lord, the 9th lord and the 10th are beneficially disposed and related to one another, the native wields great political authority as a president or head of a government. If the 10th lord is depressed, eclipsed, in an inimical sign or afflicted by malefic planets, the native will lose his lands and be forced to take to a life of servitude. The same result obtains if the 10th lord conjoins the 8th lord in the 4th house in a malefic shashtyamsa.",
                5: "The native shines well as a broker and engages in speculation and similar business. If benefics join the lord of the 10th in the 5th house, the native leads a simple and pious life engaging himself in prayers and pious activities. He may become the head of an orphanage or remand home if the 10th lord occupies the 6th, 8th or 12th Navamsa.",
                6: "The person will have an occupation bearing on judiciary, prisons or hospitals. If Saturn aspects the 10th lord, he may have to work all his life in a low-paying jbb with not much prospects. If benefics aspect the 10th lord, he holds a post of authority and will be held in high esteem for his  character. If Rahu or afflicted malefics are with the 10th lord, he may suffer disgrace in his career. He may be exposed to criminal action and face imprisonment.",
                7: "The 10th lord placed in the  7th house gives a mature wife who will assist the native in his work. He will travel abroad on diplomatic missions. He will be well known for his skill in talking and achieving objectives. He will make profits through partnerships and co-operative ventures. If malefics afflict the 10th lord, the native will be debased in his sexual habits and indulge in every kind of vice.",
                8: "The native has many breaks in career. If the 10th lord is fortified, he will occupy a high office in his field but only for a short time. If a malefic planet afflicts the 10th lord the person has criminal propensities and commits offences. If Jupiter influences the 10th lord by aspect or association in the 8th house, he will become a mystic or spiritual teacher. Saturn here makes the person an undertaker or otherwise employed in burning ghats, graveyards, etc.",
                9: "The 10th lord in the 9th house makes the native a spiritual stalwart. He will be a beacon light to spiritual seekers if Jupiter aspects the 10th lord. If both benefics and maiefics aspect the 10th lord, the native is generally fortunate and well-to-do. He follows a hereditary profession or that of a preacher, teacher or healer. The father of the native has a great influence on him. He will be a dutiful son and do many charitable deeds.",
                10: "If the 10th lord is strongly disposed in the 10th, the native can be highly successful in his profession and command respect and honour. If the lord is weak and afflicted, he will have no self-respect, cringing for favours. He will also be a dependent all his life. He will be fickle-minded. If the 10th lord occupies the 6th, 8th or 12th houses from Navamsa, the native's career will be routine and ordinary. If three other planets conjoin the 10th lord in the 10th house, the native becomes an ascetic.",
                11: "The person earns immense riches. Fortunate in every respect he will engage himself in meritorious deeds. He will give employment to hundreds of persons and will be endowed with a high sense of honour. He will have many friends. If the eleventh house comes under affliction, is friends will turn enemies and cause him every sort of hardship and worry.",
                12: "If the 10th lord occupies the twelfth house, the native will have to work in a far-off place. He will lack comforts and face many difficulties in life. If beneficially disposed, the native becomes a spiritual seeker. He will be separated from his family and wander about without success if malefics afflict the 10th lord. He will indulge in smuggling and other nefarious activities, Rahu afflicting the 10th lord makes the native a cheat and a criminal. He causes sorrow to his family and relatives."
            },
            11: {
                1: "The native will be born in a rich family. He will earn much wealth. According as the 11th lord in Lagna is strong, middling or weak, the native will be born in a very rich, fairly rich or well-to-do family. He will lose an elder brother early in life.The native will live with his elder brothers. Benefics there give harmonious relations. Malefics cause domestic bickerings but common residence. The native will earn through commercial concerns and banking business. Business with friends will bring good profits but if malefics join, the native may suffer heavy losses on account of friends.",
                2: "The native will live with his elder brothers. Benefics there give harmonious relations. Malefics cause domestic bickerings but common residence. The native will earn through commercial concerns and banking business. Business with friends will bring good profits but if malefics join, the native may suffer heavy losses on account of friends.",
                3: "The person will be a concert-singer or musician and will earn thereby. Gain through brothers is also indicated. He will have many friends and helpful neighbours- Afflictions give contrary results.",
                4: "One acquires profits through landed estates, rentals and products of the earth. His mother will be a cultured and distinguished lady. He will be renowned for his learning and scholarship of various subjects. He will live in comfort and enjoy all joys in life. He will have a devoted and charming wife.",
                5: "The native will have many children who will come up well in life. He will indulge in speculation and gain  much money. If the 11th lord is afflicted, he will be a gambler and indulge in foolish ventures. If 11th lord is beneficially disposed, the native will be pious and observe many resolves and vows which will enhance his prosperity.",
                6: "The person gains money through maternal relatives, litigation and running nursing-homes. If the 11th lord is afflicted in the 6th, the native thrives on setting person against person, involve himself in other peoples' quarrels and anti-social activity. If malefics afflict the 11 th lord, the native may lose through similar sources.",
                7: "The person marries more than once. He prospers in foreign countries. If there are afflictions to the 11th lord, the native carries on liaisons with women of ill-repute. He will indulge in trading in flesh and similar immoral activities. If the 11th lord is fortified, the native marries only once but a rich and influential woman.",
                8: "The native though rich at birth suffers  many calamities and loses much of his money. He will suffer from the depracies of thieves, cheats and swindlers. If the 11th lord occupies a malefic constellation, the native will be forced to eke out of his living by begging.",
                9: "He inherits a large paternal fortune and will be very lucky in life. He will possess many houses, conveyances and every other kind of luxury. He will be religious-minded and disseminate religious literature.	He will be charitable and set up charitable institutions.",
                10: "The native prospers very well in his business and makes good profits. His elder brother will also help him in his business. He will earn some prize-money for original contributions to the subject of his study or profession. Depending upon the benefic or malefic nature of the planet he will earn through fair or foul means.",
                11: "The native will have many friends and elder brothers who may help him throughout life. He will have a happy life with the blessings of wife, home, children and comforts.",
                12: "He will suffer losses in business. His eider brother will be ailing and much expenditure will be incurred on account of his illness. The native may also lose an elder brother by death. He will have to pay fines and penalties frequently and will be burdened with many domestic responsibilities."
            },
            12: {
                1: "The native will have a weak constitution and will be feeble-minded. He will, however, be handsome and sweet-tongued. If the sign is common, the native will generally be travelling about. If the 6th lord joins the 12th lord in Lagna,  the native will live long. But if the 8th house is afflicted, he will be short-lived. This also indicates imprisonment and living abroad. If the Lagna and 12th lords exchange signs, the native will be a miser, hated by all and devoid of intelligence.",
                2: "The person will suffer financial losses. He  may  contact  debts  and  get  involved  in nefarious activity. He will not eat timely meals. His eye-sight will be poor and his family life, marked by lack of harmony. If the twelfth lord is a benefic and in dignity, these evil indications will be greatly reduced and the native will have financial stability. He will be a tactful speaker. If the twelfth lord is ill-disposed, the native indulges in gossip and quarrelling.",
                3: "He will be timid and quiet. Loss of a brother is shown. He will be shabbily dressed. If malefics afflict, he may develop ear-ailments. He may have to spend much money on younger brothers. As a writer he may be unsuccessful. He may work in some commonplace job and earn very little. If the 12th lord joins the 2nd lord in the 3rd and is aspected by Jupiter or the 9th lord, one may have more than one wife.",
                4: "Early death to mother, mental restlessness, unnecessary worry, enmity of relatives and living abroad are some of the results. Suffering constant harrassment from the landlord, his residence will be in an ordinary house. But if the twelfth lord is well placed, these adverse indications get mitigated to a large extent. If Venus is strong, the native may own his own conveyance but it will always give trouble.",
                5: "Either difficulty to beget progeny or unhappiness from children will be experienced. He will be religious-minded and may undertake pilgrimages. Weak-minded and suffering mental aberrations, he feels he is miserable. He will not succeed in agriculture as his crops will suffer from pests and disease.",
                6: "The native will be happy and prosperous, live long, enjoy many comforts, possess a healthy and handsome physique, and vanquish his enemies. But he may become involved in litigation which may come to an end to his advantage. But if melefics afflict the 12th lord, the person will be unscrupulous, sinful and ill-tempered, hating his mother suffering from unhappiness on account of his own children. Womanising will land him in distress.",
                7: "The wife may come from a poor family. Married life will be unhappy and may end in separation. Later on he will take to asceticism. Weak in health and suffering from phlegmatic troubles, he will be without learning or property.",
                8: "The native will be rich and celebrated, will enjoy a luxurious life with many servants waiting on him. Gain through deaths and legacy is indicated. Interested in occult subjects and devoted to Lord Vishnu, he will be righteous, famous and a gentle speaker being endowed with many good qualities of head and heart.",
                9: "Residence abroad and prosperity are shown. He may acquire much property in foreign lands. Honest, generous and large-hearted he may not have any spiritual leanings. Not liking his wife, friends and preceptor, and interested in physical culture, he loses his father early in life.",
                10: "Hard-working and having to undertake tedious journeys for his occupation he will be a jailor, doctor or work in the cemetery and such places. He spends money on agricultural pursuits in which he makes profits. The native will derive no happiness or physical comforls from his sons.",
                11: "He will engage himself in business, but does not make much profit. He has few friends but many enemies. Troubled by extravagant brothers, some of whom may be invalids, the native's funds may dwindle on this account. He will earn well by trading in pearls, rubies and other precious stones.",
                12: "The native spends much on religious and righteous purposes. He will have good eye-sight and enjoy pleasures of the couch. He will be engaged in agriculture. If malefics afflict the twelfth lord, the native will be restless and always roaming about."
            }
        }

    def calculate_dashas(self, dob) -> Dict:
        moon_position = self.get_planet(PlanetName.MOON).position
        self._dashas['vimshottri'] = Dasha().get_dashas('vimshottri', start_position=moon_position,
                                                        date_of_birth=dob)

        return self._dashas

    @property
    def dashas(self):
        return self._dashas

    @staticmethod
    @abstractmethod
    def translate_position(position: Position):
        raise NotImplementedError('Not Implemented function translate_position')

    def __repr__(self) -> str:
        return self._name

    def __str__(self) -> str:
        return self._name

    @property
    def name(self) -> str:
        return self._name

    @property
    def areas_of_life(self):
        return self._areas_of_life.get(self.name)

    @property
    def ascendant(self):
        return self._ascendant

    @ascendant.setter
    def ascendant(self, value: Position):
        self._ascendant = value

    @property
    def houses(self):
        return self._houses

    # planet functions

    @property
    def planets(self):
        return self._planets

    def get_planet(self, planet) -> Planet:

        if isinstance(planet, Planet):
            return planet
        elif isinstance(planet, PlanetName):
            return self._planets.get(planet)
        elif isinstance(planet, str):
            return self._planets.get(PlanetName[str])
        else:
            print('RAISING EXCEPTION')
            raise Exception("invalid type parameter: planet", planet, type(planet))

    def get_planet_position(self, planet):
        return self.get_planet(planet).position

    def get_house_of_planet(self, planet) -> House:
        print('************getting house of planet: ', planet, 'from ', self._houses)

        for house in self._houses:
            if house.has_resident_planet(self.get_planet(planet)):
                print('returning house: ', house)
                return house

        print('self._houses: ', self._houses)
        raise Exception('House couldnt be found for planet: ', planet)

    def get_sign_of_planet(self, planet) -> Sign:
        print('**************getting sign of planet: ', planet)

        return self.get_planet(planet).position.sign

    def get_lord_of_planet(self, planet) -> PlanetName:
        print('****************getting lord of the planet: ', planet)
        return self.get_sign_of_planet(planet).lord

    def get_aspects_on_planet(self, planet, min_percent:int = 0):
        return

    def get_houses_owned(self, planet):
        houses = list()

        for house in self._houses:
            if planet.name in house.lord:
                houses.append(house)

        return houses

    # House functions
    def get_house(self, house) -> House:
        print('getting house: ', house)
        if isinstance(house, House):
            return house
        elif isinstance(house, int):
            for rec in self._houses:
                if rec.number == house:
                    return rec
        else:
            raise Exception('Invalid type for parameter house', house)

    def get_sign_of_house(self, house) -> Sign:
        return self.get_house(house).sign

    def get_lord_of_house(self, house) -> List[Planet]:
        return self.get_sign_of_house(self.get_house(house)).lord

    def get_planets_in_house(self, house):
        print('getting planets in house: ', house)
        return self.get_house(house).resident_planets

    def get_planets_aspecting_house(self, house, min_percent:int =0):
        return [planet for planet, percent in self.get_house(house).aspecting_planets if percent >= min_percent]

    # Sign Functions

    def get_planets_in_sign(self, sign) -> List[Planet]:
        print('checking planets for sign: ', sign)

        for house in self._houses:
            if house.sign == sign or house.sign == Sign(sign):
                return house.resident_planets

    def get_planet_state(self, planet) -> PlanetState:
        print('*********getting planet_state: ', planet)

        planet_search = self.get_planet(planet)

        print('*********getting planet_state: ', planet_search)

        if planet_search.exaltation_positions[0] <= self.get_planet_position(planet_search) <= planet_search.exaltation_positions[1]:
            return PlanetState.EXALTED

        if planet_search.mooltrikon_positions[0] <= self.get_planet_position(planet_search) <= planet_search.mooltrikon_positions[1]:
            return PlanetState.MOOLTRIKON

        # if in own raashi
        elif planet_search.name in self.get_planet_position(planet_search).sign.lord:
            return PlanetState.IN_OWN_SIGN

        # if debilitated
        elif planet_search.debilitation_positions[0] <= self.get_planet_position(planet_search) <= planet_search.debilitation_positions[1]:
            return PlanetState.DEBILITATED

        # get planets combined friendship with the lord of the sign
        combined_relation = self.get_planets_relation(planet_search, self.get_lord_of_planet(planet_search)[0])

        print('combined_relation: ', combined_relation)

        # if in great friend's sign
        if combined_relation == PlanetRelation.GREAT_FRIEND:
            return PlanetState.IN_GREAT_FRIEND_SIGN

        # if in friend's sign
        elif combined_relation == PlanetRelation.FRIEND:
            return PlanetState.IN_FRIENDLY_SIGN

        # if in neutral sign
        elif combined_relation == PlanetRelation.NEUTRAL:
            return PlanetState.IN_NEUTRAL_SIGN

        # if in enemy sign
        elif combined_relation == PlanetRelation.ENEMY:
            return PlanetState.IN_ENEMY_SIGN

        # if in great enemy sign
        elif combined_relation == PlanetRelation.GREAT_ENEMY:
            return PlanetState.IN_GREAT_ENEMY_SIGN

        else:
            raise Exception('Error while finding planet state.')

    def get_planet_perm_nature(self, planet):
        print('getting planet perm nature: ', planet)

        planet_search = self.get_planet(planet)

        if planet_search.name == PlanetName.MOON:
            difference = self._D1_sun_position.ra - self._D1_moon_position.ra
            if difference < 0:
                difference += 360

            if difference < 180:
                perm_nature = PlanetNature.MALEFIC
            else:
                perm_nature = PlanetNature.BENEFIC

        elif planet_search.name == PlanetName.MERCURY:
            perm_nature = PlanetNature.BENEFIC
            residence = self.get_house_of_planet(planet_search)
            planets_in_house = self.get_planets_in_house(residence)

            for planet in planets_in_house:
                print('planet: ', planet)
                if planet[0].name != PlanetName.MERCURY:
                    perm_nature += self.get_planet_perm_nature(planet[0])

        else:
            perm_nature = planet_search.nature

        return perm_nature

    def get_planet_nature(self, planet):
        print('*******getting planet nature for: ', planet)
        planet_search = self.get_planet(planet)

        # lord_strengths = [9, 1, 5, 10, 3, 6, 11, 8, 4, 12, 7, 2]
        lord_strengths = [3, 1, 1, 1, 2, 2, 2, 2, 3, 3, 3, 3]
        planet_nature = 0
        perm_nature = 0

        # getting planet's perm nature

        perm_nature = self.get_planet_perm_nature(planet_search)

        '''
        # assigning RAHU KETU nature score of their lord
        if planet_search.name in (PlanetName.RAHU, PlanetName.KETU):
            lord = self.get_lord_of_planet(planet_search)
            if lord.name != planet_search.name:
                planet_nature = self.get_planet_nature(lord)
                return planet_nature
        '''

        houses_owned = self.get_houses_owned(planet_search)
        print('houses owned: ', houses_owned)

        # calculating planet's nature based on ownership in kundli
        for house in houses_owned:
            # translating the house number to be able to search the array above of lord_strengths
            house_num = house.number - 1

            if house_num < 0:
                house_num += 12

            if house.number in (3, 6, 11):
                planet_nature -= lord_strengths[house_num]
            elif house.number in (1, 5, 9) and not(planet_search.name == PlanetName.MOON and house.number == 1):
                planet_nature += lord_strengths[house_num]
            elif house.number in (4, 7, 10) and not (planet_search.name == PlanetName.MARS and house.number == 10):
                if perm_nature < 0:
                    planet_nature += lord_strengths[house_num]
                elif perm_nature > 0:
                    planet_nature -= lord_strengths[house_num]

                # kendraadhipati dosha
                if self.get_house_of_planet(planet_search) == house.number:
                    if planet_search.nature < 0:
                        planet_nature -= lord_strengths[house_num]
                    elif planet_search.nature > 0:
                        planet_nature += lord_strengths[house_num]

        if len(houses_owned) == 2 and ((houses_owned[0].number in (4, 7, 10) and houses_owned[1].number in (5, 9))
                                       or (houses_owned[0].number in (5, 9) and houses_owned[1].number in (4, 7, 10))):
            planet_nature += 10
        elif len(houses_owned) == 2 and houses_owned[0].number in (2, 7) and houses_owned[1].number in (2, 7):
            planet_nature = -3



        print('returning: ', planet_nature)

        return planet_nature

    def get_planets_relation(self, of_planet, with_planet):

        of_planet_search = self.get_planet(of_planet)
        with_planet_search = self.get_planet(with_planet)

        print('******getting planets_relation: ', of_planet_search, with_planet_search)

        # get natural relation
        permanent_relation = of_planet_search.get_permanent_relation_with(with_planet_search)

        print('permanent relation b/w ', of_planet_search, with_planet_search, permanent_relation)

        # get relation based on ownership of houses
        temporary_relation = self.get_temporary_relation(of_planet_search, with_planet_search)
        print('temporary relation: ', temporary_relation)

        # calculate combined resulting relationship
        combined_relation = permanent_relation + temporary_relation

        print('returning combined_relation: ', combined_relation)

        return combined_relation

    def get_temporary_friends(self, planet):
        print('*******getting temporary friends for planet: ', planet)
        # get planet's sign
        sign = self.get_sign_of_planet(planet)
        print('found sign for planet: ', planet, sign)
        temporary_friends = list()

        # return planets in 2,3,4,10,11,12th sign
        for i in [2, 3, 4, 10, 11, 12]:
            sign_num = ((sign.value + i - 1) % 12)+1
            temporary_friends.extend(self.get_planets_in_sign(sign_num))

        print('returning ', temporary_friends)
        return temporary_friends

    def get_temporary_relation(self, of_planet, with_planet):
        print('getting relationship based on house ownership: ', of_planet, with_planet)
        of_planet_search = self.get_planet(of_planet)
        with_planet_search = self.get_planet(with_planet)
        
        for rec in self.get_temporary_friends(of_planet_search):
            if with_planet_search.name == rec[0].name:
                return PlanetRelation.FRIEND

        return PlanetRelation.ENEMY

    def equal_houses(self):
        print('calculating equal houses: ', self.name )
        print('self._houses: ', self._houses)
        self.get_house(1).start_position = self._ascendant.copy().add_ra(-15)
        self.get_house(1).end_position = self._ascendant.copy().add_ra(15)

        for house_num in range(2, 13):
            self.get_house(house_num).start_position = self.get_house(house_num-1).cusp_position.copy().add_ra(15)
            self.get_house(house_num).cusp_position = self.get_house(house_num-1).cusp_position.copy().add_ra(30)
            self.get_house(house_num).end_position = self.get_house(house_num-1).cusp_position.copy().add_ra(45)

        print('self._houses: ', self._houses)

    def unequal_houses(self):
        self._descendant = self._ascendant.copy().add_ra(180)
        print('Descendant: ', self._descendant)

        self._lower_meridian = self._zenith.copy().add_ra(180)
        print('lower meridian: ', self._lower_meridian)

        self.get_house(4).cusp_position = self._lower_meridian.copy()
        self.get_house(7).cusp_position = self._descendant.copy()
        self.get_house(10).cusp_position = self._zenith.copy()

        for i in range(0, 4):
            self.get_house(2 + (3 * i)).cusp_position = self.get_house(((3 * i) % 12) + 1).cusp_position.copy()
            self.get_house(3 + (3 * i)).cusp_position = self.get_house(((3 * i) % 12) + 1).cusp_position.copy()

            arc = self.get_house(((3 + (3 * i)) % 12) + 1).cusp_position.ra - self.get_house(
                ((3 * i) % 12) + 1).cusp_position.ra
            if arc < 0:
                arc = arc + 360

            self.get_house(2 + (3 * i)).cusp_position.add_ra(arc / 3)
            self.get_house(3 + (3 * i)).cusp_position.add_ra(2 * arc / 3)

        print('cusps initialized', self._houses)

        print('calculating start and end positions of houses')
        for i in range(2, 14):
            house = ((i - 1) % 12) + 1
            next_houses = (house % 12) + 1
            previous_houses = ((house - 2) % 12) + 1

            arc_ahead = self.get_house(next_houses).cusp_position.ra - self.get_house(house).cusp_position.ra
            if arc_ahead < 0:
                arc_ahead += 360

            # adding arc/2 to cusp to get end position
            self.get_house(house).end_position = self.get_house(house).cusp_position.copy().add_ra(arc_ahead / 2)

            arc_behind = self.get_house(house).cusp_position.ra - self.get_house(previous_houses).cusp_position.ra
            if arc_behind < 0:
                arc_behind += 360

            # subtracting arc/2 from cusp to get start position
            self.get_house(house).start_position = self.get_house(house).cusp_position.copy().add_ra(-arc_behind / 2)

    def add_planets_to_houses(self):
        print('Adding planets to houses')

        for house in range(1, 13):
            for planet in self._planets.values():
                print(planet)
                # incase the house spans across the end of zodiac
                if degrees_between(self.get_house(house).start_position.ra, planet.position.ra) < 180 \
                        and degrees_between(planet.position.ra, self.get_house(house).end_position.ra) < 180 :

                    degree_distance = degrees_between(planet.position.ra, self.get_house(house).cusp_position.ra)

                    house_degrees = degrees_between(
                            self.get_house(house).start_position.ra, self.get_house(house).end_position.ra)
                    # calculating strength of the house based on how far it is from cusp of the house
                    if degree_distance < 180:
                        strength_ratio = 2 * degree_distance / house_degrees
                        strength = 100-(50 * strength_ratio)
                    else:
                        strength_ratio = (2*(360-degree_distance)) / house_degrees

                        strength = 100 - (50 * strength_ratio)

                    self.get_house(house).add_resident_planet(planet, round(strength,2))


            sign_number = ((self._ascendant.sign + house - 2) % 12) + 1
            self.get_house(house).sign = Sign(sign_number)

        aspects = dict()

        aspect_values = {180: 100, 90: 75, 210: 75, 120: 50, 240: 50, 60: 25, 270: 25, 0: 0, 30: 0, 150: 0, 300: 0,
                         330: 0}

        for planet in self._planets.values():
            position = planet.position
            aspects[planet] = []
            for degrees, percent in aspect_values.items():
                ra = position.ra + degrees
                if ra > 360:
                    ra -= 360

                if planet.name == PlanetName.MARS and degrees in (90, 210):
                    aspects[planet].append((ra, 100))
                elif planet.name == PlanetName.JUPITER and degrees in (120, 240):
                    aspects[planet].append((ra, 100))
                elif planet.name == PlanetName.SATURN and degrees in (60, 270):
                    aspects[planet].append((ra, 100))
                else:
                    aspects[planet].append((ra, percent))

        print('aspects: ', aspects)

        # adding aspects
        for house in self._houses:
            # print('house: ', house)

            for planet, rec in aspects.items():
                for (ra, strength) in rec:
                    # print('planet, ra, strength: ', planet, ra, strength)

                    if 0 <= degrees_between(house.cusp_position.ra, ra) <= 30:
                        higher_values = (ra, strength)
                    elif 0 <= degrees_between(ra, house._cusp_position.ra) <= 30:
                        lower_values = (ra, strength)

                ratio = (higher_values[1] - lower_values[1]) / degrees_between(lower_values[0], higher_values[0])
                result = lower_values[1] + (degrees_between(lower_values[0], house.cusp_position.ra) * ratio)
                if result > 10:
                    house.add_aspecting_planet(planet, round(result,2))

    @property
    def chart(self, size=600):
        planet_addresses = dict()
        offset = 600/20
        points = [size/4, size/2, 3*size/4, size]
        house_numbers = [
            [points[1], points[1]],
            [points[0], points[0]],
            [points[0], points[0]],
            [points[1], points[1]],
            [points[0], points[2]],
            [points[0], points[2]],
            [points[1], points[1]],
            [points[2], points[2]],
            [points[2], points[2]],
            [points[1], points[1]],
            [points[2], points[0]],
            [points[2], points[0]]
        ]

        offsets = [
            [0, -offset],
            [0, -offset],
            [-offset, 0],
            [-offset, 0],
            [-offset, 0],
            [0, +offset],
            [0, +offset],
            [0, +offset],
            [+offset, 0],
            [+offset, 0],
            [+offset, 0],
            [0, -offset]
        ]

        house_signs=[]
        for house_num in range(1,13):
            house_signs.append(self.get_sign_of_house(house_num))

        chart = '<svg width="{size}" height="{size}">' \
                '<polygon points="0,0 {size},0 0,{size} 0,0 {size},{size} 0,{size} {size},0 {size},{size} ' \
                '{size},{hsize} {hsize},0 0,{hsize} {hsize},{size} {size},{hsize} {size},{size}"' \
                ' style="fill:white;stroke:purple;stroke-width:5" />'.format(size=size, hsize=size/2)

        #adding planets to houses in graph using column and row calculation
        for house_num in range(0, 12):
            planets_in_house = self.get_planets_in_house(house_num+1)
            for num, planet in enumerate(planets_in_house):

                row = 1
                column = 0

                if 1 <= num <= 3:
                    row = 2
                    column = ((num-1) % 3) - 1
                elif 3 < num:
                    row = 3
                    column = ((num - 4) % 3) - 2

                row += 1

                color = 'black'

                if self.get_planet_nature(planet[0]) > 0:
                    color = '#00e600'
                elif self.get_planet_nature(planet[0]) < 0:
                    color = 'red'

                if house_num in (0, 1, 11, 5, 6, 7):
                    x1 = house_numbers[house_num][0]+(offset*column)
                    y1 = house_numbers[house_num][1]+(offsets[house_num][1]*row)

                elif house_num in (2, 3, 4, 8, 9, 10):
                    x1 = house_numbers[house_num][0]+(offsets[house_num][0] * row)
                    y1 = house_numbers[house_num][1]+(offset * column)

                chart += '<text x={x} y={y} fill={color} font-size="12"> {planet} </text>'.format(
                    x= x1
                    , y= y1
                    , planet=str(planet[0].name.value[0:2])
                    , color=color)

                planet_addresses[planet[0].name] = [x1, y1]
        print('planet addresses in graph: ', planet_addresses)

        # adding sign numbers on houses
        for house_num in range(0, 12):
            x1 = house_numbers[house_num][0] + offsets[house_num][0]
            y1 = house_numbers[house_num][1] + offsets[house_num][1]

            chart += '<text x={x} y={y} fill=black> {house_sign} ' \
                     '</text>'.format(x=x1,
                                      y=y1,
                                      house_sign=house_signs[house_num])

            for lord in self.get_lord_of_house(house_num+1):
                print('lord: ', lord)
                addr = planet_addresses.get(lord)

                color = 'black'

                if self.get_planet_nature(lord) > 0:
                    color = '#00e600'
                elif self.get_planet_nature(lord) < 0:
                    color = 'red'

                chart += '<line x1 = {x1} y1 = {y1} x2 = {x2} y2 = {y2} style = "stroke:{color};stroke-width:1 stroke-dasharray: 10 5" />' \
                    .format(x1=x1, y1=y1, x2=addr[0], y2=addr[1], color=color)

        chart += '</svg>'
        return Markup(chart)

    @property
    def results(self):
        results = list()

        for house_num in range(1,13):
            for planet in self.get_lord_of_house(house_num):
                residence = self.get_house_of_planet(planet)

                print('result for ', house_num, residence.number)
                result_dict = self._lord_results.get(house_num)
                print('result_dict: ', result_dict)
                result_text = result_dict.get(residence.number)
                print(result_text)
                results.append(('lord of '+str(house_num)+' in '+str(residence.number), result_text))

        return results

    @property
    def conjunctions(self):
        conjunctions = dict()

        print('initializing conjunctions: ', conjunctions, len(conjunctions))
        for house_num in range(1, 13):
            print('house_num', house_num)
            planets = self.get_planets_in_house(house_num)
            if len(planets) > 1:
                temp_dict = dict()
                for planet, percent in planets:
                    print('planet: ', planet)
                    temp_dict[planet] = self.get_houses_owned(planet)
                if len(temp_dict):
                    conjunctions[house_num] = (temp_dict)
        print('conjunctions length ', len(conjunctions), ':', conjunctions)
        return conjunctions

    @property
    def interchanges(self):
        print('interchanges: ', self)
        interchanges = list()
        for house_num in range(1, 13):
            print('house_num: ', house_num)
            temp_list = list()
            for lord in self.get_lord_of_house(house_num):
                print('lord: ', lord)
                house_of_lord = self.get_house_of_planet(lord)
                for resident_planet, percent in self.get_planets_in_house(house_num):
                    print('resident_planet: ', resident_planet)
                    if house_of_lord in self.get_houses_owned(resident_planet) and house_of_lord.number != house_num:
                        temp_list.append((house_of_lord.number, resident_planet.name))
                if temp_list:
                    interchanges.append(((house_num, self.get_lord_of_house(house_num)), temp_list))
        print('interchanges: ', interchanges)
        return interchanges

    @property
    def mutual_aspects(self, min_percent: int = 0):
        print('mutual_aspects: ', self)
        mutual_aspects = list()
        for house_num in range(1, 13):
            print('house_num: ', house_num)
            temp_list = list()
            for aspecting_planet1 in self.get_planets_aspecting_house(house_num):
                print('planet: ', aspecting_planet1)
                house_of_planet = self.get_house_of_planet(aspecting_planet1)
                for aspecting_planet2, percent in self.get_planets_aspecting_house(house_num):
                    print('aspecting_planet: ', aspecting_planet)
                    if house_of_planet in self.get_houses_owned(aspecting_planet):
                        temp_list.append((house_of_planet.number, aspecting_planet.name))
                if temp_list:
                    mutual_aspects.append(((house_num, ), temp_list))
        print('multual_aspects: ', mutual_aspects)
        return mutual_aspects

    def get_applicable_dashas(self):

        from ChartD9 import ChartD9
        from ChartD12 import ChartD12

        chart_d9 = ChartD9(name=self.name+'-D9', ascendant=self.ascendant, zenith=self._zenith, planets=self.planets)
        chart_d12 = ChartD12(name=self.name+'-D12', ascendant=self.ascendant, zenith=self._zenith, planets=self.planets)


        applicable_dashas = []

        if chart_d9.get_sign_of_house(1) in (2,7):
            applicable_dashas.append('Dwadasottari, Revati, Ketu')

        for ascendant_lord in self.get_lord_of_house(1):

            if (((self.get_house_of_planet(PlanetName.RAHU).number - self.get_house_of_planet(ascendant_lord).number) % 4)
                    == 0) and self.get_house_of_planet(PlanetName.RAHU).number != 1:
                applicable_dashas.append('Ashtottari, Ardra, Mars '+self.get_planet_state(PlanetName.MARS).name)

            if self.get_house_of_planet(ascendant_lord).number == 7:
                applicable_dashas.append('Dwisaptati, Moola, Rahu'+self.get_planet_state(PlanetName.RAHU).name)

        if chart_d12.get_sign_of_house(1).value == 4:
            applicable_dashas.append('Panchottari, Anuradha, Venus'+self.get_planet_state(PlanetName.VENUS).name)

        if self.get_sign_of_house(1).value == chart_d9.get_sign_of_house(1).value:
            applicable_dashas.append('Satabdika, Revati, Sun'+self.get_planet_state(PlanetName.SUN).name)

        for tenth_lord in self.get_lord_of_house(10):

            if self.get_house_of_planet(tenth_lord).number == 10:
                applicable_dashas.append('Chaturaaseeti, Swati, Saturn'+self.get_planet_state(PlanetName.SATURN).name)

        for seventh_lord in self.get_lord_of_house(7):
            if self.get_house_of_planet(seventh_lord).number == 1:
                applicable_dashas.append('Dwisaptati, Moola, Rahu'+self.get_planet_state(PlanetName.RAHU).name)

        if self.get_house_of_planet(PlanetName.SUN).number == 1:
            applicable_dashas.append('Shashtisama, Ashwini, Moon'+self.get_planet_state(PlanetName.MOON).name)

        if (self.ascendant.degrees_in_sign >= 15 and self.ascendant.sign.value % 2 == 0) or \
                (self.ascendant.degrees_in_sign < 15 and self.ascendant.sign.value % 2 == 1):
            difference = self.get_planet(PlanetName.SUN).position.ra - self.get_planet(PlanetName.MOON).position.ra
            if difference < 0:
                difference += 360

            if difference >= 180:
                applicable_dashas.append('Shodasottari, Pushya, Jupiter'+self.get_planet_state(PlanetName.JUPITER).name)

            difference = self.get_planet(PlanetName.SUN).position.ra - self.ascendant.ra
            if difference < 0:
                difference += 360

            if difference >= 180:
                applicable_dashas.append('Shattrimsa, Sravana, Mercury'+self.get_planet_state(PlanetName.MERCURY).name)

        if (self.ascendant.degrees_in_sign >= 15 and self.ascendant.sign.value % 2 == 1) or \
                (self.ascendant.degrees_in_sign < 15 and self.ascendant.sign.value % 2 == 0):
            difference = self.get_planet(PlanetName.MOON).position.ra - self.get_planet(PlanetName.SUN).position.ra
            if difference < 0:
                difference += 360

            if difference >= 180:
                applicable_dashas.append('Shodasottari, Pushya, Jupiter'+self.get_planet_state(PlanetName.JUPITER).name)

            difference = self.ascendant.ra - self.get_planet(PlanetName.SUN).position.ra
            if difference < 0:
                difference += 360

            if difference >= 180:
                applicable_dashas.append('Shattrimsa, Sravana, Mercury'+self.get_planet_state(PlanetName.MERCURY).name)

        return applicable_dashas