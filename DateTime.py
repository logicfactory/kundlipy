from datetime import datetime, timezone, timedelta
import spiceypy
from Planet import Planet, PlanetName
from decimal import Decimal


class DateTime:

    def __init__(self, date_time: str = None, fmt: str = '%Y-%m-%d %H:%M %z', day:int=0, month:int=0,year:int=0,
                 hour: int= 0, min: int = 0, sec: int = 0, microsecond: int = 0, tzinfo = None, et: str = None) -> None:
        print('initializing DateTime ', date_time)
        if date_time is not None:
            self._date_time = datetime.strptime(date_time, fmt)
        elif day != 0:
            self._date_time = datetime(year=year, month=month, day=day, hour=hour, minute=min, second=sec,
                                       microsecond=microsecond, tzinfo=tzinfo)
        elif et is not None:
            self.et = et
        else:
            self._date_time = datetime.utcnow()

    def __repr__(self) -> str:
        return self._date_time.strftime('%d-%b-%Y %H:%M:%S')

    def __add__(self, other):
        self.date_time = self.date_time + other
        return self

    def __sub__(self, other):
        if isinstance(other, DateTime):
            return self.date_time - other.date_time
        elif isinstance(other, timedelta):
            self.date_time = self.date_time - other
            return self

    @property
    def date_time(self):
        return self._date_time

    @date_time.setter
    def date_time(self, value: datetime):
        print('setting datetime to: ', value)
        self._date_time = value
        print('datetime set to: ', self._date_time)

    @property
    def utc(self):
        return self._date_time.astimezone(timezone.utc)

    @utc.setter
    def utc(self, value):
        pass

    @property
    def et(self):
        print('displaying et value using: ', self.utc.strftime('%Y-%m-%dT%H:%M:%S.%f'))
        return spiceypy.utc2et(self.utc.strftime('%Y-%m-%dT%H:%M:%S.%f'))

    @et.setter
    def et(self, value):
        print('setting et value using: ', datetime.strptime(spiceypy.et2utc(value, 'C', 2), '%Y %b %d %H:%M:%S.%f').replace(tzinfo=timezone.utc))
        self._date_time = datetime.strptime(spiceypy.et2utc(value, 'C', 2), '%Y %b %d %H:%M:%S.%f').replace(tzinfo=timezone.utc)
        print('datetime set to: ', self._date_time)

    @property
    def sidereal_time(self):
        return

    @sidereal_time.setter
    def sidereal_time(self, value):
        pass

    @property
    def day(self):
        return self._date_time.day

    @property
    def month(self):
        return self._date_time.month

    @property
    def year(self):
        return self._date_time.year

    @property
    def hour(self):
        return self._date_time.hour

    @property
    def minute(self):
        return self._date_time.minute

    @property
    def second(self):
        return self._date_time.second

    @property
    def microsecond(self):
        return self._date_time.microsecond

    @property
    def tzinfo(self):
        return self._date_time.tzinfo

    @property
    def tzname(self):
        return self._date_time.tzname()

    def get_planets_positions(self, frame: str = 'ECLIPJ2000', observer: str = 'EARTH', fixed_zodiac: bool = False):
        print('getting planets position for ', observer, frame, self.utc, fixed_zodiac)
        planet_positions = dict()

        for planet in PlanetName:
            if planet.name in ('RAHU','KETU'):
                planet_positions[planet] = Planet(planet.name, self.get_nodes_position(planet, frame=frame,
                                                                                       fixed_zodiac=fixed_zodiac))
            else:
                planet_positions[planet] = Planet(planet.name, self.get_planet_position(planet, frame, observer,
                                                                                     fixed_zodiac))

        return planet_positions

    def get_planet_position(self, planet: PlanetName, frame: str = 'ECLIPJ2000', observer: str = 'EARTH', fixed_zodiac: bool = False):
        print('getting planet position for ', planet, observer, frame, self.date_time, self.et, self.utc, fixed_zodiac)
        print('   ET Seconds Past J2000: {:16.3f}'.format(self.et))
        calet = spiceypy.etcal(self.et)
        print('   Calendar ET (etcal):   {:s}'.format(calet))
        calet = spiceypy.timout(self.et, 'YYYY-MON-DDTHR:MN:SC ::TDB')
        print('   Calendar ET (timout):  {:s}'.format(calet))

        planet_positions = []

        if planet.name in ('RAHU','KETU'):
            position = self.get_nodes_position(planet, frame=frame, fixed_zodiac=fixed_zodiac)
        else:
            location, lightTimes = spiceypy.spkpos(planet.name, self.et, frame, 'NONE', observer)
            from Position import Position
            position = Position(frame, location[0], location[1], location[2])
            print('rectangular position: ', location)
            print('RAD position: ', spiceypy.recrad(location))

            #position.add_ra(self.get_tropical_correction())

            if fixed_zodiac:
                position.add_ra(-self.precession)

        print(planet, ' position in ', frame, ' : ', 'position :', position)
        return position

    def get_nodes_position(self, node: PlanetName, frame: str = 'ECLIPJ2000', fixed_zodiac: bool= False):
        print('getting ', node,' position at: ', self, fixed_zodiac)
        #
        # Local Parameters
        #
        nodes_locations = {}

        TDBFMT = 'YYYY MON DD HR:MN:SC.### (UTC) ::UTC'
        MAXIVL = 1000
        MAXWIN = 2 * MAXIVL

        # Assign the inputs for our search.
        #
        # Since we're interested in the apparent location of the
        # target, we use light time and stellar aberration
        # corrections. We use the "converged Newtonian" form
        # of the light time correction because this choice may
        # increase the accuracy of the occultation times we'll
        # compute using gfoclt.
        #
        srfpt = 'EARTH'
        obsfrm = 'ECLIPJ2000'
        target = 'MOON'
        abcorr = 'NONE'
        crdsys = 'RA/DEC'
        coord = 'DECLINATION'
        relate = '='
        revlim = 0

        #
        # The adjustment value only applies to absolute extrema
        # searches; simply give it an initial value of zero
        # for this inequality search.
        #
        adjust = 0.0

        #
        # stepsz is the step size, measured in seconds, used to search
        # for times bracketing a state transition. Since we don't expect
        # any events of interest to be shorter than five minutes, and
        # since the separation between events is well over 5 minutes,
        # we'll use this value as our step size. Units are seconds.
        #
        stepsz = 3600.0

        #
        # Display a banner for the output report:
        #
        print('\n{:s}\n'.format('Inputs for target visibility search:'))
        print('   Target                       = {:s}'.format(target))
        print('   Observation surface location = {:s}'.format(srfpt))
        print('   Observer\'s reference frame   = {:s}'.format(obsfrm))
        print('   Aberration correction        = {:s}'.format(abcorr))
        print('   Step size (seconds)          = {:f}'.format(stepsz))

        #
        # Convert the start and stop times to ET.
        #
        etbeg = self.et - 1296000
        etend = self.et + 1296000

        print('searching between: ', spiceypy.et2utc(etbeg, "C", 2), ' and ', spiceypy.et2utc(etend, "C", 2))

        #
        # Initialize the "confinement" window with the interval
        # over which we'll conduct the search.
        #
        cnfine = spiceypy.stypes.SPICEDOUBLE_CELL(2)
        spiceypy.wninsd(etbeg, etend, cnfine)

        #
        # In the call below, the maximum number of window
        # intervals gfposc can store internally is set to MAXIVL.
        # We set the cell size to MAXWIN to achieve this.
        #
        riswin = spiceypy.stypes.SPICEDOUBLE_CELL(MAXWIN)

        #
        # Now search for the time period, within our confinement
        # window, during which the apparent target has elevation
        # at least equal to the elevation limit.
        #
        spiceypy.gfposc(target, obsfrm, abcorr, srfpt,
                     crdsys, coord, relate, revlim,
                     adjust, stepsz, MAXIVL, cnfine, riswin)
        #
        # The function wncard returns the number of intervals
        # in a SPICE window.
        #
        winsiz = spiceypy.wncard(riswin)
        time1 = time2 = self.et

        if winsiz == 0:

            print('No events were found.')

        else:

            #
            # Display the visibility time periods.
            #
            print('Visibility times of {0:s} as seen from {1:s}:\n'.format(target, srfpt))

            print('winsiz: ', winsiz)
            print('riswin: ', riswin)
            time2 = etend

            for i in range(winsiz):

                #
                # Fetch the start and stop times of
                # the ith interval from the search result
                # window riswin.
                #
                ith_time = spiceypy.wnfetd(riswin, i)[0]
                print('--------------in the loop------------', spiceypy.et2utc(ith_time, "C", 2))

                print(self.et, etend, ': ', i, '->', ith_time, time1, time2 )

                if ith_time <= self.et:
                    time1 = ith_time
                elif self.et <= ith_time <= time2:
                    time2 = ith_time

                print('times: ', spiceypy.et2utc(time1, "C", 2), spiceypy.et2utc(time2, "C", 2))

            print(spiceypy.et2utc(time1, "C", 2), spiceypy.et2utc(self.et, "C", 2), spiceypy.et2utc(time2, "C", 2))
            datetime1=DateTime()
            datetime1.et = time1

            datetime2 = DateTime()
            datetime2.et = time2

            datetime11 = DateTime()
            datetime11.et = time1 - 360000

            datetime21 = DateTime()
            datetime21.et = time2 - 360000

            position1 = datetime1.get_planet_position(PlanetName.MOON, frame=frame, fixed_zodiac=fixed_zodiac)
            position2 = datetime2.get_planet_position(PlanetName.MOON, frame=frame, fixed_zodiac=fixed_zodiac)

            moon11 = datetime11.get_planet_position(PlanetName.MOON, frame=obsfrm, fixed_zodiac=fixed_zodiac)
            sun11 = datetime11.get_planet_position(PlanetName.SUN, frame=obsfrm, fixed_zodiac=fixed_zodiac)
            moon21 = datetime21.get_planet_position(PlanetName.MOON, frame=obsfrm, fixed_zodiac=fixed_zodiac)
            sun21 = datetime21.get_planet_position(PlanetName.SUN, frame=obsfrm, fixed_zodiac=fixed_zodiac)

            print('initial time11: ', datetime11, ' positions11: ', moon11.rad_coordinates, sun11.rad_coordinates)
            print('initial time1: ', datetime1, ' positions1: ', position1.rad_coordinates)
            print('initial time21: ', datetime21, ' positions21: ', moon21.rad_coordinates, sun21.rad_coordinates)
            print('initial time2: ', datetime2, ' positions2: ', position2.rad_coordinates)

            if sun11.declination < moon11.declination:
                print('Position 1 is South Node')
                from Position import Position
                position1 = Position().from_rad_coordinates(frame=position1.frame,
                                                                           distance=position1.distance,
                                                                           ra=position1.ra - 180,
                                                                           declination=position1.declination)
                print('time1: ', datetime1, ' positions1: ', position1.rad_coordinates)

            if sun21.declination < moon21.declination:
                print('Position 2 is South Node')
                from Position import Position
                position2 = Position().from_rad_coordinates(frame=position2.frame,
                                                                           distance=position2.distance,
                                                                           ra=position2.ra - 180,
                                                                           declination=position2.declination)
                print('time2: ', datetime2, ' positions2: ', position2.rad_coordinates)

            print('final time1: ', datetime1, ' positions1: ', position1.rad_coordinates, position1)
            print('final time2: ', datetime2, ' positions2: ', position2.rad_coordinates, position2)


            time_difference = time2 - time1
            print('time_difference between 2 and 1: ', time_difference)

            print('ra from: ', position1.ra, ' to: ', position2.ra)
            ra_difference = position2.ra - position1.ra
            print('ra difference and 1 and 2: ', ra_difference)

            time_ratio = (self.et - time1)/time_difference
            print('time_ratio: ', time_ratio)

            ra = position1.ra + (ra_difference*time_ratio)
            if ra < 0:
                ra += 360
            print('node ra: ', ra)

            declination_difference = (position2.declination - position1.declination)
            print('declination_difference: ', declination_difference)

            declination = position1.declination + (declination_difference * time_ratio)
            print('declination: ', declination)
            from Position import Position

            rahu_position = Position().from_rad_coordinates(frame=position1.frame,
                                                            distance=position1.distance, ra=ra,
                                                            declination=declination)

            print('RAHU: ', rahu_position.ra)

            ketu_position = Position().from_rad_coordinates(frame=rahu_position.frame,
                                                                           distance=rahu_position.distance,
                                                                           ra=rahu_position.ra + 180,
                                                                           declination=rahu_position.declination)

            nodes_locations = {'RAHU': rahu_position, 'KETU': ketu_position}
        print(nodes_locations)
        return nodes_locations.get(node.value)

    def get_tropical_correction(self):
        TDBFMT = 'YYYY MON DD HR:MN:SC.### (UTC) ::UTC'
        MAXIVL = 1000
        MAXWIN = 2 * MAXIVL

        # Assign the inputs for our search.
        #
        # Since we're interested in the apparent location of the
        # target, we use light time and stellar aberration
        # corrections. We use the "converged Newtonian" form
        # of the light time correction because this choice may
        # increase the accuracy of the occultation times we'll
        # compute using gfoclt.
        #
        srfpt = 'EARTH'
        obsfrm = 'J2000'
        target = 'SUN'
        abcorr = 'LT+S'
        crdsys = 'RA/DEC'
        coord = 'DECLINATION'
        relate = '='
        revlim = 0

        #
        # The adjustment value only applies to absolute extrema
        # searches; simply give it an initial value of zero
        # for this inequality search.
        #
        adjust = 0.0

        #
        # stepsz is the step size, measured in seconds, used to search
        # for times bracketing a state transition. Since we don't expect
        # any events of interest to be shorter than five minutes, and
        # since the separation between events is well over 5 minutes,
        # we'll use this value as our step size. Units are seconds.
        #
        stepsz = 300

        #
        # Display a banner for the output report:
        #
        print('\n{:s}\n'.format('Inputs for target visibility search:'))
        print('   Target                       = {:s}'.format(target))
        print('   Observation surface location = {:s}'.format(srfpt))
        print('   Observer\'s reference frame   = {:s}'.format(obsfrm))
        print('   Aberration correction        = {:s}'.format(abcorr))
        print('   Step size (seconds)          = {:f}'.format(stepsz))

        #
        # Convert the start and stop times to ET.
        #
        et = spiceypy.utc2et(str(self.year) + '-01-01T00:00:00.0')

        etbeg = et
        etend = et + 7296000

        # print('searching between: ', spiceypy.et2utc(etbeg, "C", 2), ' and ', spiceypy.et2utc(etend, "C", 2))

        #
        # Initialize the "confinement" window with the interval
        # over which we'll conduct the search.
        #
        cnfine = spiceypy.stypes.SPICEDOUBLE_CELL(2)
        spiceypy.wninsd(etbeg, etend, cnfine)

        #
        # In the call below, the maximum number of window
        # intervals gfposc can store internally is set to MAXIVL.
        # We set the cell size to MAXWIN to achieve this.
        #
        riswin = spiceypy.stypes.SPICEDOUBLE_CELL(MAXWIN)

        #
        # Now search for the time period, within our confinement
        # window, during which the apparent target has elevation
        # at least equal to the elevation limit.
        #
        spiceypy.gfposc(target, obsfrm, abcorr, srfpt,
                        crdsys, coord, relate, revlim,
                        adjust, stepsz, MAXIVL, cnfine, riswin)
        #
        # The function wncard returns the number of intervals
        # in a SPICE window.
        #
        winsiz = spiceypy.wncard(riswin)
        time1 = time2 = et

        if winsiz == 0:

            print('No events were found.')

        else:

            #
            # Display the visibility time periods.
            #
            # print('Visibility times of {0:s} as seen from {1:s}:\n'.format(target, srfpt))

            # print('winsiz: ', winsiz)
            # print('riswin: ', riswin)
            time2 = etend

        ra = 0

        for i in range(winsiz):
            #
            # Fetch the start and stop times of
            # the ith interval from the search result
            # window riswin.
            #
            ith_time = spiceypy.wnfetd(riswin, i)[0]
            print('--------------in the loop------------', spiceypy.et2utc(ith_time, "C", 2))

            distance, ra, dec = spiceypy.recrad(spiceypy.spkpos('SUN', ith_time, 'ECLIPJ2000', 'LT+S', 'EARTH')[0])
            ra = ra * spiceypy.dpr()
            print('ECLIPJ2000 time: ', spiceypy.et2utc(ith_time + i, 'C', 2), ' J2000ra: ', ra)

        if 0 <= ra < 180:
            return -ra
        elif 180 <= ra < 360:
            return 360 - ra

    @property
    def precession(self):
        reference_values = {
            1900: 21.32419722222222,
            2000: 22.72197777777778,
            2014: 22.91774166666667,
            2100: 24.12041111111111,
            2200: 25.51941944444444,
            2300: 26.91904166666667,
            3000: 36.73360555555556
                            }

        min_value = 0
        max_value = 3000

        for rec in reference_values.items():
            if self.year >= rec[0] >= min_value:
                min_value = rec[0]
            elif self.year < rec[0] <= max_value:
                max_value = rec[0]

        velocity = (reference_values.get(max_value) - reference_values.get(min_value)) / (max_value - min_value)

        if (max_value - self.year) < (self.year - min_value):
            ref_value = max_value
        else:
            ref_value = min_value

        precision = reference_values.get(ref_value) + (velocity * (self.year - ref_value)) + .009

        print('precision: ', precision)

        #return precision
        return 128.7219904004 - 106


    #TODO
    #date_time to longitude
    #date_time to RA
    #reverse of above 2
    #add
    #difference

